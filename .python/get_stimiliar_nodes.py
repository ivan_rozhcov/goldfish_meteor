# -*- coding: utf-8 -*-
from datetime import datetime
from fuzzywuzzy import fuzz
from settings import DB, NODES_COLLECTION, USER_ID

SAME_FOR_SURE = 97
PROBABLY = 80
MAYBE = 78


def find_similar_nodes():
    #users, 'content_type': 'text'\html
    # TODO order by process date
    for node in DB[NODES_COLLECTION].find(
            {'users': USER_ID, 'content_type': 'text', 'attrs.content': {'$exists': True}, 'was_checked_similarity': {'$ne': True}}):
        # print(node)
        for node2 in DB[NODES_COLLECTION].find(
                {'users': USER_ID, 'content_type': 'text', 'attrs.content': {'$exists': True},
                 '_id': {'$ne': node['_id']}}):
            # print(node2)
            token_set_ratio = fuzz.token_set_ratio(node['attrs']['content'], node2['attrs']['content'])
            print('token_set_ratio', token_set_ratio)
            if token_set_ratio > PROBABLY:
                print('add similar node')
                print(node['attrs']['content'])
                print('\n')
                print(node2['attrs']['content'])
                print('\n')

                print (node['_id'], node2['_id'])
                #дополнительные проверки
                token_sort_ratio = fuzz.token_sort_ratio(node['attrs']['content'], node2['attrs']['content'])
                ratio = fuzz.ratio(node['attrs']['content'], node2['attrs']['content'])
                partial_ratio = fuzz.partial_ratio(node['attrs']['content'], node2['attrs']['content'])
                print('other checks', token_sort_ratio, ratio, partial_ratio)
                if all(item > MAYBE for item in [token_sort_ratio, ratio, partial_ratio]):
                    print('looks ok')
                    # print(node, node2)

                    DB[NODES_COLLECTION].update_one(
                        {'_id': node['_id']},
                        {
                            '$addToSet': {'similar_nodes': node2['_id']},
                            '$set': {
                                'was_checked_similarity': True,
                                'similarity_check_time': datetime.now(),
                                'similarities.{}'.format(node2['_id']):
                                      {
                                          'token_set_ratio': token_set_ratio,
                                          'token_sort_ratio': token_sort_ratio,
                                          'ratio': ratio,
                                          'partial_ratio': partial_ratio,
                                      }
                            }
                        }
                    )
                    #и обратно
                    DB[NODES_COLLECTION].update_one(
                        {'_id': node2['_id']},
                        {
                            '$addToSet': {'similar_nodes': node['_id']},
                            '$set': {
                                'similarities.{}'.format(node['_id']):
                                      {
                                          'token_set_ratio': token_set_ratio,
                                          'token_sort_ratio': token_sort_ratio,
                                          'ratio': ratio,
                                          'partial_ratio': partial_ratio,
                                      }
                                  }
                        }
                    )
                    #TODO - подумать как не проходить по node2 второй раз

        DB[NODES_COLLECTION].update_one(
            {'_id': node['_id']},
            {
                '$set': {'was_checked_similarity': True,
                         'similarity_check_time': datetime.now(),
                }
            }
        )



if __name__ == '__main__':
    find_similar_nodes()