Template.functionsHelp.helpers
  getFunctions: ->
    funcArr = [
      createLink: createLink, cloneNodeShape: cloneNodeShape,
      getVirtualAttribute: getVirtualAttribute, mindTrail: mindTrail
    ]

    result = []
    for obj in funcArr
      #hack to print function name
      for key of obj
        objStrName = key
        strFunc = String(obj[key])
        if strFunc.split('/*')[1] and strFunc.split('/*')[1].split('*/')[0]
          result.push(objStrName + '<br/>' + strFunc.split('/*')[1].split('*/')[0].trim().replace(/\n/g, "<br />"))
    result