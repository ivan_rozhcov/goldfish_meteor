Template._featuresDemoPage.helpers
  node_types: ->
    nodeTypes = NodeTypes.find().fetch()
    nodeTypes = _.union nodeTypes, [type: 'adapter', type: 'adapter-dynamic']
    for nodeType in nodeTypes
      nodeType.widgets = NodeTypesToWidgets.findOne(type: nodeType.type)
    nodeTypes
  NodeWidgetTypes: ->
    NodeWidgetTypes.find()
  NodeStorageTypes: ->
    NodeStorageTypes.find()
  nodeshape_adapters: ->
    nsa = NodeShapeAdapters.find().fetch()
    for nsAdapter in nsa
      nsAdapter.defaults = NodeShapeAdaptersDefaultWidgets.findOne(type: nsAdapter.type)
    nsa

Template._featuresDemoPage.events
  'click .create': ->
    plotId = createPlot("Test plot #{@type}", @type, [Meteor.userId()])
    NodeShapeAdapters.update @_id, $set: example_plot_id: plotId
    createNodeWithNodeShapeShort "Тестовый нод", 48, 230, plotId, 160, 60
    Router.go("/plot/#{plotId}")

  'click .create-node': (ev, tmpl) ->
    #выбрать плот
    graphPlotAdapter = NodeShapeAdapters.findOne({type: "nsg_graph_plot"})
    console.log graphPlotAdapter
    if graphPlotAdapter and graphPlotAdapter.example_plot_id
      target = $(ev.currentTarget)
      console.log target.data('type'), target.data('widget')
      if target.data('type') in ['adapter', 'adapter-dynamic']
        #create plot
        createNodeWithNodeShapeAdapter 'Тестовый нод заголовок', [Meteor.userId()], 60, 270, target.data('type'), target.data('widget')
      else
  #      #создать на нем нод
        nsId = createNodeWithNodeShape 'Тестовый нод заголовок', 'Тестовый нод', [Meteor.userId()],  50, 230,
          graphPlotAdapter.example_plot_id, 160, 60, target.data('type'), target.data('widget')

      #открыть плот
      Router.go("/plot/#{graphPlotAdapter.example_plot_id}")

  'click .plot-external-edit': ->
