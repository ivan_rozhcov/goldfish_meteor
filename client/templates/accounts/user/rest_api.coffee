Template.RestApi.helpers
  userId: ()->
    Meteor.userId()

  authorizationToken: ()->
    Meteor.settings.public.apiAuthToken

  host: ()->
    Iron.Location.get(). host