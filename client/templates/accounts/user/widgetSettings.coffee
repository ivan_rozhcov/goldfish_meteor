Template.widgetSettings.helpers
  node_types: ->
    nodeTypes = NodeTypes.find().fetch()
    for nodeType in nodeTypes
      nodeType.widgets = NodeTypesToWidgets.findOne(type: nodeType.type)
    nodeTypes
  nodeshape_adapters: ->
      nsa = NodeShapeAdapters.find().fetch()
      for nsAdapter in nsa
        nsAdapter.defaults = NodeShapeAdaptersDefaultWidgets.findOne(type: nsAdapter.type)
      nsa

  NodeStorageTypes: ->
    NodeStorageTypes.find()
  NodeWidgetTypes: ->
    NodeWidgetTypes.find()


