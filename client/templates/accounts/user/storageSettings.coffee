Template.storageSettings.helpers
  getCount: (counterName) ->
    Counts.get(counterName)

  nodeStorageTypes: ->
    NodeStorageTypes.find()


Template.storageSettings.events
  'click .delete-some-history': ->
    bootbox.confirm('Вы точно хотите удалить все записи из истории?', (result) ->
        if result
          Meteor.call('deleteSomeHistory')
    )