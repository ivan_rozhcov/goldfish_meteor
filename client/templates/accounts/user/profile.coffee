Template.profileSideBar.helpers
    isActive: (templateName)->
        currentRoute = Router.current()
        currentRoute and currentRoute.lookupTemplate() == templateName

Template.connectedServices.helpers
  connected_services: ->
    #TODO plugins
    return [
        {'name': 'evernote', 'url': '/account/profile-api/evernote', isConnected: if Meteor.user() and Meteor.user().profile and Meteor.user().profile.evernote and Meteor.user().profile.evernote.accessToken then true else false},
        {'name': 'hypothesis', 'url': '/account/profile-api/hypothes', isConnected: if Meteor.user() and Meteor.user().profile and Meteor.user().profile.hypothes and Meteor.user().profile.hypothes.login then true else false}
        {'name': 'embedly', 'url': '/account/profile-api/embedly', isConnected: if Meteor.user() and Meteor.user().profile and Meteor.user().profile.embedly and Meteor.user().profile.embedly.key then true else false}
        {'name': 'evernote prod', 'url': '/account/profile-api/evernote-prod', isConnected: if Meteor.user() and Meteor.user().profile and Meteor.user().profile.evernote and Meteor.user().profile.evernote and Meteor.user().profile.evernote.prod_token then true else false}
        {'name': 'google calendar', url: '/account/profile-api/google_calendar'},
        {'name': 'dropbox', 'url': '/account/profile-api/dropbox'}
        {'name': 'google drive'},
        {'name': 'google contacts'},
        {'name': 'google tasks'},
    ]
  isConnected: ->
    return this.isConnected == true

Template.authorizedServices.helpers
  authorized_services: ->
    return [
        {'name': 'goldfish-android'},
        {'name': 'goldfish-chome-plugin'},
        {'name': 'IFTTT'},
        {'name': 'Zapier'}
    ]

# TODO сделать кастомные кнопки авторизации в шаблоне
Template._loginButtonsLoggedInDropdownActions.rendered = ->
  if $('#login-buttons-profile').length == 0
    $('#login-dropdown-list').children('.dropdown-menu').prepend('<button class="btn btn-block btn-primary" id="login-buttons-profile">Профиль</button>')

Template._loginButtonsLoggedInDropdown.events
  'click #login-buttons-profile': ->
    Router.go('/account/profile-api')

#TODO сделать супер топроный механизм подключения - указывать url-zap + вкл \ выкл

Template.connectedExtensions.helpers
  available_extensions: ->
    return [
        {'name': 'экспорт плота в evernote', 'connect_action': 'evernote-zappier'},
        {'name': 'minimap for plot'},
        {'name': 'старение карточек'},
    ]

Template.connectedExtensions.events
  'click .send-extension-request': ->
    bootbox.alert 'Создание расширений дело не сложное - свяжитесь mailto:me@ivan133.ru со мной и я расскажу подробней.'
  'click .evernote-zappier': ->

    syncZappierUrlDefaultValue = Meteor.user().profile.extensions && Meteor.user().profile.extensions.export_to_evernote && Meteor.user().profile.extensions.export_to_evernote.sync_zappier_url
    unless syncZappierUrlDefaultValue
      syncZappierUrlDefaultValue = ''

    exportZappierUrlDefaultValue = Meteor.user().profile.extensions && Meteor.user().profile.extensions.export_to_evernote && Meteor.user().profile.extensions.export_to_evernote.export_zappier_url
    unless exportZappierUrlDefaultValue
      exportZappierUrlDefaultValue = ''

    url = '<form id=\'infos\' action=\'\'>    Укажите url из рецепта zappier webhooks -> evernote:<input type=\'text\' id=\'export_zap\' value=\'' + exportZappierUrlDefaultValue + '\'/><br/>    Укажите url из рецепта zappier webhooks -> evernote (информация об обновлении в goldfish):<input type=\'text\' id=\'sync_zap\' value=\'' + syncZappierUrlDefaultValue + '\' />    </form>'

    bootbox.confirm url, (result) ->
      if result
        exportZap = $('#export_zap').val()
        syncZap = $('#sync_zap').val()
        if exportZap
          Meteor.users.update(Meteor.userId(), {$set:{'profile.extensions.export_to_evernote.export_zappier_url':exportZap}})
        if syncZap
          Meteor.users.update(Meteor.userId(), {$set:{'profile.extensions.export_to_evernote.sync_zappier_url':syncZap}})
        if syncZap and exportZap
          Meteor.users.update(Meteor.userId(), {$set:{'profile.extensions.export_to_evernote.vailable_plots': ['nsg_tree_plot'], 'profile.extensions.export_to_evernote.incompatible_node_shapes': ['nsl_url']}})

      return
