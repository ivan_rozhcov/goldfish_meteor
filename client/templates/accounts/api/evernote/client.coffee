Meteor.startup ->
  Tracker.autorun(->
    user = Meteor.user()
    if user?.profile?.evernote?
      evernoteApiParams = user.profile.evernote

      Session.set('evernoteOauthAccessToken', evernoteApiParams['accessToken'])
      Session.set('edamUserId', evernoteApiParams['id'])
      Session.set('edamExpires', evernoteApiParams['expiresAt'])
  )

# OAuth callback
callback = () ->
# Так как мы используем iron-router там все по другому нельзя напрямую callback вызвать
#  по этому через шаблон по этому this уже не тот
  pattern = new RegExp(/oauth_verifier=([^&=\?]+)/)
  verifier = document.URL.match(pattern)[1]

  if verifier.length == 0
    console.log("Missing OAuth verifier.")
    console.log(JSON.stringify(err))
    return

  Meteor.call( 'handleCallback', Meteor.userId(), localStorage.getItem('oauthToken'), localStorage.getItem('oauthTokenSecret'), verifier, (err, res) ->
    localStorage.removeItem('oauthToken')
    localStorage.removeItem('oauthTokenSecret')

    if err
      console.log(JSON.stringify(err))
      return

    unless res
      console.log("Couldn't get OAuth access token.")
      console.log(JSON.stringify(err))
      return

    Session.set('evernoteOauthAccessToken', res['oauthAccessToken'])
    Session.set('oauthAccessTokenSecret', res['oauthAccessTokenSecret'])
    Session.set('edamUserId', res['edamUserId'])
    Session.set('edamShard', res['edamShard'])
    Session.set('edamExpires', res['edamExpires'])
    Session.set('edamNoteStoreUrl', res['edamNoteStoreUrl'])
    Session.set('edamWebApiUrlPrefix', res['edamWebApiUrlPrefix'])
  )

# logout
logout = () ->
  Session.set('evernoteOauthAccessToken', undefined)
  Session.set('oauthAccessTokenSecret', undefined)
  Session.set('edamUserId', undefined)
  Session.set('edamShard', undefined)
  Session.set('edamExpires', undefined)
  Session.set('edamNoteStoreUrl', undefined)
  Session.set('edamWebApiUrlPrefix', undefined)
  Session.set('notebooks', undefined)
  Meteor.users.update(Meteor.userId(), {$set:{'profile.evernote':{}}})


Template.evernoteApiCallback.rendered = ->
  callback()
  Router.go('/account/profile-api/evernote')


Template.evernoteApi.helpers(
  'loggedIn': () ->
    Session.get('evernoteOauthAccessToken')
  'notebooks': () ->
    Session.get('notebooks')
  'oauthAccessToken': () ->
    Session.get('evernoteOauthAccessToken')
  'oauthAccessTokenSecret': () ->
    Session.get('oauthAccessTokenSecret')
  'edamNoteStoreUrl': () ->
    Session.get('edamNoteStoreUrl')
  'edamWebApiUrlPrefix': () ->
    Session.get('edamWebApiUrlPrefix')
  'edamUserId': () ->
    Session.get('edamUserId')
  'edamExpires': () ->
    Session.get('edamExpires')
)

Template.evernoteApi.events(
  'click .add-notebook' : (evt) ->
    console.log 'click .add-notebook', @guid
    Meteor.call("findNotes", Session.get('evernoteOauthAccessToken'), @guid, (err, res) ->
      if err
        console.log(JSON.stringify(err))
        return
      if res[1]
        if res[1].length > 50
          bootbox.confirm('Вы Уверены, что хотите добавить ноды?', (result) ->
            if result
              for node in res[1]
                getOrCreateEvernoteNode(node, {users: [Meteor.userId()]})
          )
        else
          for node in res[1]
            getOrCreateEvernoteNode(node, {users: [Meteor.userId()]})
    )

  'click .refresh' : (evt) ->
    if Meteor.user()?.profile?
      Meteor.call("listNotebooks", Meteor.user().profile.evernote.accessToken, (err, res) ->
        if err
          console.log(JSON.stringify(err))
          return
        Session.set('notebooks', res)
      )

  'click .login' : (evt) ->
    console.log 'click'
    evt.preventDefault()
    Meteor.call("startOAuth", (err, res) ->
      if err
        console.log(JSON.stringify(err))
        return
      console.log res
      localStorage.setItem('oauthToken', res['oauthToken'])
      localStorage.setItem('oauthTokenSecret', res['oauthTokenSecret'])
      window.location = res['authorizeUrl']
    )
    false
  'click .logout' : (evt) ->
    evt.preventDefault()
    logout()
    false
)


Template.evernoteApiProd.helpers(
  'loggedIn': () ->
    Meteor.user().profile.evernote.prod_token
)

Template.evernoteApiProd.events(
  'click .disconnect' : (evt) ->
    if Meteor.user().profile.evernote and Meteor.user().profile.evernote.prod_token
      bootbox.confirm('Отвязать аккаунт?', (result) ->
        if result
          Meteor.users.update(Meteor.userId(), {$unset:'profile.evernote.prod_token': '' })
      )
  'click .login' : (evt) ->
    bootbox.prompt "Введите апи ключ", (login) ->
      if login
        Meteor.users.update(Meteor.userId(), {$set:'profile.evernote.prod_token': login })
)