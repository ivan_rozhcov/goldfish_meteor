Template.embedlyApi.helpers(
  'loggedIn': () ->
    if Meteor.user().profile
      Meteor.user().profile.embedly.key
  'login': () ->
    if Meteor.user().profile
      Meteor.user().profile.embedly.key
)

Template.embedlyApi.events(
  'click .login' : (evt) ->
    bootbox.prompt "Введите ключ", (login) ->
      if login
        Meteor.users.update(Meteor.userId(), {$set:'profile.embedly.key': login })
)