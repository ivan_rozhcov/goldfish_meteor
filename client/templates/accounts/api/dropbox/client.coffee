Template.dropboxApi.events
  "click .login": (e) ->
      result = Meteor.call "dropboxStartOAuth"
      console.log result

# OAuth callback
dropboxCallback = () ->
  console.log 'callback'

Template.dropboxApiCallback.rendered = ->
  dropboxCallback()
  Router.go('/account/profile-api/dropbox')