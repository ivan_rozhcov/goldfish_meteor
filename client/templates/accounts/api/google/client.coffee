Meteor.startup ->
  Tracker.autorun(->
    if Meteor.user() and Meteor.user().services and Meteor.user().services.google
      evernoteApiParams = Meteor.user().services.google

      Session.set('googleOauthAccessToken', evernoteApiParams['accessToken'])
      Session.set('googleUserId', evernoteApiParams['id'])
      Session.set('googleExpires', evernoteApiParams['expiresAt'])
  )



Template.googleCalendar.helpers(
  'loggedIn': () ->
    Session.get('googleOauthAccessToken')
  'edamUserId': () ->
    Session.get('googleUserId')
  'edamExpires': () ->
    Session.get('googleExpires')
  'googleCalendars': () ->
    Session.get('googleCalendars')
)

Template.googleCalendar.events(
  'click .add-notebook' : (evt) ->
    console.log 'click .add-notebook'
    Meteor.call("findNotes", Session.get('evernoteOauthAccessToken'), @guid, (err, res) ->
      if err
        console.log(JSON.stringify(err))
        return
      Session.set('evernote_notes_list', res[0])
      Session.set('evernote_notes_list_whole', res[1])
      for node in res[1]
        getOrCreateEvernoteNode(node, {users: [Meteor.userId()]})
    )
  'click .refresh' : (evt) ->
    console.log 'click'
    Meteor.call("googleCalendarList", (err, res) ->
      if err
        console.log(JSON.stringify(err))
        return
      Session.set('googleCalendars', res)
    )
)