Template.hypothesApi.helpers(
  'loggedIn': () ->
    Meteor.user().profile.hypothes.login
  'login': () ->
    Meteor.user().profile.hypothes.login
)

Template.hypothesApi.events(
  'click .disconnect' : (evt) ->
    if Meteor.user().profile.hypothes and Meteor.user().profile.hypothes.login
      bootbox.confirm('Отвязать аккаунт?', (result) ->
        if result
          Meteor.users.update(Meteor.userId(), {$unset:'profile.hypothes': '' })
      )

  'click .login' : (evt) ->
    bootbox.prompt "Введите логин", (login) ->
      if login
        Meteor.users.update(Meteor.userId(), {$set:'profile.hypothes.login': login })
)