
Template.exportForm.events
  'change .plot-radio': (event)->
    console.log this.checked
    $("#plots-select").attr('disabled', not $('#existingPlot').prop("checked"))

  'submit form': (event)->
    event.preventDefault()
    event.stopPropagation()
    existingPlot = event.target.existingPlot.checked
    plotId = if existingPlot then event.target['plots-select'].value  else false
    createNewPlot = event.target.createNewPlot.checked
    addToInbox = event.target.addToInbox.checked


    guid = event.target['evernote-notebooks-select'].value
    filterById = event.target.filterById.value or null
    if filterById
      try
        [..., filterById, last] = filterById.split('/')
      catch err
        console.log 'ошибка при выделении id из url', err

    accessToken = Meteor.user().profile.evernote.accessToken
    console.log guid
    Meteor.call("findNotes", accessToken, guid, noteGuid: filterById, (err, res) ->
      if err
        console.log(JSON.stringify(err))
        return
      #добавляем много нодов
      if res[1]
        if res[1].length > 50
          bootbox.confirm('Вы Уверены, что хотите добавить ноды? (их больше 50)', (result) ->
            if result
              if createNewPlot
                plotId = createPlot('evernote import plot', 'nsg_graph_plot', [Meteor.userId()])
              if plotId
                addToInbox = false

              for node in res[1]
                nodeId = getOrCreateEvernoteNode(node, {users: [Meteor.userId()]}, {createInbox: addToInbox})
                if plotId
                  createNodeShapeFromNodeNew nodeId, plotId, null, null, null, null, has_new_version:true
                  node = Nodes.findOne nodeId
                  #создаем нодшейпы для вложений
                  console.log 'import node', node
                  if node.original_node and node.original_node.resources and node.original_node.resources.length
                    for attach in node.original_node.resources
                      title = "#{Meteor.user().profile.evernote.edamNoteStoreUrl.replace('notestore', 'res/')}#{attach.guid}"
                      attachNodeId = createExtenalNode(title, null, [Meteor.userId()],
                          title, attach.guid, 'url')
                      console.log 'Node created', attachNodeId
                      nodeShapeId = createNodeShapeFromNode(attachNodeId, plotId,50,50,null,null,'nsl_iframe')
                      console.log 'NodeShape created', nodeShapeId

              if plotId
                Router.go('plotTab', _id: plotId)
              else
                bootbox.alert "Успешно импортировани #{res[1].length} элементов"

          )
        else
          if createNewPlot
            plotId = createPlot('evernote import plot', 'nsg_graph_plot', [Meteor.userId()])
          if plotId
            addToInbox = false

          for node in res[1]
            nodeId = getOrCreateEvernoteNode(node, {users: [Meteor.userId()]}, {createInbox: addToInbox})
            if plotId
              createNodeShapeFromNodeNew nodeId, plotId, null, null, null, null, has_new_version:true
              node = Nodes.findOne nodeId
              #создаем нодшейпы для вложений
              console.log 'import node', node
              if node.original_node and node.original_node.resources and node.original_node.resources.length
                for attach in node.original_node.resources
                  url = "#{Meteor.user().profile.evernote.edamNoteStoreUrl.replace('notestore', 'res/')}#{attach.guid}"

                  if attach.mime.lastIndexOf('text', 0) == 0
                      createNodeWithNodeShapeNew(url, null, [Meteor.userId()], 50, 50, plotId, 150, 150)
                  else
                    if attach.mime.includes('image/')
                      createImageNode(url, 50, 50, plotId, null, null, url)
                    else
                      if attach.mime.includes('audio/')
                        createAudioNode(url, 50, 50, plotId, null, null, url)
                      else
                        if attach.mime.includes('video/')
                          createVideoNode(url, 50, 50, plotId, null, null, url)
                        else
                          attachNodeId = createExtenalNode(url, null, [Meteor.userId()], url, attach.guid, 'url')
                          createNodeShapeFromNode(attachNodeId, plotId, 50, 50, 150, 50, 'nsl_iframe')

          if plotId
            Router.go('plotTab', _id: plotId)
          else
            bootbox.alert "Успешно импортировани #{res[1].length} элементов"
    )



Template.evernoteNotebooks.onRendered ->
  $(document).ready( ->
    user = Meteor.user()
    if user?.profile?.evernote?.accessToken
      accessToken = user?.profile?.evernote?.accessToken
      Meteor.call("listNotebooks", accessToken, (err, res) ->
        if err
          console.log(JSON.stringify(err))
          return
        console.log('notebooks!', res)
        Session.set('evernoteNotebooks', res)
        plotsSelect = $("#evernote-notebooks-select").select2(
          placeholder: "Find notebook...",
          allowClear: true
        )
      )
  )

Template.evernoteNotebooks.helpers
  'notebooks': () ->
    Session.get('evernoteNotebooks')