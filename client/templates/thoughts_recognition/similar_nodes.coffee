Template.similarNodes.helpers
  nodes: ->
    res = []
    ignoreIds = []
    for node in Nodes.find({similar_nodes: {$exists: true}}).fetch()
      unless node._id in ignoreIds
        similarNodes = Nodes.find(_id: $in: node.similar_nodes).fetch()
        _.each(similarNodes, (itm) -> itm.similarity = node.similarities?[itm._id])
        node.similarNodes = similarNodes
        res.push node
        ignoreIds = ignoreIds.concat(node.similar_nodes)
    res
