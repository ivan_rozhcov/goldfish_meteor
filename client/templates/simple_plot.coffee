Template.simplePlot.helpers
  getPreviewImage: ->
    if @plot_preview_image
      Images.findOne(@plot_preview_image).url()
  getChangeTime: ->
    moment(@change_time).format('DD.MM.YYYY HH:mm') if @change_time