Template.layout.events

  #http://stackoverflow.com/questions/10453291/how-to-trigger-jquery-draggable-on-elements-created-by-templates
  "click #create_new_plot": (e) ->
    if Meteor.userId()
      bootbox.prompt 'Введите название нового плота', (title) ->
        if title
          plot = createPlot title, 'nsg_graph_plot', [Meteor.userId()]
          scrollToPlot(plot)

  "click #create_new_node": (e) ->
    if Meteor.userId()
      bootbox.prompt 'Введите содержание нода', (nodeText) ->
        if nodeText
          createNode(null, nodeText, [Meteor.userId()], null, without_node_shape: true)

  "change #plots": (e) ->
    plot = $("#plots").val()
    scrollToPlot(plot)

  "click #scroll-to-plot": (e) ->
    if $("#plots-lower").val()
      plot = $("#plots-lower").val()
      scrollToPlot(plot)

  "click #scroll-to-node": (e) ->
    plot = $("#nodeshapes").val()
    scrollToPlot(plot)

  "click #scroll-to-link": (e) ->
    plot = $("#linkshapes").val()
    scrollToPlot(plot)

  #общая логика - в выпадашках в футере
  'click .nodeshape_scroll':  (e,t) ->
    showNodeShape(getNodeShapeById(@_id))
    scrollToNodeShape(@_id, 150)
    setSelectedNodeShapeIdGlobal(@_id)
    setSelectedNodeShapeId(@plot_id, @_id)