Template.showPins.helpers
  actions: ->
    pins = Plots.findOne(system: true, system_type: 'pins', users: Meteor.userId())
    if pins
      findNodesForPlot(pins).fetch()


Template.showPins.events
  'click .nodeshape_delete':  (e,t) ->
    trashNodeShape(@node_shape_id)

