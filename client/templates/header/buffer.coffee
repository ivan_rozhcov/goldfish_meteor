Template.showBuffer.helpers
  actions: ->
    Session.get 'bufferedNodeShapes'


Template.showBuffer.events
  'click .nodeshape_delete':  (e,t) ->
    buffer = Session.get 'bufferedNodeShapes'
    toDel = @node_shape_id
    newArr = buffer.filter (word) -> word isnt toDel
    Session.set 'bufferedNodeShapes', newArr