Template.link_shape.helpers divide: (x1, x2, divider) ->
  x1 - (x1 - x2) / divider


Template.link_shape.helpers
  istmpLinkShape :  ->
    true  if @type in ["tmp-link-show", "tmp-link"]
  isAutoLinkShape :  ->
    true  if @type in ["tmp-link-show-auto", "tmp-link-auto"]

Template.link_shape_text.helpers
  divide: (x1, x2, divider) ->
    x1 - (x1 - x2) / divider

  istmpLinkShape :  ->
    true  if @type in ["tmp-link-show", "tmp-link"]
  isAutoLinkShape :  ->
    true  if @type in ["tmp-link-show-auto", "tmp-link-auto"]


Template.link_shape_halos.events
  "click .halo-delete-link": (e) ->
    console.log 'click .halo-delete-link'
    self = @
    plotIds = _.unique LinkShapes.find(link_id: @link_id).map( (l) -> l.plot_id)
    plotsCount = Plots.find(_id: $in: plotIds).count()
    bootbox.confirm("Вы уверены, что хотите удалить связь навсегда? Связь используется на #{plotsCount} плотах", (result) ->
      if result
        Meteor.call 'deleteLink', self.link_id
    )