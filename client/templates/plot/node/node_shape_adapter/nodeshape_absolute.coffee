Template.nsa_graph_absolute.helpers
  # где такое жа для авто нодов?
  istmpNodeShape : ->
    true if @type in ["tmp-node-show","tmp-node"]

Template.nsa_graph_absolute.events

  "click .node-shape-data-icon": (e, t) ->
    $('.node-context-menu').hide()
    $('.node-type-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()
    contextMenu = $("#node-data-menu-#{@_id}")
    contextMenu.show()
    e.stopPropagation()
    e.preventDefault()

  "click .node-shape-widget-type-icon": (e, t) ->
    $('.node-data-menu').hide()
    $('.node-type-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()
    contextMenu = $("#node-shape-widget-menu-#{@_id}")
    contextMenu.show()
    e.stopPropagation()
    e.preventDefault()

  "click .node-type-icon": (e, t) ->
    $('.node-context-menu').hide()
    $('.node-data-menu').hide()
    $('.node-type-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()
    contextMenu = $("#node-type-menu-#{@_id}")
    contextMenu.show()
    e.stopPropagation()
    e.preventDefault()

  "click .node-shape-type-icon": (e, t) ->
    $('.node-data-menu').hide()
    $('.node-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()
    contextMenu = $("#node-shape-type-menu-#{@_id}")
    contextMenu.show()
    e.stopPropagation()
    e.preventDefault()

  "click .node-shape-proto-icon": (e, t) ->
    $('.node-context-menu').hide()
    $('.node-data-menu').hide()
    $('.node-type-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()
    contextMenu = $("#node-proto-#{@_id}")
    contextMenu.show()
    e.stopPropagation()
    e.preventDefault()

  "click #make-fullscreen": (e, t) ->
    $('.node-context-menu').hide()
    $('.node-data-menu').hide()
    $('.node-type-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()
    Session.set("fullscreenNodeShapeId", @_id)
    e.stopPropagation()
    e.preventDefault()

  "click #make-fullscreen-2": (e, t) ->
    $('.node-context-menu').hide()
    $('.node-data-menu').hide()
    $('.node-type-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()
    if Session.get 'nodeShapePath'
      nodeShapePath = Session.get('nodeShapePath')
      nodeShapePath.push({id: @_id, title: (@node_attrs and @node_attrs.title) or 'untitled'})
      Session.set 'nodeShapePath', nodeShapePath
    else
      Session.set 'nodeShapePath', [{name: @_id, title: (@node_attrs and @node_attrs.title) or 'untitled'}]
    Router.go("/node-shape/#{@_id}/")

  "click #recreate-embedly": (e, t) ->
    console.log 'recreate-embedly'
    #  тут была проблема в том, что в plot - node attrs не содержится node_id "автоматических плотов"
    #  по этому их удаление пораждает бексонечный цикл - нужно удалять напрямую!!!
    Meteor.call 'recreateEmbedly', @node_id, (err, rez) ->
      if err
        alert 'Ошибка при удалении'

  "click #recreate-hypothes": (e, t) ->
    console.log 'recreate-hypothes'
    Meteor.call 'recreateHypothesis', @node_id, (err, rez) ->
      if err
        alert 'Ошибка при удалении'

  "click #edit-css-props": (e, t) ->
    if @node_type == 'adapter'
      console.log "To change plot settings type:\n Plots.update('#{@representing_plot_id}', {$set: {settings: {columns: ['title', 'content']}}})"
    if @styles then styles = @styles else styles = []
    stylesStr = "You can add your css styles like that:\n NodeShapes.update(\'#{@_id}\', {$addToSet: {\'styles\': {\'property\': \'background-color\', \'value\' : \'red\'}}})\n"
    for style in styles
      stylesStr += "#{style.property}: #{style.value}\n" if style.property
    console.log stylesStr
    e.stopPropagation()
    e.preventDefault()

  "click .clone-node-shape": (e, t) ->
    node_id = t.data.node_id
    plotId = t.data.plot_id
    if node_id
      createNodeShapeFromNode node_id, plotId, null, null, null, null, $(e.currentTarget).data('type')

  "click .convert-node-shape": (e, t) ->
    changeNodeShapeWidgetType(t.data._id, 'view_type', $(e.currentTarget).data('type'))

  "click .convert-node-shape-edit": (e, t) ->
    plotId = t.data._id
    changeNodeShapeWidgetType(t.data._id, 'edit_type',  $(e.currentTarget).data('type'))

  "click .convert-widget-type": (e, t) ->
    changeNodeShapeWidget(t.data._id, $(e.currentTarget).data('widget-id'))

  "click .set-node-type": (e, t) ->
    changeNodeType(t.data, $(e.currentTarget).data('type'))
  "click .set-node-shape-type": (e, t) ->
    console.log 'set-node-shape-type', t.data
    changeNodeShapeType(t.data._id, $(e.currentTarget).data('type'))
  "click .set-node-proto": (e, t) ->
    changeNodeProto(t.data._id, $(e.currentTarget).data('type'))
  "click .set-node-data": (e, t) ->
    changeNodeData(t.data._id, $(e.currentTarget).data('type'))

#example
# NodeShapes.update('jDzm7qyDZmtnAoggm', {$set: {'styles': [{'property': 'background-color', 'value' : 'red'}, {'property': 'border-color', 'value' : 'beige'}]}})
Template.nsa_graph_absolute.rendered = ->
  nodeShape = @data
  nodeShapeDiv = $(@find("##{nodeShape._id}"))
  $(@find('.node-content')).resizable()
  #TODO not reactive yet!
  if nodeShape.styles
    for style in nodeShape.styles
      nodeShapeDiv.css("#{style.property}", "#{style.value}")  if style.property