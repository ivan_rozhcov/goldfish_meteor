Template.node_shape_widget.helpers

Template.render_simple_ns_widget.helpers
  dataContext: ->
    ns_logic: @view_type
    node_shape: @
    number: 1

Template.render_edit_widget.helpers
  dataContext: ->
    ns_logic: @edit_type
    node_shape: @
    number: 1

Template.render_edit_external_widget.helpers
  dataContext: ->
    ns_logic: @edit_type
    node_shape: @
    number: 1
