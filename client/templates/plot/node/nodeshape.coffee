Template.wigetTypeIcon.helpers
  widget_icon : ->
    widgetType = WIDGET_TYPE_ICONS[@view_type]
    return if widgetType then widgetType else WIDGET_TYPE_ICONS['default']

Template.ns_button.events "click button": (e) ->
  node = Nodes.findOne(@node_id)
  eval_ node.action_code
  e.stopPropagation()
  return
