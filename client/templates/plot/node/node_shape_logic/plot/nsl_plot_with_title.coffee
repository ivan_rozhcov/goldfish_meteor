attr = 'node_attrs.title'

Template.nsl_plot_with_title.helpers
  getTitle : ->
    deepFind(@node_shape, attr)

  isOpened: ->
    Session.get 'nodeShapePath'

  nodePlot: ->
    #защита от бесконечного цикла при рендеренги
    if @node_shape.representing_plot_id == @node_shape.plot_id
      return 'same'
    plot = Plots.findOne @node_shape.representing_plot_id
    plot.zoom = @node_shape.zoom
    plot

Template.nsl_plot_with_title.events
  "blur .nsl-plot-input": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)
  "mouseout .nsl-plot-input": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)