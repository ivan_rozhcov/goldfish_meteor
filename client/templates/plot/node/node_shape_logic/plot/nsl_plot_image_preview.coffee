
Template.nsl_plot_image_preview.helpers
  getPreviewImage: ->
    plot = Plots.findOne @node_shape.representing_plot_id
    if plot?.plot_preview_image?
      Images.findOne(plot.plot_preview_image)?.url()

  getChangeTime: ->
    moment(@change_time).format('DD.MM.YYYY HH:mm') if @change_time

  nodePlot: ->
    plot = Plots.findOne @node_shape.representing_plot_id
    plot.zoom = @node_shape.zoom
    plot