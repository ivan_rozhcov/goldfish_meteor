Template.nsl_plot_folder.helpers
  isOpened: ->
    Session.get 'nodeShapePath'
  nodePlot: ->
    plot = Plots.findOne @node_shape.representing_plot_id
    plot.zoom = @node_shape.zoom
    plot