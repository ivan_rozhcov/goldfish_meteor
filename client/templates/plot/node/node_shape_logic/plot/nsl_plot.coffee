
Template.nsl_plot.helpers
  isOpened: ->
    Session.get 'nodeShapePath'
  nodePlot: ->
    #защита от бесконечного цикла при рендеренги
    if @node_shape.representing_plot_id == @node_shape.plot_id
      return 'same'
    plot = Plots.findOne @node_shape.representing_plot_id
    plot.zoom = @node_shape.zoom
    plot