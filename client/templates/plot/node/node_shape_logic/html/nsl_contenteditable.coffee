#DEBOUNCE_KEYUP_TIME = 100
#
#debouncedKeyUp = _.debounce(((node_shape, event, attr) ->
##  console.log event.currentTarget.innerHTML
#  text = t.$(event.currentTarget).html()
#  t.$('input').val(text).trigger 'input'
#  return
#  saveNodeShapeAttribute(node_shape._id, attr, event.currentTarget.innerHTML)
##  node_shape.node_attrs.content = event.currentTarget.innerHTML
#  return
#), DEBOUNCE_KEYUP_TIME, true)

#https://github.com/meteor/meteor/issues/1964#issuecomment-87444235

attr = 'node_attrs.content'
Template.nsl_contenteditable.helpers
  getContent : ->
    deepFind(@node_shape, attr)

Template.nsl_contenteditable.events
  "keyup .nsl-content": (e, t) ->
    text = t.$(e.currentTarget).html()
    t.$('input').val(text).trigger 'input'
  "blur .nsl-content": (e, t) ->
    saveNodeShapeAttribute(t.data.node_shape, attr, t.$('input').val())
    return
  "mouseout .nsl-content": (e, t) ->
    saveNodeShapeAttribute(t.data.node_shape, attr, t.$('input').val())
    return


Template.nsl_contenteditable.rendered = ->
  elem = @$('.nsl-content').html(@data.node_shape.node_attrs.content)
  return

Template.nsl_contenteditable.created = ->
  tmpl = this
  @autorun ->
    if tmpl.lastNode
      tmpl.$('.nsl-content').html Template.currentData().node_shape.node_attrs.content
    return
  return