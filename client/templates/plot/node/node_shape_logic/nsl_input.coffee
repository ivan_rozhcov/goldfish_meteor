attr = 'node_attrs.content'
Template.nsl_input.helpers
  getContent : ->
    deepFind(@node_shape, attr)

Template.nsl_input.events
  "blur textarea": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)
  "mouseout textarea": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)