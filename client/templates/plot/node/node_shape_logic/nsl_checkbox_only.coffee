attr = 'node_attrs.content'
attr1 = 'node_attrs.resolved'
Template.nsl_checkbox_only.helpers
  getContent : ->
    deepFind(@node_shape, attr)
  getCheckbox : ->
    deepFind(@node_shape, attr1)

Template.nsl_checkbox_only.events
  "change input": (event, template) ->
    saveNodeShapeAttribute(template.data.node_shape, attr1, event.target.checked)