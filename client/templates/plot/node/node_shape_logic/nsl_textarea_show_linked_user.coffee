attr = 'node_attrs.content'
Template.nsl_textarea_show_linked_user.helpers
  'node_shape_title': ->
    nodeShape = @node_shape
    getVirtualAttribute(nodeShape.node_id, 'title')['content']
  getContent : ->
    deepFind(@node_shape, attr)

Template.nsl_textarea_show_linked_user.events
  "keyup input": (event, template) ->
    nodeShape = template.data.node_shape
    #TODO эта шутка возвращает Node, а функция saveNodeShapeText, обновляет нодшейп - что то здесь не то
    saveNodeShapeText(getVirtualAttribute(nodeShape.node_id, 'title'), event, attr)

  "blur textarea": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)
  "mouseout textarea": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)