attr = 'node_attrs.title'

Template.nsl_video_title.events
  "blur input": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)
  "mouseout input": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)
