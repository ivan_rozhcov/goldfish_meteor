DEBOUNCE_KEYUP_TIME = 100

debouncedKeyUp = _.debounce(((id, event, attr) ->
  saveNodeShapeText(id, event, attr)
  return
), DEBOUNCE_KEYUP_TIME, true)

attr = 'node_attrs.content'
Template.nsl_textarea.helpers
  getContent : ->
    deepFind(@node_shape, attr)

Template.nsl_textarea.events
# TODO закоментировал тк все равно "глотает" буквы
#  "keyup textarea": (event, template) ->
#    debouncedKeyUp(template.data.node_shape._id, event, attr)
  "blur textarea": (event, template) ->
    debouncedKeyUp(template.data.node_shape, event, attr)
  "mouseout textarea": (event, template) ->
    debouncedKeyUp(template.data.node_shape, event, attr)
    NodeShapes.update
      _id: template.data.node_shape._id
    , $set: {scroll_x: $(event.currentTarget).scrollLeft(), scroll_y: $(event.currentTarget).scrollTop()}

#TODO перенести логику в другие шаблоны
Template.nsl_textarea.onRendered( ()->
  if @data.node_shape.scroll_y or @data.node_shape.scroll_x
    ta = @$('textarea')
    if @data.node_shape.scroll_y
      ta.scrollTop(@data.node_shape.scroll_y)
    if @data.node_shape.scroll_x
      ta.scrollTop(@data.node_shape.scroll_x)
)

#    debouncedKeyUp(template.data.scroll_y, event, attr)