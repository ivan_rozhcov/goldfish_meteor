Template.nsl_attribute_editor.helpers
  objectId: ->
    Template.instance().data.node_shape_id or Template.instance().data.ns_id

  getAttrs1: ->
#    console.log Template.instance().data, Template.instance().data.baseField
    inst = Template.instance().data
    rez = ''
    if inst.baseFieldParent
      rez += inst.baseFieldParent + '.'
    if inst.baseField
      rez += inst.baseField
    rez

  getAttrs :  ->
#    console.log @node_shape, @
    Object.keys(@node_shape)
  isObject :  ->
    @ and @.__proto__.toString() == "[object Object]"

addAttr = (template, fieldVal, val) ->
    field = ''
    if template.data.baseFieldParent
      field = "#{template.data.baseFieldParent}."
    field +=  if template.data.baseField then "#{template.data.baseField}.#{fieldVal}" else fieldVal
    id =  if template.data.node_shape._id then template.data.node_shape._id else template.data.node_shape_id
    if not id
      id = template.data.ns_id

    console.log id, field
    saveNodeShapeAttribute(NodeShapes.findOne(id), field, val)
    event.stopPropagation()

Template.nsl_attribute_editor.events
  "blur input": (event, template) ->
    console.log template.data.baseFieldParent
    value =  if event.target.value == 'empty_val' then null else event.target.value
    addAttr(template, @field, value)
  "mouseout input": (event, template) ->
    console.log template.data.baseFieldParent
    value =  if event.target.value == 'empty_val' then null else event.target.value
    addAttr(template, @field, value)

  "click .add-attribute": (event, template) ->
    bootbox.prompt "Укажите имя аттрибута", (name) ->
      if name
        addAttr(template, name, null)

  "click .add-object": (event, template) ->
    bootbox.prompt "Укажите имя аттрибута", (name) ->
      if name
        addAttr(template, name, {})