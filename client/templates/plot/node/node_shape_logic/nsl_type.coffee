attr = 'node_attrs.content'
attr1 = 'node_attrs.picture'
Template.nsl_type.helpers
  getPicture : ->
    deepFind(@node_shape, attr1)
  getContent : ->
    deepFind(@node_shape, attr)

Template.nsl_type.events
  "blur .content_text": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)
  "mouseout .content_text": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)

  "blur .picture": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr1)
  "mouseout .picture": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr1)


