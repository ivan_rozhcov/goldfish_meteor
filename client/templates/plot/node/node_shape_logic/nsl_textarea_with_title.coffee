attr = 'node_attrs.content'
attr1 = 'node_attrs.title'
Template.nsl_textarea_with_title.helpers
  getTitle : ->
    deepFind(@node_shape, attr1)
  getContent : ->
    deepFind(@node_shape, attr)

Template.nsl_textarea_with_title.events
  "blur input": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr1)
  "mouseout input": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr1)

  "blur textarea": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)
  "mouseout textarea": (event, template) ->
    debouncedSaveNodeShapeText(template.data.node_shape, event, attr)