Template.nsg_table_plot_full.helpers
  columns_names: ->
    if @settings and @settings.columns
      getName = (itm) ->
        itm['name']

      _.map(@settings.columns, getName)
    else
      Plots.update(@_id, {$set: {'settings.columns': [{name: "main", is_main: true}]}})
      ['main']

  node_shapes_rows: ->

    #освновная колонка, она и определят порядок и количество
    mainCol = _.find(@settings.columns, (itm)-> itm['is_main'])
    otherCols = _.map(_.filter(@settings.columns, (itm)-> not itm['is_main']), (itm)-> itm['name'])
    if mainCol
      cols = []
      #todo filter by links
      ns = findNodesForPlot(@)

      #1й столбец всегда есть - это ноды, далее описывается settings.columns
      mainNodeShapes = []
      for nodeShape in ns.fetch()
        hasParentFlag = false
        for linkName in otherCols
          if hasParent(nodeShape, linkName)
            hasParentFlag = true
            break
        unless hasParentFlag
          mainNodeShapes.push(nodeShape)

      cols.push mainNodeShapes

#      выводим значение нод шейов в связях
      for col in @settings.columns
        unless col['is_main']
          linkShapesNodeShapes = []
          for ns in mainNodeShapes
            linkShapesNodeShape = null
            #найти входящий линк
            children = getChildren(ns, col.name).fetch()
            if children
              linkShapesNodeShape = children[0]
            linkShapesNodeShapes.push linkShapesNodeShape
          cols.push linkShapesNodeShapes


      # транспонируем
      _.zip.apply(@, cols)
    else
      Plots.update(@_id, {$set: {'settings.columns': [{name: "main", is_main: true}]}})
      [findNodesForPlot(@)]


#перемещение
# создание новых нодов и тд
Template.nsg_table_plot_full.events
  'mouseover .list-item': (e) ->
    return  if checkPlotState(@plot_id, "dragging node")
    Session.set "selectedNodeId", @node_id
    setSelectedNodeShapeId(@plot_id, @_id)
    setEditingNodeShapeId(@plot_id, @_id)
    if @has_new_version
      NodeShapes.update
        _id: @_id
      ,
        $set:
          has_new_version: false
  "click .change-cols": (event, template) ->
    self = @
    initial = _.map(_.filter(@settings.columns, (itm)-> not itm['is_main']), (itm)-> itm.name).join()
    mainColumn = _.find(@settings.columns, (itm)-> itm['is_main'])
    mainColumn = if mainColumn  then mainColumn.name else ''
    html = '<div class="row">  ' + '<div class="col-md-12"> ' + '<form class="form-horizontal"> ' + '<div class="form-group"> ' + '<label class="col-md-4 control-label" for="main-col">Основной столбец</label> ' + '<div class="col-md-4"> ' + '<input id="main-col" name="main-col" type="text" placeholder="\n" class="form-control input-md" value="'+ mainColumn + '"> ' + '</div> ' + '</div> ' + '<div class="form-group"> ' + '<label class="col-md-4 control-label" for="columns">Все столбцы</label> ' + '<div class="col-md-4"> ' + '<input id="columns" name="main-col" type="text" placeholder="\n" class="form-control input-md" value="' + initial + '"> ' + '<span class="help-block">Все столбцы, через запятую - без пробелов</span> </div> ' + '</div> ' + '</form> </div>  </div>'
    bootbox.dialog
      title: "Укажите имена аттрибутов - без пробелов, разделитель запятая"
      message: html
      buttons: success:
        label: 'Разбить'
        className: 'btn-success'
        callback: ->
          mainColId = $('#main-col').val()
          columns = $('#columns').val()
          if columns
            columnObjs = _.map(columns.split(','), (el) ->
              name: el
            )
          else
            columnObjs = []
          #is_main всегда вперед
          columnObjs.unshift(
            name: mainColId,
            is_main: true
          )

          if columnObjs
            Plots.update(self._id, {$set: {settings: {columns: columnObjs}}})

