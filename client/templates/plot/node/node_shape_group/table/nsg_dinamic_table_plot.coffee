Template.nsg_dynamic_table_plot.helpers
  node_shapes: ->
    findNodesForPlot(@, null, {order: -1})

Template.nsg_dynamic_table_plot.events
  "click button": (event, template) ->
    self = @
    bootbox.prompt "Укажите имена аттрибутов - без пробелов, разделитель запятая", (columns) ->
      if columns
        Plots.update(self._id, {$set: {settings: {columns: columns.split(',')}}})

