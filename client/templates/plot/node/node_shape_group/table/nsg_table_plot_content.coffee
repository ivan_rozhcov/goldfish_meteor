getNodeShapeCols = (plot_id) ->
  linkShapes = LinkShapes.find(
    type:
      $nin: [
        "tmp-link"
        "tmp-link-show"
      ]

    plot_id: plot_id
  ).fetch()
  result = []
  for i of linkShapes
    result.push linkShapes[i].node_shape_to_id
  NodeShapes.find
    type:
      $nin: [
        "tmp-node"
        "tmp-node-show"
      ]

    plot_id: plot_id
    _id:
      $in: result
  ,
    sort: [
      "order"
      "asc"
    ]

Handlebars.registerHelper "eq2", (x1, x2) ->
  x1 is x2

Template.nsg_table_plot_content.getLinks = ->
  nodeShapeCols = getNodeShapeCols(@plot_id).fetch()
  result = []
  for i of nodeShapeCols
    nodeShapeToId = nodeShapeCols[i]._id
    linkShape = LinkShapes.findOne(
      type:
        $nin: [
          "tmp-link"
          "tmp-link-show"
        ]

      plot_id: @plot_id
      node_shape_from_id: @_id
      node_shape_to_id: nodeShapeToId
    )
    linkShape = empty: true  unless linkShape
    result.push linkShape
  result

Template.nsg_table_plot_content.node_shapes_rows = ->
  linkShapes = LinkShapes.find(
    type:
      $nin: [
        "tmp-link"
        "tmp-link-show"
      ]

    plot_id: @_id
  ).fetch()
  result = []
  for i of linkShapes
    result.push linkShapes[i].node_shape_from_id
  NodeShapes.find
    type:
      $nin: [
        "tmp-node"
        "tmp-node-show"
      ]

    plot_id: @_id
    _id:
      $in: result
  ,
    sort: [
      "order"
      "asc"
    ]


Template.nsg_table_plot_content.node_shapes_cols = ->
  getNodeShapeCols @_id

attr = 'node_attrs.content'
Template.nsg_table_plot_content.events
  "keyup .cell-node textarea": (event, template) ->
    Session.set "editing_nodeshape_value", event.target.value
    setEditingNodeShapeId(@plot_id, @_id)
    saveNodeShapeText(template.data.node_shape, event, attr)