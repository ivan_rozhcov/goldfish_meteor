Template.nsg_graph_main_list.helpers
  editableNodeShape: ->
    if getEditingNodeShapeId(@_id)
      nodeShape = NodeShapes.findOne(
        _id: getEditingNodeShapeId(@_id),
        plot_id: @_id
      )
      if nodeShape
        nodeShape.ExternalEditing = true
        nodeShape

#Template.nsg_graph_main_list.helpers(NSG_GRAPH_PLOT_HELPERS)


Template.nsg_graph_main_list.events
  "click .close-edit": (e) ->
    setEditingNodeShapeId(@_id, null)