Template.nsg_graph_plot_ext_edit.helpers(NSG_GRAPH_PLOT_HELPERS)


#только при рендиринге
Template.nsg_graph_plot_ext_edit.rendered = ->
  $("#nodes").val Session.get("selectedNodeId")
  NodeShapes.find(plot_id: @data._id).forEach (node_shape) ->
    $("#" + node_shape._id).find("textarea").highlightTextarea words: node_shape.related_texts  if node_shape.related_texts and node_shape.related_texts.length


plotEvents = _.extend(_.clone(NSG_GRAPH_PLOT_EVENTS),
  "click .close-edit": (e) ->
    setEditingNodeShapeId(@plot_id, null)
)

Template.nsg_graph_plot_ext_edit.events(plotEvents)