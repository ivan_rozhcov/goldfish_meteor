DEBOUNCE_RESIZE_TIME = 150

@NSG_GRAPH_PLOT_HELPERS =
  node_shapes: ->
    findNodesForPlot(@)

  link_shapes: ->
    findLinksForPlot(@)


  related_node_shapes: ->
    if Session.get "#{@_id}_show_tmp_nodes"
      NodeShapes.find
        type:
          $in: [
            "tmp-node"
            "tmp-node-show"
          ]
        plot_id: @_id
    else
      NodeShapes.find
        type: "tmp-node-show"
        plot_id: @_id

  auto_node_shapes: ->
    if Session.get "#{@_id}_show_tmp_nodes"
      NodeShapes.find
        type:
          $in: [
            "tmp-node-auto"
            "tmp-node-auto-show"
          ]
        plot_id: @_id
    else
      NodeShapes.find
        type: "tmp-node-auto-show"
        plot_id: @_id

  completed_node_shapes: ->
    if Session.get "#{@_id}_hide_completed_nodes"
      []
    else
      NodeShapes.find
        'node_attrs.resolved': true
        plot_id: @_id

  related_link_shapes: ->
    if Session.get "#{@_id}_show_tmp_nodes"
      LinkShapes.find
        type:
          $in: [
            "tmp-link"
            "tmp-link-show"
          ]
        plot_id: @_id
    else
      LinkShapes.find
        type: "tmp-link-show"
        plot_id: @_id

  autoLinkShapes: ->
    if Session.get "#{@_id}_show_tmp_nodes_auto"
      LinkShapes.find
        type:
          $in: [
            "tmp-link-auto"
            "tmp-link-show-auto"
          ]
        plot_id: @_id
    else
      LinkShapes.find
        type: "tmp-link-show-auto"
        plot_id: @_id

  completedLinkShapes: ->
    if Session.get "#{@_id}_hide_completed_nodes"
      []
    else
      completedNodeShapeIds = NodeShapes.find('node_attrs.resolved': true, plot_id: @_id, {fields: _id: 1}).map (itm) -> itm._id
      LinkShapes.find
        plot_id: @_id
        $or: [
          { node_shape_from_id: $in: completedNodeShapeIds }
          { node_shape_to_id: $in: completedNodeShapeIds }
        ]

        
  editableNodeShape: ->
    if getEditingNodeShapeId(@_id)
      nodeShape = NodeShapes.findOne(
        _id: getEditingNodeShapeId(@_id),
        plot_id: @_id
      )
      if nodeShape
        nodeShape.ExternalEditing = true
        nodeShape

Template.nsg_graph_plot.helpers(NSG_GRAPH_PLOT_HELPERS)


#только при рендиринге
Template.nsg_graph_plot.rendered = ->
  $("#nodes").val Session.get("selectedNodeId")
  NodeShapes.find(plot_id: @data._id).forEach (node_shape) ->
    $("#" + node_shape._id).find("textarea").highlightTextarea words: node_shape.related_texts  if node_shape.related_texts and node_shape.related_texts.length

  # TODo кажется тормозит если много iframeов
#  createPlotPreviewImage(@data._id)

#Template.nsg_graph_plot.destroyed = ->
#  @computation.stop()  if @computation
#  return


Template.nsg_graph_plot.events(NSG_GRAPH_PLOT_EVENTS)