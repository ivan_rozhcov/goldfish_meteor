Template.nsg_list.helpers
  items: ->
    Plots.find(users: Meteor.userId()
    ,
      sort:
        change_time: -1
      limit: 3
    )
  plotCount: ->
    Plots.find(users: Meteor.userId()).count()

Template.nsg_list.events
    'mouseover .tab': (e) ->
        Session.set("selectedPlotId", @_id)
        #для открытия новой вкладки
        Session.set("plotPath", @_id)

    'contextmenu .plot-placeholder': (e) ->
      e.preventDefault();
      contextMenu = $("#new-node-menu")
      coords = coordsRelativeToElement(e.currentTarget, e)
      contextMenu.css('top', "#{coords.y}px").css('left', "#{coords.x}px")
      contextMenu.toggle()

    'click .plot-placeholder': (e) ->
      e.preventDefault();
      contextMenu = $("#new-node-menu")
      coords = coordsRelativeToElement(e.currentTarget, e)
      contextMenu.css('top', "#{coords.y}px").css('left', "#{coords.x}px")
      contextMenu.toggle()

Template.plotTab.rendered = ->
    Session.get 'nodeShapePath'