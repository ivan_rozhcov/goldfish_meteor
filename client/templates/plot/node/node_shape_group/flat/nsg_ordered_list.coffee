Template.nsg_ordered_list.helpers
  node_shapes: ->
    findNodesForPlot(@)
  editableNodeShape: ->
    if getEditingNodeShapeId(@_id)
      nodeShape = NodeShapes.findOne(
        _id: getEditingNodeShapeId(@_id),
        plot_id: @_id
      )
      if nodeShape
        nodeShape.ExternalEditing = true
        nodeShape



#перемещение
# создание новых нодов и тд
Template.nsg_ordered_list.events
  "mouseover .list-node": (e) ->
    setSelectedNodeShapeId(@plot_id, @_id)
    Session.set "selectedNodeId", @node_id

  "click .list-node": (e) ->
    setSelectedNodeShapeId(@plot_id, @_id)
    Session.set "selectedNodeId", @node_id
    setEditingNodeShapeId(@plot_id, @_id)