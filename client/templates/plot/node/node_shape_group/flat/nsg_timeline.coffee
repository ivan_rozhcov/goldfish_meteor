Template.nsg_timeline.rendered = ->
  self = @
  plot = @data
  console.log @
  unless @drawTimeline
    self.drawTimeline = Tracker.autorun(->
      nodeShapes = findNodesForPlot(plot).fetch()
      dates = []
      for nodeShape in nodeShapes
        dates.push(
#          startDate: '2012,1,26'
          startDate: moment(nodeShape.create_time).format('YYYY,M,D')
          headline: nodeShape.node_attrs.title
          text: nodeShape.node_attrs.content
        )
      dataObject = 'timeline':
        'headline': plot.title
        'type': 'default'
        'text':  plot.content
        'startDate': '2012,1,26'
        'date': dates
      createStoryJS
        type: 'timeline'
        height: '600'
        source: dataObject
        embed_id: 'timelineObject'
    )

Template.nsg_timeline.destroyed = ->
    @drawTimeline.stop()

