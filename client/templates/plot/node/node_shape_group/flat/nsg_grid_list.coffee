Template.nsg_grid_list.helpers
  itemsPlots: ->
    items :Plots.find
        users: Meteor.userId()
        $or: [
          {
            system: false
          }
          {
            system: null
          }
        ],
          sort:
            change_time: 1,
            limit: 6

  plotCount: ->
    Plots.find(users: Meteor.userId()).count()

Template.nsg_grid_list.events
    'mouseover .tab': (e) ->
        Session.set("selectedPlotId", @_id)
        #для открытия новой вкладки
        Session.set("plotPath", @_id)

    'contextmenu .plot-placeholder': (e) ->
      e.preventDefault();
      contextMenu = $("#new-node-menu")
      coords = coordsRelativeToElement(e.currentTarget, e)
      contextMenu.css('top', "#{coords.y}px").css('left', "#{coords.x}px")
      contextMenu.toggle()

    'click .plot-placeholder': (e) ->
      e.preventDefault();
      contextMenu = $("#new-node-menu")
      coords = coordsRelativeToElement(e.currentTarget, e)
      contextMenu.css('top', "#{coords.y}px").css('left', "#{coords.x}px")
      contextMenu.toggle()

#Template.plotTab.rendered = ->
#    Session.get 'nodeShapePath'


# Вот как надо поступать чтобы можно было "затмевать" извне
Template.nsg_grid_list_ns.helpers
  items: ->
    if @items
      @items
    else
      findNodesForPlot(@, null, { order: -1 })