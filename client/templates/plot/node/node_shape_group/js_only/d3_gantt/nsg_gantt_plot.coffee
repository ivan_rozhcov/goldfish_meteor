#Plots.update('xGQxpTdY98PxSCjM6', {$set: {type: 'nsg_gantt_d3_plot'}})
drawTimeLine = () ->
  draw = ->
    console.log "drawing"
    svg.select("g.x.axis").call xAxis
    return
  zoom = ->
    console.log "zooming"
#    d3.event.transform x # TODO d3.behavior.zoom should support extents
    draw()
    return
  w = 1200
  h = 500
  x = d3.time.scale().range([
    0
    w
  ])
  xAxis = d3.svg.axis().scale(x).orient("bottom").tickSize(-h, 0).tickPadding(6)
  svg = d3.select("#timeline").append("svg:svg").attr("width", w).attr("height", h + 50).append("svg:g")
  svg.append("svg:g").attr("class", "x axis").attr "transform", "translate(0," + h + ")"
  svg.append("svg:rect").attr("class", "pane").attr("width", w).attr("height", h)
#  .call d3.behavior.zoom().on("zoom", zoom)
  x.domain [
    new Date(1999, 0, 1)
    new Date(2014, 0, 0)
  ]

  svg.call d3.behavior.zoom().on("zoom", zoom)
  draw()

Template.nsg_gantt_d3_plot.rendered = ->
  domElement = "#timeline"
  self = @
  @node = @find('.d3-canvas')
  console.log @
  unless @drawTimeline
    self.drawTimeline = Tracker.autorun(->
      drawTimeLine()
    )

Template.nsg_gantt_d3_plot.destroyed = ->
    @drawTimeline.stop()