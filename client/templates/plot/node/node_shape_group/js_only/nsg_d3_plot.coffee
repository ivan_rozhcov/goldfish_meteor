#TODO сделать более сложный пример - типа этого http://bl.ocks.org/mbostock/4063318
#только при рендиринге
Template.nsg_d3_plot.rendered = ->
  self = @
  @node = @find('.d3-canvas')
  console.log @
  unless @drawTimeline
    self.drawTimeline = Tracker.autorun(->
      nodeShapes = NodeShapes.find(plot_id: self.data._id).fetch()
      d3.select('.d3-canvas').selectAll("p").data(nodeShapes).enter().append("rect").attr("width", (d) ->d.width).attr("height", (d) ->d.height).attr("x", (d) ->d.x).attr("y", (d) ->d.y).text (d) ->
        "I’m number " + d + "!"
    )

Template.nsg_d3_plot.destroyed = ->
    @drawTimeline.stop()