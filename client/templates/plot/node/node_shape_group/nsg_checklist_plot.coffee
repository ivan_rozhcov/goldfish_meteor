Template.nsg_checklist_plot.created = ->
  @mainLinkType = new ReactiveVar(@data.main_link_type or 'parent')

Template.nsg_checklist_plot.onRendered ->
  self = @
  if self.data.no_tolerance_element
    console.log 'no_tolerance_element'
    $('#treeData').nestedSortable
      handle: '.tree-move'
      listType: 'ul'
      items: 'li'
  #    toleranceElement: ' .tree-move'
      relocate: (event, object)->
        hiered = $('#treeData').nestedSortable('toHierarchy')

        console.log('Relocated item', event, object, hiered)
        #resave tree
        syncChildren(hiered, self.mainLinkType.get())
  else
    $('#treeData').nestedSortable
      handle: '.tree-move'
      listType: 'ul'
      items: 'li'
      toleranceElement: ' .tree-move'
      relocate: (event, object)->
        hiered = $('#treeData').nestedSortable('toHierarchy')

        console.log('Relocated item', event, object, hiered)
        #resave tree
        syncChildren(hiered, self.mainLinkType.get())
  return

Template.nsg_checklist_plot.helpers
  items: ->
    ns = findNodesForPlot(@)

    parentNodeShapes = []
    for nodeShape in ns.fetch()
#      console.log nodeShape, hasParent(nodeShape)
      unless hasParent(nodeShape, Template.instance().mainLinkType.get())
        nodeShape.children =
        parentNodeShapes.push(nodeShape)

    parentNodeShapes


Template.treeNodeTemplate.helpers
  hasChildren: ->
    hasChildren(@, Template.instance().mainLinkType.get())
  children: ->
    getChildren(@, Template.instance().mainLinkType.get())

Template.treeNodeTemplate.created = ->
  plot = Plots.findOne(@data.plot_id)
  @mainLinkType = new ReactiveVar(plot.main_link_type or 'parent')

#перемещение
# создание новых нодов и тд

plotEvents = _.extend(_.clone(NSG_GRAPH_PLOT_EVENTS),
  'click .move-left': (e,t) ->
    console.log 'move-left', @, e, t
    if hasParent(@, t.mainLinkType.get())
      console.log 'remove-parent'
    else
      console.log 'не могу сдвинуть влево'
  'click .move-right': (e) ->
    console.log 'move-right', @
    if hasParent(@, t.mainLinkType.get())
      console.log 'has-parent'
    else
      console.log 'add-parent'
    if hasChildren(@, t.mainLinkType.get())
      console.log 'add-parent-to-children'
  'drop #tree': (ev, template) ->
    plotId = @_id
    ev.stopPropagation()
    ev.preventDefault()
    files = ev.originalEvent.dataTransfer.files
    console.log(ev.originalEvent.dataTransfer)
    for f in files
#TODO тут конечно нужна интеграция с dropbox но пока с ней разобраться не получилось
# тут http://www.html5rocks.com/en/tutorials/file/filesystem/#toc-dir вроде вы то, что надо но с другой сторны - это все умеет dropbox

#            // Read the File objects in this FileList.
      console.log(f.type, f.size, f.name)
      if f.type.lastIndexOf('text', 0) == 0
        console.log('text')
        createNodeWithNodeShapeNew(f.name, null, [Meteor.userId()], 50, 50, @_id, 150, 150)
      else
        if f.type.includes('image/')
          nsId = createImageNode(f.name, 50, 50, @_id, null, null, "/#{f.name}")
        else
          if f.type.includes('audio/')
            nsId = createAudioNode(f.name, 50, 50, @_id, null, null, "/#{f.name}")
          else
            if f.type.includes('video/')
              nsId = createVideoNode(f.name, 50, 50, @_id, null, null, "/#{f.name}")
            else
              if f.type.includes('pdf')
                nsId = createPdfNode(f.name, 50, 50, @_id, null, null, "/#{f.name}")
              else
                nodeId = createExtenalNode(f.name, null, [Meteor.userId()], "/#{f.name}", f.name, 'url')
                nsId = createNodeShapeFromNode(nodeId, @_id, 50, 50, 150, 50, 'nsl_iframe')

        Files.insert(f, (err, fileObj) ->
          console.log 'got fileObj', fileObj
          unless err
#Так как url() тут еще может быть недоступен - делам его руками, чтобы не усложнять и не выностить на сервер
            if fileObj.original and fileObj.original.name
#аттрибуты нужно так обновлять

              saveNodeShapeAttribute(NodeShapes.findOne(nsId), 'node_attrs.node_external_uri', "/cfs/files/files/" + fileObj._id + '/' + fileObj.original.name)
              # иногда картинка не успевает отобразиться
              Router.go 'plotTab', _id: plotId
        )
    #            Сейчас не используется - в паблик заливать плохая идея


    if not files.length
#            скорей всего дран н дроп - обработаем
      if ev.originalEvent.dataTransfer
        nodeShapeId = ev.originalEvent.dataTransfer.getData("node-shape-id")
        nodeId = ev.originalEvent.dataTransfer.getData("node-id")
        plotId = ev.originalEvent.dataTransfer.getData("plot-id")
        console.log plotId
        if nodeShapeId
          console.log 'node shape', nodeShapeId
          coords = coordsRelativeToElement(event.currentTarget, event)
          Meteor.call 'cloneNodeShape', nodeShapeId, plot_id:@_id, x:coords.x, y:coords.y
        else if nodeId
          console.log 'node', nodeId
          coords = coordsRelativeToElement(event.currentTarget, event)
          Meteor.call 'createNodeShapeFromNode', nodeId, @_id, coords.x, coords.y
        else if plotId
          coords = coordsRelativeToElement(event.currentTarget, event)
          Meteor.call 'createNodeShapeAdapter', plotId, coords.x, coords.y, @_id, null, null
        else
          console.log 'not files.length', ev.originalEvent.dataTransfer.getData("text")

      else
        console.log 'no data', ev.originalEvent
        
  'mouseover .list-item': (e) ->
    return  if checkPlotState(@plot_id, "dragging node")
    Session.set "selectedNodeId", @node_id
    setSelectedNodeShapeId(@plot_id, @_id)
    setEditingNodeShapeId(@plot_id, @_id)
    if @has_new_version
      NodeShapes.update
        _id: @_id
      ,
        $set:
          has_new_version: false

  "dblclick .content": (event, template) ->

    currentPlot = Plots.findOne @_id
    console.log currentPlot
    coords = coordsRelativeToElement(event.currentTarget, event)

    widgetType = getPlotDefautWidgetType currentPlot

    createNodeWithNodeShape null, null, [Meteor.userId()], coords.x, coords.y, currentPlot._id, widgetType.width, widgetType.height, widgetType.default_content_type, widgetType.default_widget

  "contextmenu .list-item": (e) ->
    ss = ServerSettings.findOne()
    unless ss.show_default_menu
      e.preventDefault()
      e.stopPropagation()
    console.log 'node menu'
    $('.node-context-menu').hide()
    $('.node-data-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()

    contextMenu = $("#node-menu-#{@plot_id}")
    coords = relativePosition($($("##{@_id}")).parent().parent().parent(), $($("##{@_id}")))
    contextMenu.css('top', "#{coords.top}px").css('left', "#{coords.left}px")
    console.log coords, contextMenu
    contextMenu.toggle()

  "click .tree-menu": (e, t) ->
    $('.node-data-menu').hide()
    $('.node-type-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()
    contextMenu = $("#node-shape-widget-menu-#{@_id}")
    contextMenu.show()
    e.stopPropagation()
    e.preventDefault()
)

Template.nsg_checklist_plot.events(plotEvents)
