#не хочет нормально инстанцироваться + не понятно как дестроить
# топорный вариант сделать чтобы всегода подказывался, а не по клику
Template.ns_ckeditor.events
  "mouseover .node-input": (e) ->
    console.log 'mouse over', @
    return  unless getEditingNodeShapeId(@plot_id) is @_id
    if getEditingNodeShapeId(@plot_id) and not Session.get("ckeditor_initialised")
      CKEDITOR.replace getEditingNodeShapeId(@plot_id) + "-ckeditor-node-input",
        toolbar: [[ # Defines toolbar group without name.
          "Cut"
          "Copy"
          "Paste"
          "PasteText"
          "PasteFromWord"
          "-"
          "Undo"
          "Redo"
        ]]

      Session.set "ckeditor_initialised", true
    return

#TODO не доедлано - не запускается + почему то в типе указан не nsl_ckeditor, a graph_ed_ckedit