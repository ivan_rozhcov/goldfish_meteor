Template.otherPanel.helpers
  settings: ->
    position: "bottom"
    limit: 5
    rules: [

      #todo по идее тут нужно просто правильный subscription выбирать
      collection: Plots
      field: "title"
      options: ""
      matchAll: true
      template: Template.plotsAutoComplete
      selector: (match) ->
        regex = new RegExp(match, 'i')
        return {$or: [{'content': regex}, {'title': regex}], users: Meteor.userId()}
#TODO так не работет - видимо придется искать другой пакет
#    collection: NodeShapes
#    field: "content"
#    options: ""
#    matchAll: true
#    selector: (match) ->
#      regex = new RegExp(match, 'i')
#      return {$or: [{'content': regex}, {'title': regex}]}

    template: Template.plotAutoComplete
    ]

Template.otherPanel.events
  "autocompleteselect input": (e, t, d)->
    scrollToPlot(d._id)