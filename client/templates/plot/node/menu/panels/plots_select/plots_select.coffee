Template.plotsSelect.helpers(
  plots: ->
    Plots.find users: Meteor.userId()
)

Template.plotsSelect.rendered = ->
  $(document).ready( ->
    plotsSelect = $(".action-select-plots").select2(
      placeholder: "Find plot...",
      allowClear: true
      templateResult: formatStatePlot
    )

    plotsSelect.on 'select2:selecting', (e) ->
      console.log e
      target = e.params.args.originalEvent.target
      if target
        #если класс info - значит мы нажали на ссылку и надо на нее перейти
        if $(target).hasClass('info')
          url = e.params.args.originalEvent.target.href
          if url
            Router.go(url)
  )
