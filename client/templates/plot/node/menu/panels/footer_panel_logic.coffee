@FOOTER_HELPERS =
  actions: ->
    Actions.find(
      user_id: Meteor.userId()
      action_type: ACTION_TYPE_VIEW,
        sort: { date_added: -1 }
        limit: 20
      ).fetch().reverse()
  nodes: ->
    Nodes.find users: Meteor.userId()
  links: ->
    Links.find users: Meteor.userId()
  plots: ->
    Plots.find users: Meteor.userId()
  autoUpdateNode: ->
    Session.get('autoSelectNode')
  autoUpdateLink: ->
    Session.get('autoSelectLink')
  getInboxNodesCount: ->
    Counts.get('inbox-count')

  getTrashNodesCount: ->
    Counts.get('trash-count')

  nodeSearchSettings: ->
    limit: 15
    position: "top"
    rules: [
      subscription: 'autocomplete-nodes'
      options: ""
      matchAll: true
      selector: (match) ->
        {$or: [{'attrs.content': $regex: match, '$options': 'i'}, {'attrs.title': $regex: match, '$options': 'i'}]}

      template: Template.simpleNodeWithNodeShapes
      collection: 'NodeShapes'
    ]


  plotSearchSettings: ->
    limit: 7
    position: "top"
    rules: [
      subscription: 'autocomplete-plots'
      options: ""
      matchAll: true
      selector: (match) ->
        {'title': $regex: match, '$options': 'i'}

      template: Template.simplePlot
      collection: 'Plots'
    ]



Template.showHistory.helpers(FOOTER_HELPERS)

Template.otherFooter.helpers(FOOTER_HELPERS)
Template.plotsFooter.helpers(FOOTER_HELPERS)
Template.plotFooter.helpers(FOOTER_HELPERS)
