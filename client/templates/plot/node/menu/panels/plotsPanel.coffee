Template.plotsPanel.helpers
  nodes: ->
    Nodes.find users: Meteor.userId()
  links: ->
    Links.find users: Meteor.userId()
  plots: ->
    Plots.find users: Meteor.userId()
  plotCount: ->
    Plots.find(users: Meteor.userId()).count()
  settings: ->
    position: "bottom"
    limit: 5
    rules: [

      #todo по идее тут нужно просто правильный subscription выбирать
      collection: Plots
      field: "title"
      options: ""
      matchAll: true
      template: Template.plotsAutoComplete
      selector: (match) ->
        regex = new RegExp(match, 'i')
        return {$or: [{'content': regex}, {'title': regex}], users: Meteor.userId()}
#      template: Template.plotAutoComplete
    ]

Template.plotsPanel.events
  "autocompleteselect input": (e, t, d)->
    scrollToPlot(d._id)