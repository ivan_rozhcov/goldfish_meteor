Template.nodeShapeTypeMenu_.helpers
  available_types : ->
    inboxPlot = Plots.findOne(system_type: 'nodeshapes_types', users: Meteor.userId())
    if inboxPlot
      NodeShapes.find(plot_id: inboxPlot._id)