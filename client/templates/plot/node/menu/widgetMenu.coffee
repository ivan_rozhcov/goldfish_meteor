Template.widgetMenu_.helpers
  avaible_types : ->
    nt = NodeTypesToWidgets.findOne({type: @node_type})
    if nt
      nt.avaible_types
  avaible_edit_modes : ->
    NodeWidgetTypes.find()
  notViewMode: ->
    @widget_type != 'view_only'