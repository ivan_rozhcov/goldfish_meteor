Template.showTags.helpers
  viewTags: ->
    self = @
    if self.tags
      mapped = _.map self.tags, (value, index) ->
        value: value
        index: index

      initialCount = mapped.length
      maxCount = Math.floor(@width / 2 / 23)
      mapped = mapped.slice(0, maxCount)
#      console.log _.extend(_.last(mapped), {"last": true})
      if maxCount > initialCount
        _.extend(_.last(mapped), {"last": true})
      mapped

Template.showTags.events
  'click .add-tag': (e,t)->
    self = @
    bootbox.prompt 'Добавить тег', (tag) ->
      if tag
        NodeShapes.update self._id, $addToSet: tags: tag

