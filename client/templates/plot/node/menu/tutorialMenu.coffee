
Template.tutorialMenu.helpers
  tutorials: ->
    TutorialPages.find(
      type: "tutorial"
    ,
      sort:
        order: 1
    )
  activeTutorialStep: ->
    if Session.equals 'tutorialLevel', @order then  "active" else ""
  seenByUser: ->
    Meteor.userId() in @seen_by


tutorial_1 = ->
    createTestNode1 = ->
      @testNode1 = createNodeWithNodeShapeShort "Привет!<br>Давайте начнем с основ.<br>" +
      "Сейчас я расскажу как пользоваться программой.<br>" + "Новые заметки будут появляться атвоматически", 25, 25, plotId, 210, 140

    createTestNode2 = ->
      @testNode2 = createNodeWithNodeShapeShort "Это нод (node)", 288, 26, plotId
      createLinkShape('далее', @testNode1, @testNode2, Meteor.userId(), 'user')
    createTestNode3 = ->
      @testNode3 = createNodeWithNodeShapeShort "Ноды можно создавать при помощи двойного клика", 543, 20, plotId, 210, 65
      createLinkShape('далее', @testNode2, @testNode3, Meteor.userId(), 'user')
    createTestNode4 = ->
      @testNode4 = createNodeWithNodeShapeShort "Условно: ноды - это как бумажки, разложенные не столе.", 867, 20, plotId, 165, 65
      createLinkShape('далее', @testNode3, @testNode4, Meteor.userId(), 'user')
    createTestNode5 = ->
      @testNode5 = createNodeWithNodeShapeShort("При клике на нод или наведении мыши, показываются специальные управляющие элементы хало (halo)", 41, 250, plotId, 258, 90)
      setSelectedNodeShapeIdGlobal nodeShape
      createLinkShape('далее', @testNode4, @testNode5 , Meteor.userId(), 'user')
    createTestNode6 = ->
      @testNode6 = createImageNode 'image', 321, 210, plotId, 298, 169, "/tutorial/node.png"
      NodeShapes.update @testNode6, $set: zoom: 0.8
      createLinkShape('далее', @testNode5, @testNode6 , Meteor.userId(), 'user')
    createTestNode7 = ->
      @testNode7 = createNodeWithNodeShapeShort("Hallo - маленькие цветные кнопки, рядом с выделенным нодом." + "При нажатии на них происходит опеределнное дейтсвие - см скриншот.", 661, 135, plotId, 240, 121)
      createLinkShape('далее', @testNode6, @testNode7 , Meteor.userId(), 'user')
    createTestNode8 = ->
      @testNode8 = createNodeWithNodeShapeShort("При помощи фиолетового halo можно менять положение нода!<br> Чтобы начать редактироватие - достаточно клинуть!", 1031, 156, plotId, 260, 104)
      createLinkShape('далее', @testNode7, @testNode8 , Meteor.userId(), 'user')
    createTestNode9 = ->
      @testNode9 = createNodeWithNodeShapeShort("Когда нодов слишком много, это начинает мешать. По этому Вы можете с легкостью спрятать лишние ноды.<br> Не волнуйтесь, они не удаляются, просто перестают показываться.", 979, 313, plotId, 305, 118)
      createLinkShape('далее', @testNode8, @testNode9 , Meteor.userId(), 'user')
    createTestNode10 = ->
      setSelectedNodeShapeIdGlobal nodeShape
      @testNode10 = createNodeWithNodeShapeShort("Итак, теперь Вы должны знать как:<br>" + "* создавать node;<br>* двигать node;<br>* редактировать node;<br>* прятать node;<br>" + "В общем, все как с бумажками на столе! Но это только начало! Нажмите Help - Туториал - Next tutorial - она будет запускать следующий туториал сама", 607, 397, plotId, 382, 195)
      createLinkShape('далее', @testNode9, @testNode10 , Meteor.userId(), 'user')

    plotId = createPlot("Tutorial #1 Базовые операции", "nsg_graph_plot", [Meteor.userId()])
    Router.go("/plot/#{plotId}")

    nodeShape = undefined

    funArr = [
      createTestNode1
      createTestNode2
      createTestNode3
      createTestNode4
      createTestNode5
      createTestNode6
      createTestNode7
      createTestNode8
      createTestNode9
      createTestNode10
    ]

    #так стек будет разворачиваться с конца
    timerChain 2600, funArr.reverse()
    Session.set "tutorialLevel", 2

tutorial_2 = ->
    createTestNode1 = ->
      @testNode1 = createNodeWithNodeShapeShort "Ой, смотрите - мы создали еще один Плот (plot). Выходит, плотов может быть больше чем 1!<br>" + "Это все равно что вырывать странички из своего блокнота и класть их на стол", 25, 25, plot, 210, 120
    createTestNode2 = ->
      @testNode2 = createNodeWithNodeShapeShort "Для того чтобы отредактировать название плота просто кликните на него 2 раза. Чтобы сохранить изменения просто клините где-то на плоте.", 37, 191, plot, 150, 90
      createLinkShape('далее', @testNode1, @testNode2, Meteor.userId(), 'user')
    #The main data representation format is graph.
    createTestNode3 = ->
      @testNode3 = createNodeWithNodeShapeShort "Это это как группа бумажек", 239, 218, plot, 150, 50
      @testNode4 = createImageNode 'image', 321, 210, plot, 250, 110, "/tutorial/tabletop_memex.jpeg"
      NodeShapes.update @testNode4, $set: zoom: 0.2
      @testNode5 = createNodeWithNodeShapeShort "как тут", 916, 243, plot
    createTestNode4 = ->
      @testNode6 = createNodeWithNodeShapeShort "Картинка слишком маленькая? Не проблема! Можно приблизить или изменить размер при помощи Halo! Можно даже в полноэкранный режим перейти!", 633, 176, plot, 190, 90
      createLinkShape('далее', @testNode5, @testNode6 , Meteor.userId(), 'user')
    createTestNode5 = ->
      @testNode7 = createNodeWithNodeShapeShort "Да, здесь тоже halo. Вам кажется, что halo слишком много? Ничего страшного - быстро привыните! Есои нет, всегда можно посмотреть в легенду -> Help -> Legend.", 625, 35, plot, 190, 90
      createLinkShape('далее', @testNode6, @testNode7 , Meteor.userId(), 'user')
    createTestNode6 = ->
      @testNode8 = createNodeWithNodeShapeShort "Теперь вы должны знать, что такое плот, как менять его имя и размер нодов. For now you should know what plot is, how to edit its name, how " + "resize nodes. Нажмите Help - Туториал - Next tutorial - она будет запускать следующий туториал сама.", 395, 214, plot, 200, 100
      createLinkShape('далее', @testNode7, @testNode8 , Meteor.userId(), 'user')

    #TODO позиция меню!!!
    $('.fake-mouse').show()
    boundingClientRect = $('.plot-menu-dropdown a')[0].getBoundingClientRect()
    $('.fake-mouse').animate({
        top: "#{boundingClientRect.top + boundingClientRect.height/2}px",
        left: "#{boundingClientRect.left + boundingClientRect.width/2}px"
      }, 2000 )

    timerChainNormal 2600, [
      () ->
        $($('.plot-menu-dropdown a')[0]).trigger('click')
        boundingClientRect = $('.plot-menu-dropdown a')[1].getBoundingClientRect()
        $('.fake-mouse').animate({
            top: "#{boundingClientRect.top + boundingClientRect.height/2}px",
            left: "#{boundingClientRect.left + boundingClientRect.width/2}px"
          }, 2000 )
      () ->
        $($('.plot-menu-dropdown a')[1]).trigger('click')
        boundingClientRect = $('.btn.btn-primary[data-bb-handler="confirm"]')[0].getBoundingClientRect()
        $('.fake-mouse').animate({
            top: "#{boundingClientRect.top + boundingClientRect.height/2}px",
            left: "#{boundingClientRect.left + boundingClientRect.width/2}px"
          }, 2000 )
      -> emulateTypeText('Tutorial #2 Плоты', document.getElementsByClassName("bootbox-input bootbox-input-text form-control")[0]),
      ->
        $('.fake-mouse').hide()
        pass
#        plot = createPlot("Tutorial #2 Плоты", "nsg_graph_plot", [Meteor.userId()])
#        Router.go("/plot/#{plot}")
#
#        funArr = [
#          createTestNode1
#          createTestNode2
#          createTestNode3
#          createTestNode4
#          createTestNode5
#          createTestNode6
#        ]
#
#        #так стек будет разворачиваться с конца
#        timerChain 2500, funArr.reverse()
#        Session.set "tutorialLevel", 3
    ]

tutorial_3 = ->
    createTestNode1 = ->
      createNodeWithNodeShapeShort "Итак теперь у нас есть Плот и ноды на нем.<br>" + "А что же дальше? Что это за стрелки, которые связывают ноды? Сейчас узнаем ...", 25, 25, plot, 210, 80
    createTestNode2 = ->
      #TODO показать яваскриптом!
      createNodeWithNodeShapeShort "Чтобы задать связь между двумя нодами нужно нажать на зеленое halo.<br>" + "А потом отпустить на нужном ноде.", 265, 18, plot, 272, 50
    createTestNode3 = ->
      createNodeWithNodeShapeShort "Теперь 2 нода соеденены. Так же связи можно задать имя.<br>" + "Связи очень полезны, так как они содержат метаинформацию, например 'блокирует', 'связан с' и тд...<br> или 'далее', как в случае нашего туториала", 565, 18, plot, 300, 100
    createTestNode4 = ->
      createNodeWithNodeShapeShort "Когда вы прячете нод, связи прячутся автоматом.", 150, 150, plot, 160, 50
    createTestNode5 = ->
      createNodeWithNodeShapeShort "Когда Вы нажимаете на синее halo, отображаются скрытые ноды." + "Чтобы вернуть нод на плот - просто кликните на него (или отредактируйте), чтобы добавить связь кликните на нее.", 350, 150, plot, 200, 100
    createTestNode6 = ->
      createNodeWithNodeShapeShort "For now you should know how to connect nodes and parts of nodes " + "Click showHelp -> Tutorial 4 to know more или Help - Туториал - Next tutorial .", 550, 250, plot, 200, 90

    plot = createPlot("Tutorial #3 Связи", "nsg_graph_plot", [Meteor.userId()])
    Router.go("/plot/#{plot}")

    funArr = [
      createTestNode1
      createTestNode2
      createTestNode3
      createTestNode4
      createTestNode5
      createTestNode6
      createTestNode7
      createTestNode8
    ]

    #так стек будет разворачиваться с конца
    timerChain 2700, funArr.reverse()
    Session.set "tutorialLevel", 4

tutorial_4 = ->
    createTestNode1 = ->
      createNodeWithNodeShapeShort "Так... теперь у нас есть много нодов и связей.<br>" + "И что теперь? Почему нельзя тоже самое делать в любой другой программе или на бумажке?", 25, 25, plot, 210, 90
    createTestNode2 = ->
      createNodeWithNodeShapeShort "Дело в том, что я утаил некоторую инфомрацю от Вас. На самом делел когда Вы создаете нод, реально создается только его визуальное отображение. Можно добавлять больше чем одно предстваление нода ... даже на разные плоты!", 271, 25, plot, 210, 120
    createTestNode3 = ->
      createNodeWithNodeShapeShort "Эта операция называется <b>клонирование</b>. Чтобы сколнировать нод, нужно выбрать его и в меню Node -> Clone Node", 324, 150, plot, 200, 100
      @testNode4 = createImageNode 'image', 321, 210, plot, 250, 110, "/tutorial/cloneNode.png"
      NodeShapes.update @testNode4, $set: zoom: 0.7
    createTestNode4 = ->
      #TODO добавить пример!!!
      createNodeWithNodeShapeShort "Если редактируется склонированный нод, все его представления автоматически обновляются", 25, 250, plot, 200, 90
    createTestNode5 = ->
      createNodeWithNodeShapeShort "Теперь Вы знаете необходимый минимум для работы с программой!<br>" + "Я предлагаю поиграться с нодами, клонами и связями.", 235, 260, plot, 180, 80
    plot = createPlot("Tutorial #4 Клоны нодов", "nsg_graph_plot", [Meteor.userId()])
    Router.go("/plot/#{plot}")

    funArr = [
      createTestNode1
      createTestNode2
      createTestNode3
      createTestNode4
      createTestNode5
    ]

    #так стек будет разворачиваться с конца
    timerChain 2500, funArr.reverse()
    Session.set "tutorialLevel", 5

tutorial_5 = ->
    createTestNode1 = ->
      @testNode1 = createNodeWithNodeShapeShort "Теперь хочу рассказать про выделение фрагметов нода.", 25, 25, plotId, 210, 140

    createTestNode2 = ->
      @testNode2 = createNodeWithNodeShapeShort "Когда работаешь с большим объемом информации, часто просто связей не достаточно. бывает нужно выделить фрагмент текста.", 288, 26, plotId
      createLinkShape('далее', @testNode1, @testNode2, Meteor.userId(), 'user')
    createTestNode3 = ->
      @testNode3 = createNodeWithNodeShapeShort "Это делается при помощи бирюзового halo", 543, 20, plotId, 210, 65
      createLinkShape('далее', @testNode2, @testNode3, Meteor.userId(), 'user')
    createTestNode4 = ->
      @testNode4 = createNodeWithNodeShapeShort "нужно выделить фрагмент текста и нажать на него (или другого контента).", 867, 20, plotId, 165, 65
      createLinkShape('далее', @testNode3, @testNode4, Meteor.userId(), 'user')
    createTestNode5 = ->
      @testNode5 = createNodeWithNodeShapeShort("после этого создается отдельный нод, который содержит в себе только выделенный контент", 41, 250, plotId, 258, 90)
      setSelectedNodeShapeIdGlobal nodeShape
      createLinkShape('далее', @testNode4, @testNode5 , Meteor.userId(), 'user')
    createTestNode6 = ->
      @testNode6 = createImageNode 'image', 321, 210, plotId, 250, 110, "/tutorial/node.png"
      NodeShapes.update @testNode6, $set: zoom: 0.7
      createLinkShape('далее', @testNode5, @testNode6 , Meteor.userId(), 'user')
    createTestNode7 = ->
      @testNode7 = createNodeWithNodeShapeShort("С ним можно работать как с полноценным нодом - клонировать, двигать и копировать.", 661, 135, plotId, 240, 121)
      createLinkShape('далее', @testNode6, @testNode7 , Meteor.userId(), 'user')
    createTestNode8 = ->
      @testNode8 = createNodeWithNodeShapeShort("Можно соединять 2 отрывка из разных нодов!", 1031, 156, plotId, 260, 104)
      createLinkShape('далее', @testNode7, @testNode8 , Meteor.userId(), 'user')
    createTestNode9 = ->
      @testNode9 = createNodeWithNodeShapeShort("Не обязательно текстовых!", 979, 313, plotId, 305, 118)
      #TODO примеры!
      createLinkShape('далее', @testNode8, @testNode9 , Meteor.userId(), 'user')
    createTestNode10 = ->
      setSelectedNodeShapeIdGlobal nodeShape
      @testNode10 = createNodeWithNodeShapeShort("Теперь мы умеем делять фрагменты и соединять их.", 536, 376, plotId, 382, 195)
      createLinkShape('далее', @testNode9, @testNode10 , Meteor.userId(), 'user')

    plotId = createPlot("Tutorial #5 Фрагменты", "nsg_graph_plot", [Meteor.userId()])
    Router.go("/plot/#{plotId}")

    nodeShape = undefined

    #            var nodeShapeButton = createButtonNode('I see!', 250, 50, plot);
    funArr = [
      createTestNode1
      createTestNode2
      createTestNode3
      createTestNode4
      createTestNode5
      createTestNode6
      createTestNode7
      createTestNode8
      createTestNode9
      createTestNode10
    ]

    #так стек будет разворачиваться с конца
    timerChain 2600, funArr.reverse()
    Session.set "tutorialLevel", 6

tutorial_6 = ->
    createTestNode1 = ->
      @testNode1 = createNodeWithNodeShapeShort "Итак, мы узнали, что у нодов бывают клоны - то есть представление нода хранится отдельно от его содержимого.", 25, 25, plotId, 210, 140

    createTestNode2 = ->
      @testNode2 = createNodeWithNodeShapeShort "Так вот... Это представление можно менять.", 288, 26, plotId
      createLinkShape('далее', @testNode1, @testNode2, Meteor.userId(), 'user')
    createTestNode3 = ->
      @testNode3 = createNodeWithNodeShapeShort "Возьмем этот нод. Сейчас он представлен в виде текстового поля", 543, 20, plotId, 210, 65
      createLinkShape('далее', @testNode2, @testNode3, Meteor.userId(), 'user')
    createTestNode4 = ->
      @testNode4 = createNodeWithNodeShapeShort "Вот тут можно поменять его представление, например сделать отображение с заголовком.<br>Тут же можно настроить отображение клона", 867, 20, plotId, 165, 65
      createLinkShape('далее', @testNode3, @testNode4, Meteor.userId(), 'user')
    createTestNode5 = ->
      @testNode5 = createNodeWithNodeShapeShort("Или вообще чекбоксом сделать!", 41, 250, plotId, 258, 90)
      setSelectedNodeShapeIdGlobal nodeShape
      createLinkShape('далее', @testNode4, @testNode5 , Meteor.userId(), 'user')
    createTestNode6 = ->
      @testNode6 = reateNodeWithNodeShapeShort("Или вообще чекбоксом сделать!", 41, 250, plotId, 258, 90)
      createLinkShape('далее', @testNode5, @testNode6 , Meteor.userId(), 'user')
    createTestNode10 = ->
      setSelectedNodeShapeIdGlobal nodeShape
      @testNode10 = createNodeWithNodeShapeShort("Теперь мы не только клоинровать ноды, но и настраивать их отображение. Значит мы можем представлять отдни и те же данные не только в разных местах, но и в разном виде!.", 536, 376, plotId, 382, 195)
      createLinkShape('далее', @testNode9, @testNode10 , Meteor.userId(), 'user')

    plotId = createPlot("Tutorial #6 Виды нода", "nsg_graph_plot", [Meteor.userId()])
    Router.go("/plot/#{plotId}")

    nodeShape = undefined

    #            var nodeShapeButton = createButtonNode('I see!', 250, 50, plot);
    funArr = [
      createTestNode1
      createTestNode2
      createTestNode3
      createTestNode4
      createTestNode5
      createTestNode6
      createTestNode7
      createTestNode8
      createTestNode9
      createTestNode10
    ]

    #так стек будет разворачиваться с конца
    timerChain 2600, funArr.reverse()
    Session.set "tutorialLevel", 7

tutorial_7 = ->
    createTestNode1 = ->
      @testNode1 = createNodeWithNodeShapeShort "Теперь мы умеем манипулировать нодам: настраивать клоны, вид, размеры, задавать связи, выделять фрагменты. Но чем же это отличает нашу программу от массы других?.", 25, 25, plotId, 210, 140

    createTestNode2 = ->
      @testNode2 = createNodeWithNodeShapeShort "Дело в том, что, просто ноды со связями это граф и в нем станет сложно ориентироваться.", 288, 26, plotId
      createLinkShape('далее', @testNode1, @testNode2, Meteor.userId(), 'user')
    createTestNode3 = ->
      @testNode3 = createNodeWithNodeShapeShort "По этому я ввел понятие <b>тип нода</b>.", 543, 20, plotId, 210, 65
      createLinkShape('далее', @testNode2, @testNode3, Meteor.userId(), 'user')
    createTestNode4 = ->
      @testNode4 = createNodeWithNodeShapeShort "Вот этот нод - похоже, он ссылается на человека.", 867, 20, plotId, 165, 65
      createLinkShape('далее', @testNode3, @testNode4, Meteor.userId(), 'user')
    createTestNode5 = ->
      @testNode5 = createNodeWithNodeShapeShort("Ему можно присвоить тип человек! Так как то что он представляет человека, не зависит от того в какой форме он это делает.", 41, 250, plotId, 258, 90)
      setSelectedNodeShapeIdGlobal nodeShape
      createLinkShape('далее', @testNode4, @testNode5 , Meteor.userId(), 'user')
    createTestNode6 = ->
      @testNode6 = createNodeWithNodeShapeShort("Но у вида нода тоже может быть тип - обычно это что то локальное, что то что имеет смысл только в рамках данного плота", 41, 250, plotId, 258, 90)

      createLinkShape('далее', @testNode5, @testNode6 , Meteor.userId(), 'user')
    createTestNode7 = ->
      @testNode7 = createNodeWithNodeShapeShort("Например этот нод. В данном контектсе, я пометил его как вопрос, это значит что я хочу по нему что-то уточнить.", 661, 135, plotId, 240, 121)
      createLinkShape('далее', @testNode6, @testNode7 , Meteor.userId(), 'user')
    createTestNode8 = ->
      @testNode8 = createNodeWithNodeShapeShort("И типы ноды и типы отображения, это настраевыемые списки, я ничего не навязываю Вы настраиваете их под свои нужды.", 1031, 156, plotId, 260, 104)
      createLinkShape('далее', @testNode7, @testNode8 , Meteor.userId(), 'user')
    createTestNode9 = ->
      @testNode9 = createNodeWithNodeShapeShort("Итак, мы познакомились с важным понятием тип нода и тип вида нода.", 979, 313, plotId, 305, 118)
      createLinkShape('далее', @testNode8, @testNode9 , Meteor.userId(), 'user')

    plotId = createPlot("Tutorial #7 Типы Нода", "nsg_graph_plot", [Meteor.userId()])
    Router.go("/plot/#{plotId}")

    nodeShape = undefined

    #            var nodeShapeButton = createButtonNode('I see!', 250, 50, plot);
    funArr = [
      createTestNode1
      createTestNode2
      createTestNode3
      createTestNode4
      createTestNode5
      createTestNode6
      createTestNode7
      createTestNode8
      createTestNode9
      createTestNode10
    ]

    #так стек будет разворачиваться с конца
    timerChain 2600, funArr.reverse()
    Session.set "tutorialLevel", 8

tutorial_8 = ->
    createTestNode1 = ->
      @testNode1 = createNodeWithNodeShapeShort "Поздравляю, Вы дошли до 8го туториала! Это здорово. Но ... А что если я хочу на это плоте представить туториал, который мы прошли?", 25, 25, plotId, 210, 140

    createTestNode2 = ->
      @testNode2 = createNodeWithNodeShapeShort "Это очень просто, но об этом чуть позже.<br> Как вы заметили у нодов еще есть и тип их контента.", 288, 26, plotId
      createLinkShape('далее', @testNode1, @testNode2, Meteor.userId(), 'user')
    createTestNode3 = ->
      @testNode3 = createNodeWithNodeShapeShort "Картинка, Текст,  Html и так далее...", 543, 20, plotId, 210, 65
      createLinkShape('далее', @testNode2, @testNode3, Meteor.userId(), 'user')
    createTestNode4 = ->
      @testNode4 = createNodeWithNodeShapeShort "Так вот - плот это просто группа нодов и соответсвенно это просто нод с типом плот.", 867, 20, plotId, 165, 65
      createLinkShape('далее', @testNode3, @testNode4, Meteor.userId(), 'user')
    createTestNode5 = ->
      @testNode5 = createNodeWithNodeShapeShort("Так же как и любой нод, можно или создать новый или использовать существующий", 41, 250, plotId, 258, 90)
      setSelectedNodeShapeIdGlobal nodeShape
      createLinkShape('далее', @testNode4, @testNode5 , Meteor.userId(), 'user')
    createTestNode6 = ->
      @testNode6 = createNodeWithNodeShapeShort("Tutirial #1 - это я использовал существующий", 41, 250, plotId, 258, 90)

      createLinkShape('далее', @testNode5, @testNode6 , Meteor.userId(), 'user')
    createTestNode7 = ->
      @testNode7 = createNodeWithNodeShapeShort("А это новый.", 661, 135, plotId, 240, 121)
      createLinkShape('далее', @testNode6, @testNode7 , Meteor.userId(), 'user')
    createTestNode8 = ->
      @testNode8 = createNodeWithNodeShapeShort("Каждый плот можно через меню открыть в новом окне, чтобы было проще редактировать. Так же у него есть несколько вариантов представления", 1031, 156, plotId, 260, 104)
      createLinkShape('далее', @testNode7, @testNode8 , Meteor.userId(), 'user')

    plotId = createPlot("Tutorial #8 Размещение на плоте других плотов", "nsg_graph_plot", [Meteor.userId()])
    Router.go("/plot/#{plotId}")

    nodeShape = undefined

    #            var nodeShapeButton = createButtonNode('I see!', 250, 50, plot);
    funArr = [
      createTestNode1
      createTestNode2
      createTestNode3
      createTestNode4
      createTestNode5
      createTestNode6
      createTestNode7
      createTestNode8
      createTestNode9
      createTestNode10
    ]

    #так стек будет разворачиваться с конца
    timerChain 2600, funArr.reverse()
    Session.set "tutorialLevel", 9

tutorial_9 = ->
    createTestNode1 = ->
      @testNode1 = createNodeWithNodeShapeShort "Мы узнали, что плоты при отображение на другом плоте виду себя как ноды. В целом это так.", 25, 25, plotId, 210, 140

    createTestNode2 = ->
      @testNode2 = createNodeWithNodeShapeShort "Но у плота есть еще одно важно свойство. Все плоты, что мы видели раньше отображались в виде графа. А что если в виде графа неудобно?", 288, 26, plotId
      createLinkShape('далее', @testNode1, @testNode2, Meteor.userId(), 'user')
    createTestNode3 = ->
      @testNode3 = createNodeWithNodeShapeShort "Можно поменять на другие виды представления (не путать с просто видами!)", 543, 20, plotId, 210, 65
      createLinkShape('далее', @testNode2, @testNode3, Meteor.userId(), 'user')
    createTestNode4 = ->
      @testNode4 = createNodeWithNodeShapeShort "Например сдлеаем дерево!", 867, 20, plotId, 165, 65
      createLinkShape('далее', @testNode3, @testNode4, Meteor.userId(), 'user')
    createTestNode5 = ->
      @testNode5 = createNodeWithNodeShapeShort("... Или список", 41, 250, plotId, 258, 90)
      setSelectedNodeShapeIdGlobal nodeShape
      createLinkShape('далее', @testNode4, @testNode5 , Meteor.userId(), 'user')
    createTestNode6 = ->
      @testNode6 = createNodeWithNodeShapeShort("При этом сами ноды присуствующие на плоте не меняются, меняются только параметры их отображения. Например для графа это координата xy, а для списка просто порядковый номер.", 41, 250, plotId, 258, 90)

      createLinkShape('далее', @testNode5, @testNode6 , Meteor.userId(), 'user')
    createTestNode7 = ->
      @testNode7 = createNodeWithNodeShapeShort("Вот я вернул представление назад к графу и ничего не потерялось.", 661, 135, plotId, 240, 121)
      createLinkShape('далее', @testNode6, @testNode7 , Meteor.userId(), 'user')
    createTestNode8 = ->
      @testNode8 = createNodeWithNodeShapeShort("Итак теперь мы знаем, как настраивать представление для плотов. Можем и в виде таблицы и в виде графа.", 1031, 156, plotId, 260, 104)
      createLinkShape('далее', @testNode7, @testNode8 , Meteor.userId(), 'user')

    plotId = createPlot("Tutorial #9 Тип представления плотов", "nsg_graph_plot", [Meteor.userId()])
    Router.go("/plot/#{plotId}")

    nodeShape = undefined

    #            var nodeShapeButton = createButtonNode('I see!', 250, 50, plot);
    funArr = [
      createTestNode1
      createTestNode2
      createTestNode3
      createTestNode4
      createTestNode5
      createTestNode6
      createTestNode7
      createTestNode8
      createTestNode9
      createTestNode10
    ]

    #так стек будет разворачиваться с конца
    timerChain 2600, funArr.reverse()
    Session.set "tutorialLevel", 10

tutorial_10 = ->
    createTestNode1 = ->
      @testNode1 = createNodeWithNodeShapeShort "Мы узнали, что у плотов можно, менять типы представления.", 25, 25, plotId, 210, 140

    createTestNode2 = ->
      @testNode2 = createNodeWithNodeShapeShort "Но что если я хочу в одном плоте представить нод в виде графа, а на другом в виде таблицы?", 288, 26, plotId
      createLinkShape('далее', @testNode1, @testNode2, Meteor.userId(), 'user')
    createTestNode3 = ->
      @testNode3 = createNodeWithNodeShapeShort "Для этого есть функция клонирования плота!", 543, 20, plotId, 210, 65
      createLinkShape('далее', @testNode2, @testNode3, Meteor.userId(), 'user')
    createTestNode4 = ->
      @testNode4 = createNodeWithNodeShapeShort "Вот плот!", 867, 20, plotId, 165, 65
      createLinkShape('далее', @testNode3, @testNode4, Meteor.userId(), 'user')
    createTestNode5 = ->
      @testNode5 = createNodeWithNodeShapeShort("... А это его клон!", 41, 250, plotId, 258, 90)
      setSelectedNodeShapeIdGlobal nodeShape
      createLinkShape('далее', @testNode4, @testNode5 , Meteor.userId(), 'user')
    createTestNode6 = ->
      @testNode6 = createNodeWithNodeShapeShort("Если я вношу изменения в 1й плот - они попадают и во второй (так же, как это происходит и с нодами)", 41, 250, plotId, 258, 90)

      createLinkShape('далее', @testNode5, @testNode6 , Meteor.userId(), 'user')
    createTestNode7 = ->
      @testNode7 = createNodeWithNodeShapeShort("Собственно, для начала это все.<br>Спасибо за внимание!", 661, 135, plotId, 240, 121)
      createLinkShape('далее', @testNode6, @testNode7 , Meteor.userId(), 'user')

    plotId = createPlot("Tutorial #10 Клоны плотов", "nsg_graph_plot", [Meteor.userId()])
    Router.go("/plot/#{plotId}")

    nodeShape = undefined

    #            var nodeShapeButton = createButtonNode('I see!', 250, 50, plot);
    funArr = [
      createTestNode1
      createTestNode2
      createTestNode3
      createTestNode4
      createTestNode5
      createTestNode6
      createTestNode7
      createTestNode8
      createTestNode9
      createTestNode10
    ]

    #так стек будет разворачиваться с конца
    timerChain 2600, funArr.reverse()

Template.layout.events
  "click .tutorial": (e) ->
    unless Meteor.userId()
      return
#    Router.go('/')

    TutorialPages.update
      _id: @_id
    ,
      $addToSet:
        seen_by: Meteor.userId()

    console.log('click')
    eval @func

  "click #show-help": (e) ->
    unless Meteor.userId()
      return
    Router.go('/')
    #TODO from mongo
    tutorialLevel = Session.get("tutorialLevel")
    if tutorialLevel is `undefined` or tutorialLevel is null
      Session.set "tutorialLevel", 1
      tutorialLevel = 1
    if tutorialLevel > 4
        bootbox.alert 'Поздравляем - Вы завершили туториал. Если остались вопросы, вы можете пройти его сначала.'
        Session.set "tutorialLevel", null
        return
    tutorial = TutorialPages.findOne(order: tutorialLevel)
    $("##{tutorial._id}").click()

  "click #show-legend": (e) ->
    $( "#legend-image" ).dialog(width:'auto').show();