
Template.graph_plot.helpers
  getEditingValue: -> Session.get "editing_nodeshape_value"
  getEditingPlotValue: -> Session.get "editing_plot_val"
  showTmpNodes: -> Session.get("#{@_id}_show_tmp_nodes")
  hideCompleteNodes: -> Session.get("#{@_id}_hide_completed_nodes")
  getFilter: ->
    filter_type = @filter_type or 'static'
    if filter_type == 'static'
      "#{filter_type}"
    else
      "#{filter_type} #{JSON.stringify(@.filter)}"
  settings: ->
    plotId = @_id
    position: "bottom"
    limit: 5
    rules: [
      #todo по идее тут нужно просто правильный subscription выбирать
      collection: NodeShapes
      field: "content"
      options: ""
      matchAll: true
      filter:
        plot_id: plotId
      selector: (match) ->
        regex = new RegExp(match, 'i')
        return {$or: [{'node_attrs.content': regex}, {'node_attrs.title': regex}]}

      template: Template.plotAutoComplete
    ]
  projects: ->
    inboxPlot = Plots.findOne(system_type: 'projects', users: Meteor.userId() )
    if inboxPlot
      NodeShapes.find(plot_id: inboxPlot._id)
  getExternalUrl: ->
    @url

@NODE_MENU_EVENTS_EXTENDED =
  "autocompleteselect input": (e, t, d)->
    showNodeShape(d)

  "click .plot-work-mode": (e) ->
    console.info "To change plot filter type type \n
     to add filter type\n
      Plots.update('#{@_id}', {$set: {filter: {content: 'sad'}, filter_type:'dynamic'}})\n
      allowed values are: static, dynamic, dynamic_full"
  "dblclick .plot-title": (e) ->
    Session.set "editing_plot_id", @_id
    Session.set "editing_plot_val", @title
    updatePlotState @_id, "editing plot title"
    saveAction
      plot_id: @_id
      action_type: ACTION_TYPE_UPDATE
      user_id: Meteor.userId()

  "keyup #plot-title-input": (e) ->
    Session.set "editing_plot_id", @_id
    Session.set "editing_plot_val", $("#plot-title-input").val()
    updatePlotState @_id, "editing plot title"

  "click #plot-menu": (e, t) ->
    $('.node-type-menu').hide()
    contextMenu = $("#node-type-menu-#{@_id}")
    console.log contextMenu
    contextMenu.show()
    e.stopPropagation()
    e.preventDefault()


  "click .convert-node-shape": (e, t) ->
    plotId = t.data._id
    changeNodeShapeWidgetType(getEditingNodeShapeId(plotId), 'view_type', $(e.currentTarget).data('type'))

  "click .convert-plot": (e, t) ->
    console.log ('change plot')
    plotId = t.data._id
    changeNodeShapeTypePlot(plotId, $(e.currentTarget).data('type'))

  #TODO nodeshape!
  "click .clone-node-shape": (e, t) ->
    node_id = t.data.node_id
    plotId = t.data.plot_id
    if node_id
      createNodeShapeFromNode node_id, plotId, null, null, null, null, $(e.currentTarget).data('type')


  "click .clone-plot": (e, t) ->
    clonePlot t.data._id, $(e.currentTarget).data('type')

  "click .copy-plot": (e, t) ->
    plot_id = t.data._id
    plot = Plots.findOne(plot_id)
    if plot_id
      plot =  copyPlot plot_id
      scrollToPlot(plot)

  "click .clone-plot": (e, t) ->
    plot_id = @_id
    plot = Plots.findOne(plot_id)
    if plot_id
      plot =  clonePlot plot_id, plot.type
      scrollToPlot(plot)

  "click .create-thumbnail-plot": (e, t) ->
    plot_id = @_id
    createPlotPreviewImage(plot_id)

  "click #add-node-shape-default": (e) ->
    console.log 'click'
    widgetType = getPlotDefautWidgetType @

    nodeShapeId = createNodeWithNodeShapeNew(null, null, [Meteor.userId()], null, null, @_id, widgetType.width, widgetType.height, {content_type: widgetType.default_content_type},{view_type: widgetType.default_widget})
    console.log nodeShapeId

  "click #clone-node": (e) ->
    # существует 2 варианта откуда клонировать
    # 1. нодшейп выбран на плоте (красная рамочка)
    # 2. нод выбран внизу в списке нодов
    # красная рамочка имеет приоритет перед списком нодав
    # нодшейп выбран (красная рамочка) - копируем параметры с него
    if getSelectedNodeShapeId(@_id)
      node_shape = NodeShapes.findOne(getSelectedNodeShapeId(@_id))
      cloneNodeShape node_shape._id, plot_id:@_id, x:50, y:50
    else
      # TODO его больше нет! Убрать! Подумать на что заменить!
      # это список нодов внизу экрана
      node_id = $("#nodes").val()

      # если viewType не известен, пытаемся определить дефолтовый
      widgetType = NodeShapeAdaptersDefaultWidgets.findOne type: @type
      if not (widgetType and widgetType.default_widget and widgetType.default_content_type)
        throw 'Default widget isn\'t configured for this plot type'
      else
        viewType = widgetType.default_widget
      createNodeShapeFromNode node_id, @_id, null, null, null, null, viewType if node_id

  "click #copy-node": (e) ->
    # копируем нод
    if getSelectedNodeShapeId(@_id)
      node_shape = NodeShapes.findOne(getSelectedNodeShapeId(@_id))
      copyNodeShape node_shape._id, plot_id:@_id, x:50, y:50
    else
      # TODO его больше нет! Убрать! Подумать на что заменить!
      # это список нодов внизу экрана
      node_id = $("#nodes").val()

      # если viewType не известен, пытаемся определить дефолтовый
      widgetType = NodeShapeAdaptersDefaultWidgets.findOne type: @type
      if not (widgetType and widgetType.default_widget and widgetType.default_content_type)
        throw 'Default widget isn\'t configured for this plot type'
      else
        viewType = widgetType.default_widget
      newNodeId = copyNode(node_id)
      createNodeShapeFromNode newNodeId, @_id, null, null, null, null, viewType if node_id

  "click .set-plot-filter": (e) ->
    type = $(e.currentTarget).data('type')
    if type == 'static'
      Plots.update(@_id, $set: 'filter_type': type)
    else
      self = @
      bootbox.prompt 'Введите значение фильтра (javascript)\nПример: {"node_attrs.content": "2222"}', (filter) ->
        if filter
          filterObj = JSON.parse(filter)
          if filterObj
            if type == 'dynamic'
                filterObj.plot_id = self._id
                Plots.update(self._id, $set:
                  filter_type: type
                  filter: filter
                )
            else
              if type == 'dynamic_full'
                Plots.update(self._id, $set:
                  filter_type: type
                  filter: filter
                )

  "click .set-plot-limit": (e) ->
    limit = $(e.currentTarget).data('limit')
    console.log limit
    if typeof limit isnt 'undefined'
      Plots.update(@_id, $set: 'limit': limit)
    else
      console.log 'unset'
      Plots.update(@_id, $unset: 'limit': '')

  'click .set-plot-limit-custom': ->
    self = @
    bootbox.prompt 'Укажите ограничение нодов', (limit) ->
      if limit and Number(limit)
        Plots.update(self._id, $set: 'limit': Number(limit))

  "click .set-plot-sort": (e) ->
    sortType = $(e.currentTarget).data('type')
    sortField = $(e.currentTarget).data('field')
    sortOrder = $(e.currentTarget).data('order')

    if sortType
      obj = {}
      obj[sortField] = sortOrder
      Plots.update(@_id, $set:
        sort: obj
        sort_type: sortType
      )
    else
      Plots.update(@_id, $unset:
        sort: ''
        sort_type: ''
      )

  'click .set-plot-sort-custom': ->
    self = @
    bootbox.prompt "Укажите поле для сортировки", (field) ->
      if field
        sortType = field
        sortField = field
        sortOrder = -1
        obj = {}
        obj[sortField] = sortOrder
        Plots.update(self._id, $set:
          sort: obj
          sort_type: sortType
        )


  "click #clone-node-with-links": (e) ->
    nodeShapeIdFrom = getSelectedNodeShapeId(@_id)
    nodeIdFrom = Session.get("selectedNodeId")
    fromNodeShape = NodeShapes.findOne(nodeShapeIdFrom)
    if nodeShapeIdFrom
      nodeShapeToId = createNodeWithNodeShape(null, "This is node", [Meteor.userId()], 50, 50, @_id)
      nodeShapeDataTo = NodeShapes.findOne(nodeShapeToId)
      nodeId = nodeShapeDataTo.node_id
      saveAction
        plot_id: @_id
        node_id: nodeId
        nodeShape: nodeShapeDataTo._id
        action_type: ACTION_TYPE_CREATE

      link_title = "Cloned From"
      link = Links.insert(
        title: link_title
        node_from_id: nodeIdFrom
        node_to_id: nodeId
        users: [Meteor.userId()]
      )
      linkShapeData =
        title: link_title
        start_x: fromNodeShape.x
        start_y: fromNodeShape.y
        end_x: nodeShapeDataTo.x
        end_y: nodeShapeDataTo.y
        link_id: link
        plot_id: @_id
        node_shape_from_id: nodeShapeIdFrom
        node_shape_to_id: nodeShapeToId
        node_from_id: nodeIdFrom
        node_to_id: nodeId
        width: 70
        height: 20

      linkShape = LinkShapes.insert(linkShapeData)
      #TODO копировать все связи? или ноды тоже?

  "click #delete-hidden-nodes": (e) ->
    self = @
    bootbox.confirm('"Вы уверены, что хотите удалить все спрятанные ноды?', (result) ->
      if result
        for ns in NodeShapes.find(
          type:
            $in: [
              "tmp-node"
              "tmp-node-show"
            ]
          plot _id: self._id).fetch()
          #TODO remove links
          trashNodeShape(ns._id)
    )

  "click #explode-node": (e) ->
    nodeShapeIdFrom = getSelectedNodeShapeId(@_id)
    if nodeShapeIdFrom
      bootbox.dialog
        title: 'Разбиение нода'
        message: '<div class="row">  ' + '<div class="col-md-12"> ' + '<form class="form-horizontal"> ' + '<div class="form-group"> ' + '<label class="col-md-4 control-label" for="divider">Разделитель</label> ' + '<div class="col-md-4"> ' + '<input id="divider" name="divider" type="text" placeholder="\n" class="form-control input-md"> ' + '<span class="help-block">\n или &lt;br&gt;</span> </div> ' + '</div> ' + '<div class="form-group"> ' + '<label class="col-md-4 control-label" for="link-type">Тип связи</label> ' + '<div class="col-md-4"> ' + '<input id="link-type" name="divider" type="text" placeholder="\n" class="form-control input-md"> ' + '<span class="help-block">Тип связи который будет создан между нодами (с учетом вложенности)</span> </div> ' + '</div> ' + '</form> </div>  </div>'
        buttons: success:
          label: 'Разбить'
          className: 'btn-success'
          callback: ->
            linkType = $('#link-type').val()
            divider = $('#divider').val()
            explodeNodeShape(nodeShapeIdFrom, linkType, divider)
            console.log linkType, divider
            return


  "click #add-url-node": (e) ->
    self = @
    bootbox.prompt "Укажите url", (url) ->
      if url
        externalId = null
        if url
          type = 'url'
          if url.match(/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/)
            externalId = url.match(/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/)[1]
            url = "http://www.youtube.com/embed/" + externalId
            type = 'youtube_video'
          else
            match = url.match(/([^\/]+)(?=\.\w+$)/)
            title = (if match then match[0] else url)

          node = createExtenalNode(title, null, [Meteor.userId()],
              url, externalId, type)

          nsType = 'nsl_iframe'
          user = Meteor.user()
          if user and user.profile and user.profile.embedly and user.profile.embedly.key
            nsType = 'nsl_iframe_preview'

          createNodeShapeFromNode(node,self._id,50,50,null,null,nsType)

  "click .set-plot-link-type": (e) ->
    plotId = @_id
    bootbox.prompt "Укажите тип связи (сейчас: #{@main_link_type or 'parent'})", (newLinkType) ->
      if newLinkType
        Plots.update plotId, $set: main_link_type: newLinkType
        Meteor._reload.reload()

  #TODO DOESNT WORK
  "click #draw-node": (e) ->
    unless checkPlotState(@plot_id, "drawing node")
      Session.set "drawingNodeShapeId", nodeShape._id
      updatePlotState @_id, "drawing node"
    return

  "click #show-hidden-nodes": (e) ->
    Session.set "#{@_id}_show_tmp_nodes", $(e.currentTarget).prop("checked")
    
  "click #hide-complete-nodes": (e) ->
    Session.set "#{@_id}_hide_completed_nodes", $(e.currentTarget).prop("checked")

  "change .project-select": (e) ->
    console.log e.currentTarget
    Plots.update @_id, $set: project_id: e.currentTarget.value

  "click .empty-trash": (e) ->
    bootbox.confirm('Удалить все ноды навсегда?', (result) ->
      if result
        Meteor.call 'emptyTrash'
    )
    
  "click .expose-fragment": (e) ->
    exposeFragment e, @

  "click .show-related": (e) ->
    showRelated e, @
    
  "click .hide-node": (e) ->
    hideNode e, @

  "click .delete-node": (e) ->
    deleteNode e, @
      
  "click .resolve-node": (e) ->
    resolveNodeCli e, @

  "click .move-to-node": (e) ->
#    adapterType =  $(e.currentTarget).data('type')
    plot = Plots.findOne(@plot_id)
    adapterType =  plot.type
    nt = NodeTypesToWidgets.findOne({type: adapterType})
    if not (nt and nt.avaible_types)
      throw 'Default widget isn\'t configured for this adapter type'
    else
      widgetType = nt.avaible_types[0]

#    coords = coordsRelativeToElement(event.currentTarget, event)
    coords = x: 50, y: 50
    plotNodeShapeId = createNodeWithNodeShapeAdapter null, [Meteor.userId()], coords.x, coords.y, plot._id, null, null, adapterType, widgetType

    plotNodeShape = NodeShapes.findOne plotNodeShapeId

    selectedNodeShapesIds = getSelectedNodeShapeIds(plot._id)
    for nsId in selectedNodeShapesIds
      console.log 'moveNodeShape', nsId, plotNodeShape.representing_plot_id
      moveNodeShape(nsId, plotNodeShape.representing_plot_id)
      linkShapesToMoveOrTrash = LinkShapes.find($or: [
        { node_shape_from_id: nsId }
        { node_shape_to_id: nsId }
      ]).fetch()
      for linkShape in linkShapesToMoveOrTrash
        #если оба нодшейпа переносятся линкшейп тоже переносится
        if linkShape.node_shape_from_id in selectedNodeShapesIds and linkShape.node_shape_to_id in selectedNodeShapesIds
          LinkShapes.update linkShape._id, $set: plot_id: plotNodeShape.representing_plot_id
        # если нет - удаляем линшейп, но не линк
        else
          LinkShapes.remove linkShape._id


    #убираем выделелние
    selectedNodesKey = "#{@plot_id}_selected_nodes"
    setSelectedNodeShapeId(@plot_id, null)
    Session.set selectedNodesKey, []

#    $('.new-node-menu').hide()

Template.graph_plot.events (_.extend(NODESHAPE_MENU_EVENTS, NODE_MENU_EVENTS_EXTENDED))

Meteor.saveFile = (blob, name, path, type, callback) ->
  fileReader = new FileReader()
  method = undefined
  encoding = "binary"
  type_ = type or "binary"
  switch type_
    when "text"
      # TODO Is this needed? If we're uploading content from file, yes, but if it's from an input/textarea I think not...
      method = "readAsText"
      encoding = "utf8"
    when "binary"
      method = "readAsBinaryString"
      encoding = "binary"
    else
      method = "readAsBinaryString"
      encoding = "binary"
  fileReader.onload = (file) ->
    Meteor.call "saveFile", file.srcElement.result, name, path, encoding, callback
    return

  fileReader[method] blob
  return

