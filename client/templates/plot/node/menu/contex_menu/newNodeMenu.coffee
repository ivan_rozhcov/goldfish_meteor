Template.newNodeMenu.helpers
  avaible_types: ->
    NodeTypes.find()
  avaible_edit_modes: ->
    NodeWidgetTypes.find()
  avaible_adapter_types: ->
    NodeShapeAdapters.find()

@NODESHAPE_MENU_EVENTS =
  'click .copy-selected-nodes': (e, t) ->
    buffer = Session.get 'bufferedNodeShapes'
    buffer = _.uniq(buffer)
    selectedNodes = Session.get "#{@_id}_selected_nodes"
    if selectedNodes
      buffer = _.union(buffer, selectedNodes)
    selectedNs =  getSelectedNodeShapeId(@_id)
    console.log selectedNs, NodeShapes.findOne(_id: selectedNs, plot_id: @_id)
    if selectedNs and NodeShapes.findOne(_id: selectedNs, plot_id: @_id)
      unless selectedNs in buffer
        buffer.push selectedNs
    Session.set 'bufferedNodeShapes', buffer

  'click .paste-nodes': (e, t) ->
    buffer = Session.get 'bufferedNodeShapes'
    if buffer
      for nodeShape in buffer
        coords = coordsRelativeToElement(event.currentTarget, event)
        nodeShapeId = cloneNodeShape nodeShape, plot_id:@_id, x:coords.x, y:coords.y
        setSelectedNodeShapeId(@_id, nodeShapeId)

  'click .select-all-nodes': (e, t) ->
    console.log 'test'
    selectedNodesKey = "#{@_id}_selected_nodes"
    selectedNodes = Session.get selectedNodesKey
    nsUds = NodeShapes.find(plot_id: @_id, type: $ne: 'tmp-node').map( (s) -> s._id)
    selectedNodes = _.uniq(_.union(selectedNodes, nsUds))
    Session.set selectedNodesKey, selectedNodes

  'click .deselect-all-nodes': (e, t) ->
    selectedNodesKey = "#{@_id}_selected_nodes"
    setSelectedNodeShapeId(@_id, null)
    Session.set selectedNodesKey, []


NODESHAPE_MENU_EVENTS_EXTENDED =
  'click .copy-selected-node': (e, t) ->
    # TODO его больше нет! Убрать! Подумать на что заменить!
    node_id = $("#nodes").val()

    if node_id
      coords = coordsRelativeToElement(event.currentTarget, event)
      if not viewType
        #ищем дефольтовый тип для данного плота
        widgetType = NodeShapeAdaptersDefaultWidgets.findOne type: @type
        if not (widgetType and widgetType.default_widget and widgetType.default_content_type)
          throw 'Default widget isn\'t configured for this plot type'
        else
          viewType = widgetType.default_widget

        createNodeShapeFromNode node_id, @_id, coords.x, coords.y, null, null, viewType if node_id

  'click .copy-selected-plot': (e, t) ->
    #TODO это тоже deprecated
    plotId = $("#plots-lower").val()
    if plotId
      coords = coordsRelativeToElement(event.currentTarget, event)
      createNodeShapeAdapter(plotId, [Meteor.user()], coords.x, coords.y, Session.get('selectedPlotId'), null, null)

  'click .create-node-by-content-type': (e, t) ->
    currentPlot = Plots.findOne Session.get('selectedPlotId')
    coords = coordsRelativeToElement(event.currentTarget, event)

    widgetType = NodeShapeAdaptersDefaultWidgets.findOne type: currentPlot.type
    if not (widgetType and widgetType.default_widget)
      throw 'Default widget isn\'t configured for this plot type'
    createNodeWithNodeShape null, null, [Meteor.userId()], coords.x, coords.y, Session.get('selectedPlotId'), null, null, $(e.currentTarget).data('content-type'), widgetType.default_widget

    $('.new-node-menu').hide()

  'click .create-adapter': (e, t) ->
    adapterType =  $(e.currentTarget).data('type')
    nt = NodeTypesToWidgets.findOne({type: adapterType})
    if not (nt and nt.avaible_types)
      throw 'Default widget isn\'t configured for this adapter type'
    else
      widgetType = nt.avaible_types[0]

    coords = coordsRelativeToElement(event.currentTarget, event)
    createNodeWithNodeShapeAdapter null, [Meteor.userId()], coords.x, coords.y, Session.get('selectedPlotId'), null, null, adapterType, widgetType
    $('.new-node-menu').hide()


Template.newNodeMenu.events (_.extend(NODESHAPE_MENU_EVENTS, NODESHAPE_MENU_EVENTS_EXTENDED))
