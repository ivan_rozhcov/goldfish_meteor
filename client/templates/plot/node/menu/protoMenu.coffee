Template.protoMenu_.helpers
  available_types : ->
    inboxPlot = Plots.findOne(system_type: 'protos', users: Meteor.userId())
    if inboxPlot
      NodeShapes.find(plot_id: inboxPlot._id)