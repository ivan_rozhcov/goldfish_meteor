Template.tag.helpers
  getShortName: ->
    @value.slice(0,5)
  getX: ->
    40 + 20*@index

Template.showAllTags.helpers
  getX: ->
    40 + 20*(@index+1)

Template.showAllTags.events
  'click .show-tags': (e,t)->
    console.log 'show menu'
    $('#show-other-tags').show()
    e.stopPropagation()
    e.preventDefault()