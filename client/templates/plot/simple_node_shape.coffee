#логика для поиска по плоту (вверху)
Template.simpleNodeShape.events
  "click .nodeshape_scroll": (e, template) ->
    if not @plot_id
      @plot_id = NodeShapes.findOne(@node_shape_id).plot_id
    setSelectedNodeShapeId(@plot_id, @node_shape_id)
    setSelectedNodeShapeIdGlobal(@node_shape_id)
    scrollToNodeShape(@node_shape_id, 150)
    e.stopPropagation()