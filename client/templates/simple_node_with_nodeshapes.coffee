Template.simpleNodeWithNodeShapes.helpers
  nodeShapes: ->
    _.pairs(
      _.groupBy  NodeShapes.find(node_id: @_id).fetch()
      , (nodeShape) -> nodeShape.plot_id
    )
  getNodeShapePlot: ->
    Plots.findOne @.toString()