@getSelectedText = ->
  if window.getSelection
    txt = window.getSelection()
  else if window.document.getSelection
    txt = window.document.getSelection()
  else txt = window.document.selection.createRange().text  if window.document.selection
  txt.toString()

# это deprecated
@coordsRelativeToElement = (element, event) ->
  elem = $(element)
  offset = elem.offset()
  x = event.pageX - offset.left + elem.scrollLeft()
  y = event.pageY - offset.top + elem.scrollTop()
  x: x
  y: y

#TODO лучше использовать это
@relativePosition = (parent, element) ->
  offset = element.offset()
  pos = element.position()
  top: pos.top + parent.scrollTop()
  left: pos.left  + parent.scrollLeft()

@blockingSleep = (milliseconds) ->
  start = new Date().getTime()
  i = 0

  while i < 1e7
    break  if (new Date().getTime() - start) > milliseconds
    i++
  return

@moveSelected = (ol, ot, selectedObjs) ->
#  console.log 'moving to: ' + ol + ':' + ot
  if selectedObjs
    _.each selectedObjs, (id)->
      if id
        $this = $("##{id}")
  #      console.log id, $this, $this.position()
        p = $this.position()
        l = p.left
        t = p.top
        $this.css 'left', l + ol
        $this.css 'top', t + ot
        return
    return

@renderUser = (user_id) ->
    if user_id
      el = Meteor.users.findOne(user_id)
      if el
        if el.profile then el.profile.name  else ''


@computeFrameOffset = (frameId) ->
  #скролл iframe
  [window.frames[frameId].contentDocument.body.scrollLeft, window.frames[frameId].contentDocument.body.scrollTop]

@computeFrameSize = (frameId) ->
  #размеры iframe
  [window.frames[frameId].contentDocument.body.scrollWidth, window.frames[frameId].contentDocument.body.scrollHeight]

@gCal =
  insertEvent: (cliente, poblacion, texto, fecha)->
  #to-do calendar devuelve un Event Object que incluye un ID
  # si incluimos este id como campo en la alerta podremos despues
  # eliminar el evento en el calendario directamente desde la app
    url = "https://www.googleapis.com/calendar/v3/calendars/primary/events"
    event=  {
      summary: cliente
      location: poblacion
      description: texto
      start:
        "date": fecha
      end:
        "date": fecha
      }
    evento = JSON.stringify event
    console.log evento
    Auth = 'Bearer ' + Meteor.user().services.google.accessToken
    Meteor.http.post url, {
      params: {key: 'AIzaSyCnCsppPM8r72FiArubbMUQ2vj9JUb7-Zs'},
      data: event,
      headers: {
        'Authorization': Auth
        'Content-Type': 'application/json'
      }
      },
      (err, result)->
        console.log result
        return result.id

@clearUrlParams = (url) ->
  oldURL = url
  index = 0
  newURL = oldURL
  index = oldURL.indexOf('?')
  if index == -1
    index = oldURL.indexOf('#')
  if index != -1
    newURL = oldURL.substring(0, index)
   newURL



@debouncedResize = _.debounce(((nodeShape, width, height) ->
  unless nodeShape.width is width and nodeShape.height is height
    NodeShapes.update nodeShape._id,
      $set:
        width: width
        height: height
    return
), 150, true)

@NSG_GRAPH_PLOT_EVENTS =

  "click .content": (e) ->

    if window.activeDrawingLine
      window.activeDrawingLine.remove()
      window.activeDrawingLine = null
      Session.set "nodeShapeFromId", null
      returnToPrevPlotState @plot_id
      return

    #update node text TODO через plot mode
    else if Session.get("editing_plot_id") is @_id
      newPlotTitle = $("#plot-title-input").val()
      console.log 'set plot title'
      Plots.update
        _id: @_id
      ,
        $set:
          title: newPlotTitle

      Session.set "editing_plot_id", null
      returnToPrevPlotState @_id

    $('.node-context-menu').hide()
    $('.node-data-menu').hide()
    $('.node-type-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()
    $('.node-menu').hide()
    e.stopPropagation()
    return

  "contextmenu .node": (e) ->
    ss = ServerSettings.findOne()
    unless ss.show_default_menu
      e.preventDefault()
      e.stopPropagation()
    console.log 'node menu'
    $('.node-context-menu').hide()
    $('.node-data-menu').hide()
    $('.node-shape-type-menu').hide()
    $('.node-type-menu').hide()
    $('.node-proto-menu').hide()
    $('.new-node-menu').hide()

    contextMenu = $("#node-menu-#{@plot_id}")
    coords = relativePosition($("##{@_id}").parent(), $(("##{@_id}")))
    contextMenu.css('top', "#{coords.top}px").css('left', "#{coords.left}px")
    console.log coords, contextMenu
    contextMenu.toggle()
    e.stopPropagation()


  "contextmenu .content": (e) ->
    ss = ServerSettings.findOne()
    unless ss.show_default_menu
      e.preventDefault()

    $('.node-type-menu').hide()
    $('.node-proto-menu').hide()
    contextMenu = $("#new-node-menu-#{@_id}")
    coords = coordsRelativeToElement(e.currentTarget, e)
    #    coords.y = event.pageY
    contextMenu.css('top', "#{coords.y}px").css('left', "#{coords.x}px")
    contextMenu.toggle()
    e.stopPropagation()

#хало нажат
  "mouseover .halo": (e) ->
    e.preventDefault()
    return

#  TODO так и должно называться? halo-add-link-link
  "mousedown .halo-add-link-link": (e) ->
    node = NodeShapes.findOne(@node_shape_from_id)
    startDrawLine node, e
    Session.set "nodeShapeFromId", getSelectedNodeShapeId(@plot_id)
    Session.set "nodeFromId", Session.get("selectedNodeId")
    Session.set "linkShapeTitle", @title
    updatePlotState @plot_id, "connecting link"
    e.stopPropagation()
    return

  "mousedown .halo-add-link": (e) ->
    startDrawLine this, e
    e.stopPropagation()
    e.preventDefault()
    Session.set "nodeShapeFromId", getSelectedNodeShapeId(@plot_id)
    Session.set "nodeFromId", Session.get("selectedNodeId")
    Session.set "linkShapeTitle", null
    updatePlotState @plot_id, "connecting link"
    e.stopPropagation()
    return

  'mousedown .halo-add-link-with-node': (e) ->
    startDrawLine @, e
    Session.set "nodeShapeFromId", getSelectedNodeShapeId(@plot_id)
    Session.set "nodeFromId", Session.get("selectedNodeId")
    coords = coordsRelativeToElement(event.currentTarget, event)
    #    newNodeShapeId = createNodeWithNodeShapeNew null, null,[Meteor.userId()] , coords.x, coords.y, @plot_id
    options =
      x: coords.x
      y: coords.y
      node_attrs: {}

    newNodeShapeId = copyNodeShape getSelectedNodeShapeId(@plot_id), options
    # все же лучше независимый создавать, чем клонировать
    # newNodeShapeId = cloneNodeShape @_id, plot_id:@plot_id, x:coords.x, y:coords.y
    setSelectedNodeShapeId(@plot_id, newNodeShapeId)
    window.activeDrawingNodeShape = $("##{newNodeShapeId}")


    e.stopPropagation()
    e.preventDefault()
    return

  "click .halo-select-related": (e) ->
    showRelated e, @
    e.stopPropagation()

  "click .halo-show-auto": (e) ->
    showAuto e, @
    e.stopPropagation()

  "mousedown .halo-expose-fragment": (e) ->
    exposeFragment e, @
    e.stopPropagation()

#  TODO deprecated
  "mousedown .halo-select-related-text": (e) ->
    e.stopPropagation()
    e.preventDefault()
    if Session.get("nodeSelectedTextFrom")
#      TODO тут надо класс использовать - get selected area
      text = getSelectedText()
      title = "relText"
      fromNodeShape = NodeShapes.findOne(Session.get("nodeShapeFromId"))
      link = Links.insert(
        title: title
        node_from_id: fromNodeShape.node_id
        node_to_id: @node_id
        node_from_context: Session.get("nodeSelectedTextFrom")
        node_to_context: text
        users: [Meteor.userId()]
      )
      linkShapeData =
        title: title
        start_x: fromNodeShape.x
        start_y: fromNodeShape.y
        end_x: @x
        end_y: @y
        link_id: link
        plot_id: @plot_id
        node_shape_from_id: Session.get("nodeShapeFromId")
        node_shape_to_id: @_id
        node_from_id: fromNodeShape.node_id
        node_to_id: @node_id
        node_from_context: Session.get("nodeSelectedTextFrom")
        node_to_context: text
        width: 70
        height: 20

      linkShape = LinkShapes.insert(linkShapeData)
      linkShapeData._id = linkShape

      #для простоты вывода записываем и в NodeShape - сделать массив? выделенных объектов
      NodeShapes.update
        _id: Session.get("nodeShapeFromId")
      ,
        $addToSet:
          related_texts: Session.get("nodeSelectedTextFrom")

      NodeShapes.update
        _id: @_id
      ,
        $addToSet:
          related_texts: text

      updatePlotState @plot_id, "connecting related"
      Session.set "nodeShapeFromId", null
      Session.set "nodeSelectedTextFrom", null
      return
    Session.set "nodeShapeFromId", @_id
    selecteText = getSelectedText()
    Session.set "nodeSelectedTextFrom", selecteText
    updatePlotState @plot_id, "connecting related"
    e.stopPropagation()
    return

  "mousedown .halo-select-related": (e) ->
    e.stopPropagation()
    Session.set "show_node_relations_id", @node_id
    e.stopPropagation()
    return

  "click .halo-hide-node": (e) ->
    hideNode e, @
    e.stopPropagation()

  "click .halo-delete-node": (e)->
    deleteNode e, @
    e.stopPropagation()

  "click .halo-resolve-node": (e) ->
    resolveNodeCli e, @
    e.stopPropagation()


  "click .link-shape-input .halo-hide-node": (e) ->
    console.log 'hide', @plot_id, @node_shape_from_id
    LinkShapes.update
      _id: @_id
    ,
      $set:
        type: "tmp-link"
    e.stopPropagation()
    return

  "click .halo-resize-up": (e) ->
    if @width < 300
      NodeShapes.update @_id,
        $inc:
          width: 10
          height: 10
    e.stopPropagation()
    return

  "click .halo-resize-down": (e) ->
    if @width > 150
      NodeShapes.update @_id,
        $inc:
          width: -10
          height: -10
    e.stopPropagation()
    return

  "click .halo-zoom-up": (e) ->
    NodeShapes.update @_id,
      $inc:
        zoom: 0.1
    e.stopPropagation()
    return

  "click .halo-zoom-down": (e) ->
    if @zoom > 0.2
      NodeShapes.update @_id,
        $inc:
          zoom: -0.1
    e.stopPropagation()
    return


#TODO по хорошему редактирование должно само осуществлятся, там где стоит курсор
  "click .halo-edit": (e) ->
    Session.set "nodeShapeFromId", getSelectedNodeShapeId(@plot_id)
    e.stopPropagation()
    return

  "resize .node-content": (e) ->
    width = e.currentTarget.offsetWidth
    height = e.currentTarget.offsetHeight
    debouncedResize(@, width, height)
#    e.stopPropagation()

  "blur .node-content": (e) ->
    width = e.currentTarget.offsetWidth
    height = e.currentTarget.offsetHeight
    debouncedResize(@, width, height)
#    e.stopPropagation()

  "dblclick .halo": (e) ->
#        чтобы при клике на хало не создавался новый нод
    e.preventDefault()
    e.stopPropagation()
    return
# тут мы не делаем e.preventDefault() то есть это событие передаетс вверх, в частности в mouseup .content
  "mouseup .node-content": (e) ->
    console.log 'mouseup node'
    #resize
    if @width != e.currentTarget.offsetWidth or @height != e.currentTarget.offsetHeight
      console.log 'update nodeshape size'
      NodeShapes.update @_id,
        $set:
          width: e.currentTarget.offsetWidth
          height: e.currentTarget.offsetHeight

    $target = $(e.target)
    $cardContainer = (if $target.hasClass("node") then $target else $target.parents(".node"))
    if Session.get("nodeShapeFromId") != @_id
      if window.activeDrawingLine
        linkShapeTitle = Session.get 'linkShapeTitle'
        if $cardContainer
          linkId = createLink(linkShapeTitle, Session.get("nodeFromId"), @node_id, [Meteor.userId()])
          createLinkShapeFromLink(linkId, @plot_id, window.activeDrawingLine.getAttribute("x1"),
            window.activeDrawingLine.getAttribute("y1"), @x, @y, Session.get("nodeShapeFromId"), @_id)
        else
          return
        Session.set "nodeShapeFromId", null
        Session.set 'linkShapeTitle', null
        returnToPrevPlotState @plot_id
#    e.stopPropagation()

  "mouseup .content": (e) ->
    console.log 'mouseup content'
    if window.activeDrawingLine
      if window.activeDrawingNodeShape
        linkShapeTitle = Session.get 'linkShapeTitle'
        nodeShape = NodeShapes.findOne(window.activeDrawingNodeShape.attr('id'))
        console.log nodeShape
        linkId = createLink(linkShapeTitle, Session.get("nodeFromId"), nodeShape.node_id, [Meteor.userId()])
        createLinkShapeFromLink(linkId, nodeShape.plot_id, window.activeDrawingLine.getAttribute("x1"),
          window.activeDrawingLine.getAttribute("y1"), window.activeDrawingNodeShape.css('left').replace('px', ''), window.activeDrawingNodeShape.css('top').replace('px', ''), Session.get("nodeShapeFromId"), nodeShape._id)
        target = $('#'+nodeShape._id)
        parent = target.parent()
        pos = relativePosition(parent, target, e)

        Meteor.call "updateNodeShapePosition",
          nodeShapeId: nodeShape._id
          newPos: pos


      window.activeDrawingLine.remove()
      window.activeDrawingLine = null
      window.activeDrawingNodeShape = null
      # тут контекст плот
      returnToPrevPlotState @_id
      e.preventDefault()
    e.stopPropagation()
    return

  "mousemove .content": (e) ->
    if window.activeDrawingLine
      coords = coordsRelativeToElement(e.currentTarget, e)
      window.activeDrawingLine.setAttribute "x2", coords.x
      window.activeDrawingLine.setAttribute "y2", coords.y
      if window.activeDrawingNodeShape
        window.activeDrawingNodeShape.css(top: coords.y, left: coords.x)
#    e.stopPropagation()
    return

  "click .node-content": (e) ->
    setSelectedNodeShapeId(@plot_id, @_id)
    Session.set "selectedNodeId", @node_id
    setSelectedNodeShapeIdGlobal(@_id)
    #добавляем информацию о просмотре нода
    # TODO вынести в hooks
    userId = Meteor.userId()
    if @.is_seen_by?.length? and not (userId in @.is_seen_by)
      NodeShapes.update(@_id, $addToSet: is_seen_by: Meteor.userId())
      saveAction
        plot_id: @plot_id
        node_shape_id: @_id
        action_type: ACTION_TYPE_VIEW
        user_id: Meteor.userId()
#    e.stopPropagation()

  "mouseover .node": (e) ->
    return  if checkPlotState(@plot_id, "dragging node")
    return  if checkPlotState(@plot_id, "drawing node")
    $cardContainer = $(e.target)

    Session.set "selectedNodeId", @node_id
    setSelectedNodeShapeIdGlobal(@_id)

    setSelectedNodeShapeId(@plot_id, @_id)
    setEditingNodeShapeId(@plot_id, @_id)

    if e.shiftKey
      console.log 'print shiffft'
      selectedNodesKey = "#{@plot_id}_selected_nodes"
      selectedNodes = Session.get selectedNodesKey
      unless selectedNodes
        selectedNodes = []
      console.log selectedNodes
      if not (@_id in selectedNodes)
        selectedNodes.push(@_id)
      else
        selectedNodes = _.without(selectedNodes, @_id)
      Session.set selectedNodesKey, selectedNodes

    if @has_new_version
      NodeShapes.update
        _id: @_id
      ,
        $set:
          has_new_version: false


    #закоментировано тк постоянно срабатывает -что бы от этого избавить нужно разрабатывать сложную логику
    #        updatePlotState(this.plot_id, 'editing node');

    #        Session.set('editing_value', $cardContainer[0].innerText);
    #вынестие в хало
    keyName = "#{@plot_id}_selected_nodes"
    $cardContainer.draggable
      containment: "parent"
      distance: 15
      handle: ".halo-drag"
      drag: (event, ui)->
        currentLoc = $(@).position()
        prevLoc = $(this).data('prevLoc')
        if !prevLoc
          prevLoc = ui.originalPosition
        offsetLeft = currentLoc.left-prevLoc.left
        offsetTop = currentLoc.top-prevLoc.top
        moveSelected(offsetLeft, offsetTop, Session.get keyName)
        $(this).data('prevLoc', currentLoc)
#    e.stopPropagation()

  "mouseleave .node-content": (e, template) ->


#        returnToPrevPlotState(this.plot_id);
  "dragstart .node": (e, template) ->
    Session.set "draggingNodeShapeId", @_id
    updatePlotState @plot_id, "dragging node"
    e.stopPropagation()
    return

  "dragstop .node": (e) ->

#TODO разобраться с zoom
# для zoom = 0.5 - возможно трансформ работает 2 раза
#    pos.left = pos.left * 4
#    pos.top = pos.top * 4

#со стороны клиента разрешено обновлять только по id, по этому перезаписываем
#http://www.hypothete.com/blog/mongodb-nested-arrays-in-meteor-and-how-to-thwart-them/
    console.log 'dragstop'
    target = $(e.target)
    parent = target.parent()
    pos = relativePosition(parent, target, e)

    Meteor.call "updateNodeShapePosition",
      nodeShapeId: Session.get("draggingNodeShapeId")
      newPos: pos

    keyName = "#{@plot_id}_selected_nodes"
    selectedObjs = Session.get keyName
    if selectedObjs
      _.each selectedObjs, (id)->

        if id == @_id or not id
          return
        target = $("##{id}")
        parent = target.parent()
        pos = relativePosition(parent, target, e)
        console.log id, pos
        Meteor.call "updateNodeShapePosition",
          nodeShapeId: id
          newPos: pos

    Session.set "draggingNodeShapeId", null
    returnToPrevPlotState @plot_id
    getSoundForEvent('node_drag_stop')
    e.stopPropagation()

  "dblclick .node-content": (evt, tmpl) -> # start editing list name
    evt.stopPropagation()
    return

  "dblclick .content": (ev, template) ->
    #this это плот, тк мы кликаем не на нодшейп
    currentPlot = Plots.findOne @_id
    #    console.log currentPlot
    coords = coordsRelativeToElement(ev.currentTarget, event)

    widgetType = getPlotDefautWidgetType currentPlot

    createNodeWithNodeShape null, null, [Meteor.userId()], coords.x, coords.y, currentPlot._id, widgetType.width, widgetType.height, widgetType.default_content_type, widgetType.default_widget

    $('.new-node-menu').hide()
    ev.stopPropagation()

  "scroll .node-content textarea": (event, template) ->
#TODO нужно сохранять в нодшейп и выводить при рисовании
#    console.log 'scroll'
    originalEvent = $(event.originalEvent.target)
    #    console.log event.originalEvent.target
    #    console.log originalEvent.scrollTop()
    #    console.log originalEvent.scrollLeft()
    NodeShapes.update(@_id, $set: {scroll_top: originalEvent.scrollTop(), scroll_left: originalEvent.scrollLeft()})


#пока добавляем нод по клику, надо подумать про множетсвенное добавление
  "click .tmp-node": (e, template) ->
    NodeShapes.update
      _id: @_id
    ,
      $set:
        type: "node"

    e.stopPropagation()
    return

  "click .tmp-link": (e, template) ->
    unhideLinkShape(@)
    e.stopPropagation()

  "click .tmp-link-shape-input": (e, template) ->
    unhideLinkShape(@)
    e.stopPropagation()

  "keyup .link-shape-input input": (e, template) ->
    Session.set "editing_linkshape_value", event.target.value
    Session.set "editing_linkshape_id", @_id
    saveLinkShapeChanges()
    e.stopPropagation()
    return

  "mouseover .link-shape-input": (e, template) ->
    Session.set "selectedLinkShapeId", @_id
    Session.set "selectedLinkId", @link_id
    Session.set "editing_linkshape_id", Session.get("selectedLinkShapeId")
    if @has_new_version
      LinkShapes.update
        _id: @_id
      ,
        $set:
          has_new_version: false
    e.stopPropagation()
    return

  "click .link": (e, template) ->
    Session.set "selectedLinkShapeId", @_id
    e.stopPropagation()
    return

  "keyup .fullscreen": (e, template) ->
    Session.set "fullscreenNodeShapeId", null
    e.stopPropagation()
    return

  "click #hide-fullscreen": (e, template) ->
    Session.set "fullscreenNodeShapeId", null
    e.stopPropagation()
    return


#чтобы нельзя было перетащить файл
  "dragover .content": (e, template) ->
    e.stopPropagation()
    e.preventDefault()
    return

  "dragover .content": (e, template) ->
    e.stopPropagation()
    e.preventDefault()
    return

  'drop .content': (ev, template) ->
    plotId = @_id
    ev.stopPropagation()
    ev.preventDefault()
    files = ev.originalEvent.dataTransfer.files
    console.log(ev.originalEvent.dataTransfer)
    for f in files
      #TODO тут конечно нужна интеграция с dropbox но пока с ней разобраться не получилось
      # тут http://www.html5rocks.com/en/tutorials/file/filesystem/#toc-dir вроде вы то, что надо но с другой сторны - это все умеет dropbox

      # Read the File objects in this FileList.
      console.log(f.type, f.size, f.name)
      coords = coordsRelativeToElement($('.content'), event)
      if f.type.lastIndexOf('text', 0) == 0
        console.log('text')
        r = new FileReader
        plotId = @_id
        #TODO возможно тут тоже просто создавать файл
        r.onload = (e) ->
          contents = e.srcElement.result
          createNodeWithNodeShapeNew(f.name, contents, [Meteor.userId()], coords.x, coords.y, plotId, 150, 150, {filetype: f.type, filesize: f.size, content_type: 'text'})
          return

        r.readAsText f
      else
        if f.type.includes('image/')
          nsId = createImageNode(f.name, coords.x, coords.y, @_id, null, null, "/#{f.name}")
        else
          if f.type.includes('audio/')
            nsId = createAudioNode(f.name, coords.x, coords.y, @_id, null, null, "/#{f.name}")
          else
            if f.type.includes('video/')
              nsId = createVideoNode(f.name, coords.x, coords.y, @_id, null, null, "/#{f.name}")
            else
              if f.type.includes('pdf')
                nsId = createPdfNode(f.name, coords.x, coords.y, @_id, null, null, "/#{f.name}")
              else
                nodeId = createExtenalNode(f.name, null, [Meteor.userId()], "/#{f.name}", f.name, 'url')
                nsId = createNodeShapeFromNode(nodeId, @_id, coords.x, coords.y, 150, 50, 'nsl_iframe')

        Files.insert(f, (err, fileObj) ->
          console.log 'got fileObj', fileObj
          unless err
            #Так как url() тут еще может быть недоступен - делам его руками, чтобы не усложнять и не выностить на сервер
            if fileObj.original and fileObj.original.name
              #аттрибуты нужно так обновлять
              saveNodeShapeAttribute(NodeShapes.findOne(nsId), 'node_attrs.node_external_uri', "/cfs/files/files/" + fileObj._id + '/' + fileObj.original.name)
              # иногда картинка не успевает отобразиться
              Router.go 'plotTab', _id: plotId
        )
    if not files.length
      # скорей всего дран н дроп - обработаем
      if ev.originalEvent.dataTransfer
        nodeShapeId = ev.originalEvent.dataTransfer.getData("node-shape-id")
        nodeId = ev.originalEvent.dataTransfer.getData("node-id")
        plotId = ev.originalEvent.dataTransfer.getData("plot-id")
        coords = coordsRelativeToElement($('.content'), event)
        console.log plotId
        if nodeShapeId
          console.log 'node shape', nodeShapeId
          Meteor.call 'cloneNodeShape', nodeShapeId, plot_id:@_id, x:coords.x, y:coords.y
        else if nodeId
          console.log 'node', nodeId
          Meteor.call 'createNodeShapeFromNode', nodeId, @_id, coords.x, coords.y
        else if plotId
          Meteor.call 'createNodeShapeAdapter', plotId, coords.x, coords.y, @_id, null, null
        else
          console.log 'not files.length', ev.originalEvent.dataTransfer.getData("text")

      else
        console.log 'no data', ev.originalEvent
    ev.stopPropagation()
