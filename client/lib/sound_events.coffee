#TODO эта штука должна задаваться для каждого юзера и через можнгу (плот)
#тип события : путь в звуку
@soundEvents =
  node_drag_stop: new Howl (urls: ['/sounds/vk_new_message.mp3'])
  node_drag_start: null
  node_click: undefined
  node_resize_up: false

@isSoundEndbledForUser = ->
#  Meteor.userId().profile.sounds_enabled
  true

@getSoundForEvent = (eventType) ->
  sound = soundEvents[eventType]
  if sound and isSoundEndbledForUser()
    sound.play()
