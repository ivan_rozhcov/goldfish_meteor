@createNodeWithNodeShapeShort = (content, x, y, plot, width, height, widgetType) ->
  createNodeWithNodeShape null, content, [Meteor.userId()], x, y, plot, width, height, null, widgetType


@onDragStartNodeShape = (event) ->
#  console.log event.target
#  console.log $(event.target).data('node-shape-id')
  event.dataTransfer.setData("node-shape-id", $(event.target).data('node-shape-id'))

@onDragStartNode = (event) ->
  event.dataTransfer.setData("node-id", $(event.target).data('node-id'))


@showRelated = (e, nodeShape) ->
    e.stopPropagation()
    #TODO эти все переменные по хорошему тоже вынести в плотозависимые
    if Session.get("NodeShapeIdForRelated") is nodeShape._id
      Session.set "NodeShapeIdForRelated", null
      Meteor.call "clearRelatedForPlot", nodeShape.plot_id
      return
    Session.set "NodeShapeIdForRelated", nodeShape._id
    nodeShapeId = getSelectedNodeShapeId(nodeShape.plot_id)
    plotId = nodeShape.plot_id
    Meteor.call "showRelatedNodesLinks",
      nodeShapeId: nodeShapeId
      plotId: plotId
    return

@showAuto = (e, nodeShape) ->
    e.stopPropagation()
    if Session.get("NodeShapeIdForAuto") is nodeShape._id
      Session.set "NodeShapeIdForAuto", null
      Meteor.call "clearAutoForPlot", nodeShape.plot_id
      return
    Session.set "NodeShapeIdForAuto", nodeShape._id
    nodeShapeId = getSelectedNodeShapeId(nodeShape.plot_id)
    plotId = nodeShape.plot_id
    Meteor.call "showAutoNodesLinks",
      nodeShapeId: nodeShapeId
      plotId: plotId
    return

@exposeFragment = (evnt, nodeShape) ->
    evnt.stopPropagation()
    evnt.preventDefault()
    #мы считаем ,что фрагмент уже выбран
    #TODO нужно сделать выбор режима выделения
    if nodeShape.node_type in ['text', 'html']
      content = getSelectedText()
      console.log content
      if content
        #это нодшейп, но со специальным типом partial - для него должна быть более хитрая логика апдейта
        toNodeShapeId = createNodeShapeFromNodeShape(nodeShape, content, null, null, null, null, true)
        toNodeShape = NodeShapes.findOne(toNodeShapeId)
        linkId = createLink('partOf', nodeShape.node_id, toNodeShape.node_id, [Meteor.userId()], 'system')

        createLinkShapeFromLink(linkId, nodeShape.plot_id, nodeShape.x,
        nodeShape.y, toNodeShape.x, toNodeShape.y, nodeShape._id, toNodeShape._id)
    else
      if nodeShape.node_type == 'url' and nodeShape.node_external_type == 'youtube_video'
        self = nodeShape
        bootbox.prompt("Введите начало отрывка", (start) ->
          baseUrl = "http://www.youtube.com/v/#{self.node_external_id}"
          if start
            baseUrl = baseUrl + '&start=' + start

          bootbox.prompt("Введите конец отрывка", (end) ->
            if end
              baseUrl = baseUrl + '&end=' + end
            baseUrl = baseUrl + '&version=3&modestBranding=1&rel=0'

            toNodeShapeId = createNodeShapeFromNodeShape(self, content, null, null, null, null, true, node_external_uri: baseUrl)
            toNodeShape = NodeShapes.findOne(toNodeShapeId)
            linkId = createLink('partOf', self.node_id, toNodeShape.node_id, [Meteor.userId()], 'system')

            createLinkShapeFromLink(linkId, self.plot_id, self.x,
            self.y, toNodeShape.x, toNodeShape.y, self._id, toNodeShape._id)
          )
        )

      else
        if nodeShape.node_type in ['video', 'audio']
          self = nodeShape
          bootbox.prompt "Введите начало отрывка", (start) ->
            bootbox.prompt "Введите конец отрывка", (end) ->
              #Упс - 2 раза такое не сработает
              baseUrl = clearUrlParams self.node_attrs.node_external_uri
              baseUrl = baseUrl + "#t=#{start or ''},#{end or ''}"
              #присваеваем тут тк через options не получится - будте менять .
              self.node_attrs.node_external_uri = baseUrl
              toNodeShapeId = createNodeShapeFromNodeShape(self, content, null, null, self.width, self.height, true)
              toNodeShape = NodeShapes.findOne(toNodeShapeId)
              linkId = createLink('partOf', self.node_id, toNodeShape.node_id, [Meteor.userId()], 'system')

              createLinkShapeFromLink(linkId, self.plot_id, self.x,
              self.y, toNodeShape.x, toNodeShape.y, self._id, toNodeShape._id)
        # для типов - image adapter (не доделано)
        else
          console.log 'image'
          self = nodeShape
          #TODO нужен универсальный селектор для контента нода
          target = $(evnt.currentTarget)
          div = target.siblings('.node-content').children('.scroll')
          div.children(".select-area-canvas").remove()
          div.append( '<canvas id="select_canvas" class="select-area-canvas" style="
        position: absolute;
        top: 0;
        left: 0;
    "></canvas>' );
          start = null;
          ctx = $("#select_canvas").get(0).getContext('2d')
          ctx.globalAlpha = 0.5

          $('#select_canvas').mousedown((e) ->
            start = [
              e.offsetX
              e.offsetY
            ]
            return
          ).mouseup((e) ->
            end = [
              e.offsetX
              e.offsetY
            ]

            x1 = Math.min(start[0], end[0])
            x2 = Math.max(start[0], end[0])
            y1 = Math.min(start[1], end[1])
            y2 = Math.max(start[1], end[1])
            grabbed = []
            $('*').each ->
              if !$(this).is(':visible')
                return
              o = $(this).offset()
              x = o.left
              y = o.top
              w = $(this).width()
              h = $(this).height()
              if x > x1 and x + w < x2 and y > y1 and y + h < y2
                grabbed.push this
              return
            console.log self, self.node_type

            if self.node_type == 'image'
              console.log start, end, div.scrollTop(), div.scrollLeft()
              console.log 'zoom', self.zoom
              offsetY = div.scrollTop()/self.zoom
              offsetX = div.scrollLeft()/self.zoom
              console.log offsetX, offsetY
              Meteor.call 'extractAreaFromImage', self, [[x1/self.zoom+offsetX,y1/self.zoom+offsetY], [x2/self.zoom+offsetX,y2/self.zoom+offsetY]], (err, res) ->
                toNodeShapeId = res
                toNodeShape = NodeShapes.findOne(toNodeShapeId)
                linkId = createLink('partOf', self.node_id, toNodeShape.node_id, [Meteor.userId()], 'system')

                createLinkShapeFromLink(linkId, self.plot_id, self.x,
                self.y, toNodeShape.x, toNodeShape.y, self._id, toNodeShape._id)
            else
              if self.node_type == 'adapter'
                #div из image?
                html2canvas $(div), onrendered: (canvas) ->
                  document.body.appendChild canvas
                  return
              else
                console.log start, end, computeFrameOffset("iframe_#{self._id}")
                console.log 'size', computeFrameSize("iframe_#{self._id}")
                console.log 'zoom', self.zoom
                [offsetX, offsetY] = computeFrameOffset("iframe_#{self._id}")
                offsetY /= self.zoom
                offsetX /= self.zoom


            start = null
            $(this).hide()
            return
          ).mousemove (e) ->
            if !start
              return
            ctx.clearRect 0, 0, nodeShape.offsetWidth, nodeShape.offsetHeight
            ctx.beginPath()
            x = e.offsetX
            y = e.offsetY
            ctx.rect start[0], start[1], x - (start[0]), y - (start[1])
            ctx.fill()
            return
#      для картинок http://annotorious.github.io/getting-started.html
#для видео https://github.com/CtrHellenicStudies/OpenVideoAnnotation

@hideNode = (e, nodeShape) ->

    # remove nodeshape on current tab
    NodeShapes.update
      _id: nodeShape._id
    ,
      $set:
        type: "tmp-node"


    #remove links!
    Meteor.call "hideLinkShapesOnNodeShape",
      plotId: nodeShape.plot_id
      nodeShapeId: nodeShape._id


    #end
    setSelectedNodeShapeId(nodeShape.plot_id, nodeShape._id)
    e.stopPropagation()
    return

@deleteNode = (e, nodeShape) ->
    #TODO delete node
    # remove nodeshape on current tab - не забыть про связи!
    self = nodeShape
    bootbox.confirm('Вы точно хотите удалить нод? Все связи будут потеряны.', (result) ->
      if result
        trashNodeShape(self._id)
    )
    e.stopPropagation()

@resolveNodeCli = (e, nodeShape) ->
    if nodeShape.node_attrs.resolved
      resolveNode(nodeShape._id, false)
    else
      resolveNode(nodeShape._id, true)
    e.stopPropagation()


# лучше все же не вешать на keyup - все равно будет "глотать" буквы
@DEBOUNCE_KEYUP_TIME = 100

@debouncedSaveNodeShapeText = _.debounce(((nodeShape, event, attr) ->
    #nsl_plot может выводить на себе другие нодшейпы (ну и в теории где то еще) с input и по этому в
    # events могут перехватываться лишние события
    # по этому проверям, что id ближашего родительского элемента соотвествует id нодшейпа текущего
    # темплейта
  if nodeShape._id == $(event.target).closest('.node').attr('id')
#    console.log 'equal'
    saveNodeShapeText(nodeShape, event, attr)
#  else
#    console.log 'not equal'
  return
), DEBOUNCE_KEYUP_TIME, true)
