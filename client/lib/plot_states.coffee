#TODO наверное это должно быть на сервере, и просиходить вместе с инсертом соотв модели
#возможно даже на уровне монги -OPLOG?

@updatePlotState = (plotId, newState) ->
  Session.set "plot" + plotId + "prev_mod", Session.get("plot" + plotId)
  Session.set "plot" + plotId, newState
  return

@returnToPrevPlotState = (plotId) ->
  Session.set "plot" + plotId, Session.get("plot" + plotId + "prev_mod")
  return

@getPlotState = (plotId) ->
  Session.set "plot" + plotId, "viewing"  if Session.get("plot" + plotId) is `undefined`
  Session.get "plot" + plotId

@checkPlotState = (plotId, state) ->
  getPlotState(plotId) is state

#todo тут нужно сделать стек s.push() s.pop()
_getPrevState = ->

#editing_node_id - редактируемый нод на данном плоте
@getEditingNodeShapeId = (plotId) ->
  Session.get plotId + '_EditingNodeShapeId'
@setEditingNodeShapeId = (plotId, nodeShapeId) ->
  Session.set plotId + '_EditingNodeShapeId', nodeShapeId

#selectedNodeShapeId  -  выбранный нод на данном плоте
@getSelectedNodeShapeId = (plotId) ->
  Session.get plotId + '_SelectedNodeShapeId'
@setSelectedNodeShapeId = (plotId, nodeShapeId) ->
  Session.set plotId + '_SelectedNodeShapeId', nodeShapeId

@getSelectedNodeShapeIdGlobal = () ->
  Session.get "selectedNodeShapeId"
@setSelectedNodeShapeIdGlobal = (nodeShapeId) ->
  Session.set "selectedNodeShapeId", nodeShapeId

@getSelectedNodeShapeIds = (plotId) ->
  res = Session.get(plotId + '_selected_nodes') or []
  res.push(Session.get plotId + '_SelectedNodeShapeId')
  _.uniq(res)