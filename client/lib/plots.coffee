@scrollToNodeShape = (nodeShapeId, offsetFromTop) ->
  nodeShape = NodeShapes.findOne(nodeShapeId)
  if nodeShape
    unless offsetFromTop
      offsetFromTop = 60
    target = $("#" + nodeShape._id)
    if target.length
      #TODO тут мы пытаемся сделать скролл по body, что работает только если боди растянуто. К тому же не работает 
# горизонтальный скролл. По хорошему нужно скроллить к div
      $("html,body").animate
        scrollTop: target.offset().top  - offsetFromTop
      , 1000
    else
      #TODO dirty hack - видимо как то связан с iron router, возможно из за 2й схемы маппинга урлов
      Meteor.setTimeout(->
        Router.go('plotTab', {_id: nodeShape.plot_id}, {hash: nodeShape._id})
      , 500)

@scrollToPlot = (plotId, offsetFromTop) ->
  unless offsetFromTop
    offsetFromTop = 60
  target = $("#" + plotId)
  if target.length
    $("html,body").animate
      scrollTop: target.offset().top  - offsetFromTop
    , 1000
  else
    #TODO dirty hack - видимо как то связан с iron router, возможно из за 2й схемы маппинга урлов
    Meteor.setTimeout(->
      Router.go('plotTab', _id: plotId)
    , 500)

@mindTrail = (plotId) ->
  ###
   показывает создание нодшейпов по порядку следования, предполагается, что плот открыт в новом окне
   @param {String} plotId - плот, для которого будет вестить демонстрация
   ###
  #todo sort! filter hidden
  funArr = []
  # в обратном порядке тк timerChain - работает с конца
#  TODO через функцию getOnlyNormalNodeShapesFilter
  nodeShapes = NodeShapes.find(
    plot_id: plotId,
    type:
      $nin: [
        "tmp-node"
        "tmp-node-show"
      ],
  sort:
    create_time: -1
    )
  for nodeShape in nodeShapes.fetch()
    func = ((ns) ->
      zoom = ->
        $("##{ns._id}").zoomTo
          targetsize: 0.3
          duration: 600

      unzoom = ->
        $("body").zoomTo
          targetsize: 0
          duration: 600


      timerChain 1000, [unzoom, zoom]
      return
    ).bind(this, nodeShape);

    funArr.push func


  timerChain 1000, funArr

@parseNodes = () ->
  Meteor.call "parseNodes"

@getPlotDefautWidgetType = (plot) ->
  widgetType = NodeShapeAdaptersDefaultWidgets.findOne type: plot.type
  if not (widgetType and widgetType.default_widget and widgetType.default_content_type)
    throw 'Default widget isn\'t configured for this plot type'

  if plot.default_widget
    widgetType.default_widget = plot.default_widget
  widgetType

@onDragStartPlot = (event) ->
  console.log event.target
  console.log $(event.target).data('plot-id')
  event.dataTransfer.setData("plot-id", $(event.target).data('plot-id'))

base64toBlob = (base64Data, contentType) ->
  base64Data = base64Data.replace(/^data:image\/(png|jpg);base64,/, "")
  contentType = contentType or ''
  sliceSize = 1024
  byteCharacters = atob(base64Data)
  bytesLength = byteCharacters.length
  slicesCount = Math.ceil(bytesLength / sliceSize)
  byteArrays = new Array(slicesCount)
  sliceIndex = 0
  while sliceIndex < slicesCount
    begin = sliceIndex * sliceSize
    end = Math.min(begin + sliceSize, bytesLength)
    bytes = new Array(end - begin)
    offset = begin
    i = 0
    while offset < end
      bytes[i] = byteCharacters[offset].charCodeAt(0)
      ++i
      ++offset
    byteArrays[sliceIndex] = new Uint8Array(bytes)
    ++sliceIndex
  new Blob(byteArrays, type: contentType)

@createPlotPreviewImage = (plotId) ->
  html2canvas $('#' + plotId),
    allowTaint: true
    useCORS: true
    timeout: 1000
    onrendered: (canvas) ->
      png = canvas.toDataURL("image/png");
      blob = base64toBlob(png, "image/png")
      Images.insert(blob, (err, fileObj)->
        unless err
#          console.log fileObj
          Plots.update(plotId, $set: plot_preview_image: fileObj._id)
      )
#      Meteor.saveFile(blob, "#{plotId}.png", null, null, ()-> Plots.update(plotId, $set: plot_preview_image: "/#{plotId}.png"))
      return