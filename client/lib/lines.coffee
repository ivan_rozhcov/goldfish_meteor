@activeDrawingLine = null

@startDrawLine  = (context, e) ->
  @activeDrawingLine = document.createElementNS("http://www.w3.org/2000/svg", "line")
  activeDrawingLine.setAttribute "x1", context.x
  activeDrawingLine.setAttribute "y1", context.y
  activeDrawingLine.setAttribute "x2", context.x
  activeDrawingLine.setAttribute "y2", context.y
  drawable = $(e.currentTarget.parentNode.parentNode.childNodes[5])
  drawable.append @activeDrawingLine
  return