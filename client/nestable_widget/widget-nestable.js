//{% extends 'admin/common.html' %}
//{% from 'admin/common.html' import render_bool %}
//
//{% block content %}
//    <h1 class="uk-h1">{{ _('Provinces list page header') }}
//        {% if not view_only and perm_create -%}
//            <a class="uk-button uk-button-primary uk-float-right" href="{{ url_for('admin.edit_region') }}">{{ _('Add region button') }}</a>
//        {%- endif %}
//    </h1>
//
//    <form class="nodblclk" action="{{ url_for('admin.admin_provinces') }}" method="post">
//        <div class="uk-nestable-header">
//            <table>
//                <tr>
//                    <td style="width:450px">{{ _('Region title column') }}</td>
//                    <td style="width:50px" class="uk-text-center"><span class="uk-icon-globe"></span></td>
//                </tr>
//            </table>
//        </div>
//
//        {% call(item) render_tree(items=provinces) -%}
//            {{ render_row(item) }}
//        {% endcall -%}
//
//        <div class="save_cancel">
//            <div class="save_cancel-content">
//                {% if not view_only -%}
//                    <button type="submit" class="uk-button uk-button-primary">{{ _('Save provinces order button') }}</button>
//                    <a href="{{ url_for('admin.admin_provinces') }}" class="save_cancel-cancel uk-button uk-button-link">{{ _('Cancel link') }}</a>
//                {%- else -%}
//                    <span class="uk-text-danger uk-clearfix">{{ _('You do not have permission to edit this data string') }}</span>
//                {%- endif %}
//            </div>
//        </div>
//    </form>
//{% endblock %}
//
//{% macro render_row(option) -%}
//    <div class="uk-nestable-item">
//        <input type="hidden" name="node" value="{{ option.id }}" />
//        <div data-netable-action="toggle"></div>
//        {% if not view_only -%}<div class="uk-nestable-handle"></div>{%- endif %}
//        <table data-link="#{{ option.id }}">
//            <tr>
//                <td data-width="450" class="ellipsis"><a href="{{ url_for('admin.edit_region', region_id=option.id) }}">{{ option.title }}</a></div></td>
//                <td data-width="50" class="uk-text-center"><span class="{{ 'uk-icon-check status_on' if option.activity else 'uk-icon-remove off' }}"></span></td>
//            </tr>
//        </table>
//    </div>
//{%- endmacro %}
//
//{% macro render_tree(items=[]) -%}
//    <ul class="uk-nestable" data-max_depth="3">
//        <li class="uk-nestable-list-item">
//        {% with depth = 0 -%}
//            {% for item in items -%}
//                {% if not loop.first %}
//                    {%- if item.mp_depth > depth %}
//                        <ul class="uk-nestable-list">
//                    {%- elif item.mp_depth < depth -%}
//                        {{ '</li></ul>'|safe * (depth - item.mp_depth) }}
//                        </li>
//                    {%- else -%}
//                        </li>
//                    {%- endif %}
//                {%- endif %}
//
//                <li class="uk-nestable-list-item">
//                    {{ caller(item) }}
//
//                {%- set depth = item.mp_depth -%}
//
//                {%- if loop.last %}
//                    {{ '</li></ul>'|safe * (depth - 1) }}
//                    </li>
//                {%- endif %}
//            {%- endfor %}
//        {%- endwith %}
//        </li>
//    </ul>
//{%- endmacro %}

//$(function() {
//	$(".uk-nestable").widgetNestable();
//});
//
//$.fn.widgetNestable = function (data){
//	var $elem = this;
//	var tempItem;
//	var maxDepth = 10;
////	if($elem.data().max_depth) maxDepth = $elem.data().max_depth;
//	console.log(maxDepth);
//	$("td", $elem).each(function(){
//		var td = $(this);
//		var level = td.parents("ul").length;
//		var width = parseInt(td.data().width, 10);
//		if(td.is(":first-child")) width = parseInt(td.data().width, 10) - ((level - 1) * 20);
//		td.css({"width": width, "min-width": width});
//		if($(this).hasClass("ellipsis")){
//			var content = $("<div></div>").addClass("ellipsis").html(td.html()).attr("title", td.text()).outerWidth(1);
//			var widthIn = td.html(content).width();
//			content.outerWidth(widthIn);
//		}
//	});
//	function calcWidth(td){
//		var level = td.parents("ul").length;
//		var width = parseInt(td.data().width, 10);
//		td.removeAttr("style");
//		if(td.is(":first-child")) width = parseInt(td.data().width, 10) - ((level - 1) * 20);
//		td.css({"width": width, "min-width": width});
//		var content = $("div.ellipsis", td).removeAttr("style").outerWidth(1);
//		var widthIn = td.html(content).width();
//		content.outerWidth(widthIn);
//	}
//	var nestable = $.UIkit.nestable($elem, {maxDepth:maxDepth});
//	nestable.on("stop.uk.nestable", function(e, obj){
//		checkParents();
//		$("td.ellipsis:first-child", tempItem).each(function(){
//			calcWidth($(this));
//		});
//	});
//	$(".uk-nestable-handle", $elem).on("mousedown", function(){
//		tempItem = $(this).closest("li");
//	});
//	function checkParents(){
//		$("input[name=parent]",$elem).each(function(){
//			var level = $(this).parents("ul").length;
//			$($(this).parent().find("input[name=level]")[0]).val(level);
//			if(level == 1){
//				$($(this).parent().find("input[name=parent]")[0]).val(0);
//			}else{
//				var parentLi = $(this).parents("li")[1];
//				var parentNode = $(parentLi).find("input[name=node]")[0];
//				var parentId = $(parentNode).val();
//				$($(this).parent().find("input[name=parent]")[0]).val(parentId);
//			}
//		});
//	}
//	$('<input type="hidden" name="level" value="0" /><input type="hidden" name="parent" value="0" />').prependTo($(".uk-nestable-item", $elem));
//	checkParents();
//
//	return $elem;
//};
