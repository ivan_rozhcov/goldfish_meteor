#TODO с одной стороны проще сделать, чтобы плагины могли быть только на строное клиента
# с другой не очень гибко

#TODO проверять, что включена для данного юзера
Meteor.startup ->
  Tracker.autorun ->
    exportToEvernoteExtension = getEvernoteExtension()

    if exportToEvernoteExtension
      insertLink = ($listHeaderQuickIcons) ->
        $link = $("<li> <a href='#' class='export-to-evennote-plot'>Export to evernote</a></li>")
        $link.on("click", clickListener)
        $listHeaderQuickIcons.append($link)

      clickListener = (event) ->
        #TODO плагинам хорошо бы предоставлять API с текущим выбранным плотом и тд.
        # интересно, а можно события через compose подсовывать?
        info = ->
          bootbox.alert 'Заметка экспортирована успешно'
        plotId = _.last(window.location.href.split('/'))
        plot = Plots.findOne(plotId)

        incompatibleNodeShapes = NodeShapes.find(plot_id: plotId, view_type: $in: exportToEvernoteExtension.incompatible_node_shapes).fetch()
        if exportToEvernoteExtension.available_plots and plot.type in exportToEvernoteExtension.available_plots
          if incompatibleNodeShapes
            plotId = clonePlot(plotId, plot.type)
            Meteor.setTimeout(->
              Router.go('plotTab', _id: plotId)
            , 500)

            #обновляем ноды на совместимые
            incompatibleNodeShapes = NodeShapes.find(plot_id: plotId, view_type: $in: exportToEvernoteExtension.incompatible_node_shapes).fetch()
            for incompatibleNodeShape in incompatibleNodeShapes
              NodeShapes.update incompatibleNodeShape._id, $set: view_type: 'nsl_readonly'

            exportToEvernote(plotId, exportToEvernoteExtension.export_zappier_url, window.location.href, info, window.location.origin)
          else
            exportToEvernote(plotId, exportToEvernoteExtension.export_zappier_url, window.location.href, info, window.location.origin)
        else
          unless plot.type == 'nsg_evernote_list'
            plotId = clonePlot(plotId, 'nsg_evernote_list')

            #TODO по идее тут preventDefault поможет
            Meteor.setTimeout(->
              Router.go('plotTab', _id: plotId)
            , 500)

          #обновляем ноды на совместимые
          incompatibleNodeShapes = NodeShapes.find(plot_id: plotId, view_type: $in: exportToEvernoteExtension.incompatible_node_shapes).fetch()
          for incompatibleNodeShape in incompatibleNodeShapes
            NodeShapes.update incompatibleNodeShape._id, $set: view_type: 'nsl_readonly'

          exportToEvernote(plotId, exportToEvernoteExtension.export_zappier_url, window.location.href, info, window.location.origin)

      #через compose можно достаточно легко расширять функционал
      Template.plotTab.rendered = _.compose(->
        insertLink($(".plot-menu-dropdown > .dropdown-menu"))
      , Template.plotTab.rendered)

      # тут исполняем код
      Meteor.startup ->
        insertLink($(".plot-menu-dropdown > .dropdown-menu"))

#, если нужны хуки
#Cards.before.insert (userId, card) ->
#  list = Lists.findOne(card.listId)
#  if userId in list.memberIds and not card.memberIds.length
#    card.memberIds.push(userId)