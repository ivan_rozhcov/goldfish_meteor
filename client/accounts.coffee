Accounts.ui.config(
  extraSignupFields: [],
  requestPermissions: {
       google:[
         'https://www.google.com/m8/feeds',
         'https://www.googleapis.com/auth/calendar',
         'https://www.googleapis.com/auth/userinfo.profile',
         'https://www.googleapis.com/auth/tasks'
      ]
  },
  requestOfflineToken: {
    google: true
  },
  forceApprovalPrompt: {
    google: true
  }
)