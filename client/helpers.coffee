
Template.registerHelper 'isPlotView', ->
    Router.current().lookupTemplate() in ['plotTab', 'PlotTab']

Template.registerHelper 'isPlotsView', ->
    Router.current().lookupTemplate() == 'Content'

Template.registerHelper "isDebug",  ->
  Session.equals "template_debug_mode", true

#режимы работы с плотом
Template.registerHelper "getCurrentPlotMode",  ->

  #в переменной вида plotvpZpgh7KCYaRQKQFD записывает текущий режим
  getPlotState @_id

Template.registerHelper "getCurrentPlotModeNs",  ->
  getPlotState @node_shape.plot_id

Template.registerHelper "getCursorForState", (input) ->

  #в переменной вида plotvpZpgh7KCYaRQKQFD записывает текущий режим
  return "auto"  if getPlotState(@_id) is "viewing"
  return "move"  if getPlotState(@_id) is "dragging node"
  "alias"  if getPlotState(@_id) is "connecting link"

Template.registerHelper "isEditing", (input) ->
  getEditingNodeShapeId(@plot_id) is @_id

Template.registerHelper "isNodeSelectRelated", (input) ->
  Session.equals "show_node_relations_id", @node_id

Template.registerHelper "isEditingPlotTitle", (input) ->
  Session.equals "editing_plot_id", @_id

Template.registerHelper "isPlotSelected", (input) ->
  Session.equals "selectedPlotId", @_id

Template.registerHelper "autoUpdatePlot", (input) ->
  Session.get "autoSelectPlot"

Template.registerHelper "updateSelectedPlot", (input) ->
  Session.get("autoSelectPlot") and Session.equals("selectedPlotId", @_id)

Template.registerHelper "isNodeSelected", (input) ->
  Session.equals "selectedNodeId", @_id

Template.registerHelper "updateSelectedNode", (input) ->
  Session.get("autoSelectNode") and Session.equals("selectedNodeId", @_id)

Template.registerHelper "isLinkSelected", (input) ->
  Session.equals "selectedLinkId", @_id

Template.registerHelper "isNodeShapeSelected", (input) ->
  getSelectedNodeShapeId(@plot_id) is @_id

Template.registerHelper "getSelectedNodeShape", ->
  NodeShapes.findOne(getSelectedNodeShapeId(@_id))

Template.registerHelper "isNodeShapeSelectedById", (id) ->
  #это глобальный нод шейп (последний выбранный)
  getSelectedNodeShapeIdGlobal() is id

Template.registerHelper "isNodeSelectedById", (id) ->
  Session.get("selectedNodeId") is id

#TODO не очень эффективно
Template.registerHelper "isNodeShapeTmpById", (id) ->
  NodeShapes.findOne(_id: id, type: "tmp-node")

Template.registerHelper "isNodeShapeSelectedMultiple", (input) ->
  selectedNodes = Session.get "#{@plot_id}_selected_nodes"
  unless selectedNodes
      selectedNodes = []
  @_id in selectedNodes

Template.registerHelper "isLinkShapeSelected", (input) ->
  Session.get("selectedLinkShapeId") is @_id

Template.registerHelper "isFullScreen", (input) ->
  Session.get("fullscreenNodeShapeId") is @_id

Template.registerHelper "eq", (x1, x2) ->
  x1 is x2

Template.registerHelper "div", (x1, x2) ->
  x1 / x2

Template.registerHelper "plus", (x1, x2) ->
  x1 + x2

Template.registerHelper "getAttr", (scope, arg) ->
  # по другому не получается, чтобы получить доступ к скопу шаблона мы его передаем первым агуметом
  res = deepFind(scope, arg)
  #в шаблоне используется with и он пропускает пустые знаение
  if res is null
    'empty_val'
  else
    res

Template.registerHelper 'dateAgo',(date) ->
    moment(date).fromNow()

Template.registerHelper 'renderNodeContent',(_id) ->
    if _id
      el = Nodes.findOne(_id)
      if el and el.attrs
        el.attrs.content

#TODO так можно возвращать html - но пока это не нужно
#Template.registerHelper 'renderNodeIcon',(_id) ->
#    if _id
#      el = Nodes.findOne(_id)
#      console.log 'el', el
#      if el and el.attrs
##        el.attrs.picture
#        console.log '<span class="glyphicon glyphicon-info-sign"></span>'
#        new Handlebars.SafeString('<span class="glyphicon glyphicon-info-sign"></span>')

Template.registerHelper 'renderNodeAttribute',(_id, attr) ->
    if _id
      el = Nodes.findOne(_id)
      if el?.attrs?
        el.attrs[attr]

getNodeTitle = (id) ->
  ns = Nodes.findOne(id)
  if ns and ns.attrs
    if ns then ns.attrs.title or ns.attrs.content else 'not found'
  else
    'not found'

getNodeShapeTitle = (id) ->
  ns = NodeShapes.findOne(id)
  if ns and ns.node_attrs
    if ns then ns.node_attrs.title or ns.node_attrs.content else 'not found'
  else
    'not found'

Template.registerHelper 'renderNodeShapeById',(id) ->
    str = getNodeShapeTitle(id)
    if str and str.length > 10 then str[0..10] + '...' else str

Template.registerHelper 'getNodeShapeTitle',(id) ->
    getNodeShapeTitle(id)

Template.registerHelper 'getNodeTitle',(id) ->
    getNodeTitle(id)

Template.registerHelper 'getUserName', renderUser

Template.registerHelper 'getNewNodesCount', (plotId) ->
    plot = Plots.findOne(_id: plotId, users: Meteor.userId())
    NodeShapes.find(plot_id: plot._id, has_new_version: true, type: $ne: 'tmp-node').count()

Template.registerHelper "isNodeShapeHasOutgoingLinks", (input) ->
  hasOutgoingLinkShapes(@)

Template.registerHelper "isNodeShapeHasIncomingLinks", (input) ->
  hasIncomingLinkShapes(@)

Template.registerHelper "NodeShapeGetOutgoingLinks", (input) ->
  getOutgoingLinkShapes(@)

Template.registerHelper "NodeShapeGetIncomingLinks", (input) ->
  getIncomingLinkShapes(@)

Template.registerHelper "isDataTypeIconPicture", () ->
  not @data_icon.startsWith('glyphicon')

Template.registerHelper "nodeHasAttr", (attr) ->
  node = Nodes.findOne(@node_id)
  if node
    deepFind(node, attr)


Template.registerHelper 'arrayify', (obj) ->
  result = []
  for key of obj
    result.push obj[key]
  result

Template.registerHelper 'ruDateTime', (date) ->
    moment(date).format('DD.MM.YYYY HH:mm') if date
  
Template.registerHelper 'isNumber', (numb) ->
  not isNaN numb