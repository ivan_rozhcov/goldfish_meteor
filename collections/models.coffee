@Plots = new Mongo.Collection("plots")

#TODO Эти данные вообще не используются (читается из Plot), а надо бы читам из них
@NodeShapes = new Mongo.Collection("node_shapes")
@LinkShapes = new Mongo.Collection("link_shapes")

# TODO сейчас все данные обновляются в плоте по хорошему нужно использовать хук
@Nodes = new Mongo.Collection("nodes")
@Links = new Mongo.Collection("links")
@Actions = new Mongo.Collection("actions")

#TODO это наверное нужно сделать попользовательно!
@NodeTypesToWidgets = new Mongo.Collection("node_types_to_widgets")
@NodeTypes = new Mongo.Collection("node_types")
@NodeStorageTypes = new Mongo.Collection("node_storage_types")
@NodeWidgetTypes = new Mongo.Collection("node_widget_types")
@NodeShapeAdapters = new Mongo.Collection("node_shapes_adapters")
@NodeShapeAdaptersDefaultWidgets = new Mongo.Collection("node_shapes_adapters_default_widgets")

@TutorialPages = new Mongo.Collection("tutorial_pages")

@ServerSettings = new Mongo.Collection("server_settings")

@Extensions =  new Mongo.Collection("extensions")
