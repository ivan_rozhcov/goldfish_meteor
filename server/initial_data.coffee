Meteor.startup ->
#TODO по хорошему это в profile

  #to renew start meteor mongo
  #db.node_types_to_widgets.remove();

  #список установленных "понимаемых" типов данных
  if not NodeTypes.find().count()
    NodeTypes.insert(type: 'text')
    NodeTypes.insert(type: 'html')
#    NodeTypes.insert(type: 'markdown')
#    NodeTypes.insert(type: 'image')
#    NodeTypes.insert(type: 'video')
#    NodeTypes.insert(type: 'url')
  #Тут еще надо проверять, что этот сервис активен
#    NodeTypes.insert(type: 'evernote')

  #список установленных вариахтов хранения нодов - сейчас не используется
  #TODO а как же линки?
  if not NodeStorageTypes.find().count()
    NodeStorageTypes.insert(type: 'mongo')
#    NodeStorageTypes.insert(type: 'filesystem')
#    NodeStorageTypes.insert(type: 'evernote')
#    NodeStorageTypes.insert(type: 'gdrive')
#    NodeStorageTypes.insert(type: 'dropbox')

  if not NodeWidgetTypes.find().count()
    NodeWidgetTypes.insert(type: 'view_and_edit')
    NodeWidgetTypes.insert(type: 'view_and_edit_external')
    NodeWidgetTypes.insert(type: 'view_only')

  end = false
  while not end
    #список установленных виждетов для типов данных
    if not NodeTypesToWidgets.find().count()
      NodeTypesToWidgets.insert(type: 'text', avaible_types: ['nsl_readonly', 'nsl_textarea', 'nsl_input', 'nsl_textarea_with_title', 'nsl_attribute_editor', 'nsl_checkbox'])
      NodeTypesToWidgets.insert(type: 'html', avaible_types: ['nsl_readonly', 'nsl_textarea', 'nsl_ckeditor', 'nsl_attribute_editor'])
      #для нодов
      NodeTypesToWidgets.insert(type: 'adapter', avaible_types: ['nsl_plot_image_preview', 'nsl_plot','nsl_plot_with_title', 'nsl_attribute_editor'])
      NodeTypesToWidgets.insert(type: 'adapter-dynamic', avaible_types: ['nsl_plot','nsl_plot_with_title', 'nsl_attribute_editor'])
      #plots
      #TODO тут для связности нужно придумать что то посложней, может быть просто по группам сгрупировать type: 'group'
      NodeTypesToWidgets.insert(type: 'nsg_graph_plot',
      avaible_types: ['nsg_graph_plot', 'nsg_ordered_list', 'nsg_tree_plot', 'nsg_table_plot_full', 'nsg_grid_list_ns', 'nsg_graph_plot_ext_edit', 'nsg_graph_main_list', 'nsg_table_plot_content', 'nsg_dynamic_table_plot', 'nsg_table_plot'],
      )
      NodeTypesToWidgets.insert(type: 'nsg_ordered_list', avaible_types: ['nsg_ordered_list', 'nsg_graph_plot'])
      NodeTypesToWidgets.insert(type: 'nsg_table_plot_full', avaible_types: ['nsg_table_plot_full', 'nsg_graph_plot'])
      NodeTypesToWidgets.insert(type: 'nsg_graph_plot_ext_edit', avaible_types: ['nsg_ordered_list', 'nsg_graph_plot'])
      NodeTypesToWidgets.insert(type: 'nsg_table_plot', avaible_types: ['nsg_table_plot', 'nsg_graph_plot'])
      NodeTypesToWidgets.insert(type: 'nsg_table_plot_content', avaible_types: ['nsg_table_plot_content', 'nsg_table_plot', 'nsg_graph_plot'])
      NodeTypesToWidgets.insert(type: 'nsg_dynamic_table_plot', avaible_types: ['nsg_dynamic_table_plot', 'nsg_table_plot', 'nsg_graph_plot'])
      NodeTypesToWidgets.insert(type: 'nsg_grid_list_ns', avaible_types: ['nsg_dynamic_table_plot', 'nsg_table_plot', 'nsg_graph_plot'])
      NodeTypesToWidgets.insert(type: 'nsg_graph_main_list', avaible_types: ['nsg_dynamic_table_plot', 'nsg_table_plot', 'nsg_graph_plot'])
      NodeTypesToWidgets.insert(type: 'nsg_tree_plot', avaible_types: ['nsg_dynamic_table_plot', 'nsg_table_plot', 'nsg_graph_plot'])

    #адаптеры - плоты
    if not NodeShapeAdapters.find().count()
      syncNodeShapeAdapters()


    #дефолтные виджеты для адаптеров
    if not NodeShapeAdaptersDefaultWidgets.find().count()
      NodeShapeAdaptersDefaultWidgets.insert(type: 'nsg_graph_plot', default_widget: 'nsl_textarea', default_content_type: 'text')
    #информация про туториалы
    if not TutorialPages.find().count()
      TutorialPages.insert(title: 'Tutorial 1', seen_by: [], plot_id: null, type: 'tutorial', order: 1, func: 'tutorial_1();')
      TutorialPages.insert(title: 'Tutorial 2', seen_by: [], plot_id: null, type: 'tutorial', order: 2, func: 'tutorial_2();')
      TutorialPages.insert(title: 'Tutorial 3', seen_by: [], plot_id: null, type: 'tutorial', order: 3, func: 'tutorial_3();')
      TutorialPages.insert(title: 'Tutorial 4', seen_by: [], plot_id: null, type: 'tutorial', order: 4, func: 'tutorial_4();')

    settings = ServerSettings.findOne()

    if not settings or not settings.migration or settings.migration == 0
      console.log 'migrating'
      #migrate all title & contnent
      for ns in NodeShapes.find(content: $exists: true).fetch()
        NodeShapes.update ns._id,
            $set: 'node_attrs.content': ns.content, hooks_do_sync: false
          $unset: content: ''
      for ns in NodeShapes.find(title: $exists: true).fetch()
        NodeShapes.update ns._id,
            $set: 'node_attrs.title': ns.title,  hooks_do_sync: false
          $unset: content: ''

      for ns in Nodes.find(content: $exists: true).fetch()
        Nodes.update ns._id,
            $set: 'attrs.content': ns.content, hooks_do_sync: false
          $unset: content: ''
      for ns in Nodes.find(title: $exists: true).fetch()
        Nodes.update ns._id,
            $set: 'attrs.title': ns.title,  hooks_do_sync: false
          $unset: content: ''

      if not settings
        ServerSettings.insert migration: 1
        settings = ServerSettings.findOne()
      else
        ServerSettings.update settings._id, $set: migration: 1


    if settings.show_default_menu == undefined
      ServerSettings.update settings._id, $set: show_default_menu: false

    if settings and settings.migration == 1
      Plots.update(system_type: 'types', {$set: {default_widget: 'nsl_type'}}, multi: true)
      ServerSettings.update settings._id, $set: migration: 2
    else if settings and settings.migration == 2
      NodeShapeAdaptersDefaultWidgets.insert type: 'nsg_tree_plot', default_widget: 'nsl_textarea', default_content_type: 'text'
      ServerSettings.update settings._id, $set: migration: 3
    else if settings and settings.migration == 3
      NodeTypes.insert(type: 'url')
      NodeTypesToWidgets.insert(type: 'url', avaible_types: ['nsl_iframe', 'nsl_iframe_url', 'nsl_iframe_preview', 'nsl_attribute_editor' , 'nsl_url'])
      ServerSettings.update settings._id, $set: migration: 4
    else if settings and settings.migration == 4
      NodeTypesToWidgets.remove type: 'html'
      NodeTypesToWidgets.insert(type: 'html', avaible_types: ['nsl_readonly', 'nsl_textarea', 'nsl_ckeditor', 'nsl_attribute_editor'])
      ServerSettings.update settings._id, $set: migration: 5
    else if settings and settings.migration == 5
      NodeTypesToWidgets.update {type: 'adapter'},
        $addToSet: avaible_types: 'nsl_plot_image_preview'
      NodeTypesToWidgets.update {type: 'adapter-dynamic'},
        $addToSet: avaible_types: 'nsl_plot_image_preview'
      ServerSettings.update settings._id, $set: migration: 6
    else if settings and settings.migration == 6
      for plot in Plots.find().fetch()
        nodeId = plot.node_id
        node = Nodes.findOne(nodeId)
        if node.attrs and node.attrs.content == null
          Nodes.update(node._id, $set: 'attrs.content': [])
      ServerSettings.update settings._id, $set: migration: 7
    else if settings and settings.migration == 7
      Extensions.insert(name: 'export_to_evernote')
      ServerSettings.update settings._id, $set: migration: 8
    else if settings and settings.migration == 8
      NodeTypesToWidgets.update {type: 'html'},
        $addToSet: avaible_types: 'nsl_readonly_with_title'
      ServerSettings.update settings._id, $set: migration: 9
    else if settings and settings.migration == 9
      NodeTypesToWidgets.update {type: 'url'},
        $addToSet: avaible_types: 'nsl_iframe_title'
      NodeTypesToWidgets.insert(type: 'pdf', avaible_types: ['nsl_iframe', 'nsl_iframe_title', 'nsl_iframe_url', 'nsl_attribute_editor' , 'nsl_url'])
      NodeTypesToWidgets.insert(type: 'image', avaible_types: ['nsl_image', 'nsl_image_title', 'nsl_image_url', 'nsl_attribute_editor' , 'nsl_url'])
      NodeTypesToWidgets.insert(type: 'video', avaible_types: ['nsl_video', 'nsl_video_title', 'nsl_video_url', 'nsl_attribute_editor' , 'nsl_url'])
      NodeTypesToWidgets.insert(type: 'audio', avaible_types: ['nsl_audio', 'nsl_audio_title', 'nsl_audio_url', 'nsl_attribute_editor' , 'nsl_url'])
      ServerSettings.update settings._id, $set: migration: 10
    else if settings and settings.migration == 10
      NodeTypesToWidgets.update {type: 'html'},
        $addToSet: avaible_types: 'nsl_contenteditable'
      ServerSettings.update settings._id, $set: migration: 11
    else if settings and settings.migration == 11
      TutorialPages.remove({})
      TutorialPages.insert(title: 'Tutorial #1 Базовые операции', seen_by: [], plot_id: null, type: 'tutorial', order: 1, func: 'tutorial_1();')
      TutorialPages.insert(title: 'Tutorial #2 Плоты', seen_by: [], plot_id: null, type: 'tutorial', order: 2, func: 'tutorial_2();')
      TutorialPages.insert(title: 'Tutorial #3 Связи', seen_by: [], plot_id: null, type: 'tutorial', order: 3, func: 'tutorial_3();')
      TutorialPages.insert(title: 'Tutorial #4 Клоны нодов', seen_by: [], plot_id: null, type: 'tutorial', order: 4, func: 'tutorial_4();')
      TutorialPages.insert(title: 'Tutorial #5 Фрагменты', seen_by: [], plot_id: null, type: 'tutorial', order: 4, func: 'tutorial_5();')
      TutorialPages.insert(title: 'Tutorial #6 Виды нода', seen_by: [], plot_id: null, type: 'tutorial', order: 4, func: 'tutorial_6();')
      TutorialPages.insert(title: 'Tutorial #7 Типы Нода', seen_by: [], plot_id: null, type: 'tutorial', order: 4, func: 'tutorial_7();')
      TutorialPages.insert(title: 'Tutorial #8 Размещение на плоте других плотов', seen_by: [], plot_id: null, type: 'tutorial', order: 4, func: 'tutorial_8();')
      TutorialPages.insert(title: 'Tutorial #9 Тип представления плотов', seen_by: [], plot_id: null, type: 'tutorial', order: 4, func: 'tutorial_9();')
      TutorialPages.insert(title: 'Tutorial #10 Клоны плотов', seen_by: [], plot_id: null, type: 'tutorial', order: 4, func: 'tutorial_10();')
      ServerSettings.update settings._id, $set: migration: 12
    else if settings and settings.migration == 12
      NodeShapeAdaptersDefaultWidgets.insert type: 'nsg_checklist_plot', default_widget: 'nsl_contenteditable', default_content_type: 'text', width: '80%', height: '100%'
      NodeTypesToWidgets.update({type: 'nsg_graph_plot'}, {$set: avaible_types: ['nsg_graph_plot', "nsg_tree_plot", 'nsg_checklist_plot', "nsg_ordered_list",
        "nsg_grid_list_ns", "nsg_graph_plot_ext_edit", "nsg_graph_main_list", "nsg_table_plot_content","nsg_dynamic_table_plot", "nsg_table_plot"]})
      NodeTypesToWidgets.update {type: 'text'},
        $addToSet: avaible_types: 'nsl_contenteditable'
      ServerSettings.update settings._id, $set: migration: 13
    else if settings and settings.migration == 13
      NodeTypesToWidgets.update({type: 'adapter'}, {$set: {avaible_types: ['nsl_plot_image_preview', 'nsl_plot','nsl_plot_with_title']}})
      ServerSettings.update settings._id, $set: migration: 14
    else
      end = true

#Migrations
#createPlot('Pins', 'nsg_graph_plot', [Meteor.userId()], system_type: 'pins', system: true)