SyncedCron.add
  name: 'Получать аннотации по веб страницам'
  schedule: (parser) ->
    # parser is a later.parse object
    parser.text 'every 3 minutes'
  job: ->
    getWebPageAnnotations()


SyncedCron.add
  name: 'Reasoner'
  schedule: (parser) ->
    # parser is a later.parse object
    parser.text 'every 2 minutes'
  job: ->
    createAuto()

SyncedCron.add
  name: 'Получать контент для веб страниц'
  schedule: (parser) ->
    # parser is a later.parse object
    parser.text 'every 3 minutes'
  job: ->
    getWebPageContent()


SyncedCron.start()

#TODO пока выключил, тк плодит лишние ноды + нету нормальных нодшейпов
#SyncedCron.add
#  name: 'API для распознавания смысла текста'
#  schedule: (parser) ->
#    # parser is a later.parse object
#    parser.text 'every 15 minutes'
#  job: ->
#    parseNodesDusi()

