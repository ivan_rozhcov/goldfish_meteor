

Meteor.methods
  parseNodes: (options) -> parseNodesDusi

  #TODO это тоже в хуки
  saveOtherLinkShapeChanges: (options) ->
    linkId = options.linkId
    editedLinkShapeId = options.editedLinkShapeId
    Links.update
      _id: linkId
    ,
      $set:
        title: options.newText

      $inc:
        version: 1

    link = Links.findOne(_id: linkId)

    #обвноялем другие плоты
    LinkShapes.update
      link_id: linkId
      _id:
        $ne: editedLinkShapeId
    ,
      $set:
        has_new_version: true
    ,
      multi: true

    LinkShapes.update
      link_id: linkId
      _id:
        $ne: editedLinkShapeId

      $or: [
        {
          has_conflict: false
        }
        {
          has_conflict: null
        }
      ]
    ,
      $set:
        title: link.title
        link_version: link.version
    ,
      multi: true

    return

  hideLinkShapesOnNodeShape: (options) ->
    LinkShapes.update
      plot_id: options.plotId
      $or: [
        {
          node_shape_from_id: options.nodeShapeId
        }
        {
          node_shape_to_id: options.nodeShapeId
        }
      ]
    ,
      $set:
        type: "tmp-link"
    ,
      multi: true

    return

  updateNodeShapePosition: (options) ->
    nodeShapeId = options.nodeShapeId
    pos = options.newPos

    svgYoffset = 0
    svgXoffset = 0
    NodeShapes.update nodeShapeId,
      $set:
        x: pos.left
        y: pos.top

    LinkShapes.update
      node_shape_from_id: nodeShapeId
    ,
      $set:
        start_x: pos.left + svgXoffset
        start_y: pos.top + svgYoffset
    ,
      multi: true

    LinkShapes.update
      node_shape_to_id: nodeShapeId
    ,
      $set:
        end_x: pos.left + svgXoffset
        end_y: pos.top + svgYoffset
    ,
      multi: true

    return

  showRelatedNodesLinks: (options) ->
    #nodeShapeId, plotId,
    options = _.extend(options, nodeOldType: "tmp-node", nodeNewType: "tmp-node-show", linkOldType:"tmp-link", linkNewType: "tmp-link-show")
    nodeShapeChangeMode(options)

  showAutoNodesLinks: (options) ->
    #nodeShapeId, plotId,
    options = _.extend(options, nodeOldType: "tmp-node-auto", nodeNewType: "tmp-node-auto-show", linkOldType:"tmp-link-auto", linkNewType: "tmp-link-show-auto")
    nodeShapeChangeMode(options)

  clearRelatedForPlot: (plotId) ->
    nodeShapePlotChangeMode(plotId, "tmp-node-show", "tmp-node", "tmp-link-show", "tmp-link")
    return

  clearAutoForPlot: (plotId) ->
    nodeShapePlotChangeMode(plotId, "tmp-node-auto-show", "tmp-node-auto", "tmp-link-show-auto", "tmp-link-auto")
    return


  #    Meteor.call('cmd', 'pdfgrep PDF /Users/ivan/projects/goldfish_meteor/public/pdf-test.pdf -n', function (err, data) {
  #          Session.set('output', data );})
  cmd: (command) ->
    Future = Meteor.npmRequire("fibers/future")
    explode = command.split(" ")
    cmd = explode[0]
    args = explode.splice(1, explode.length)
    console.log "e:" + cmd + JSON.stringify(args)
    fut = new Future()
    spawn = Meteor.npmRequire("child_process").spawn
    if args.length is 0
      ls = spawn(cmd)
    else
      ls = spawn(cmd, args)
    ls.stdout.on "data", (data) ->

      #        createNode();
      console.log "" + data
      return

    ls.stderr.on "data", (data) ->
      console.log "" + data
      return

    fut.wait()

  saveFile: (blob, name, path, encoding) ->

    # Clean up the path. Remove any initial and final '/' -we prefix them-,
    # any sort of attempt to go to the parent directory '..' and any empty directories in
    # between '/////' - which may happen after removing '..'

    # TODO Add file existance checks, etc...
    cleanPath = (str) ->
      str.replace(/\.\./g, "").replace(/\/+/g, "").replace(/^\/+/, "").replace /\/+$/, ""  if str
    cleanName = (str) ->
      str.replace(/\.\./g, "").replace /\//g, ""
    path = cleanPath(path)
    fs = Meteor.npmRequire("fs")
    name = cleanName(name or "file")
    encoding = encoding or "binary"
    chroot = Meteor.settings.file_path
    path = chroot + ((if path then "/" + path + "/" else "/"))
    fs.writeFile path + name, blob, encoding, (err) ->
      if err
        console.log err
        throw (new Meteor.Error(500, "Failed to save file.", err))
      else
        console.log "The file " + name + " (" + encoding + ") was saved to " + path
      return

    return

  sendEmail: (to, from, subject, text) ->
    check([to, from, subject, text], [String])
    @unblock()
    Email.send({
      to: to,
      from: from,
      subject: subject,
      text: text
    })

  deleteSomeHistory: () ->
    Actions.remove({user: Meteor.userId()})

  emptyTrash: () ->
    skipNodeShapes = []
    trashPlot = Plots.findOne(system_type:'trash', users: Meteor.userId())
    # попробуем удлалять частями
    nIds = []
    for ns in NodeShapes.find({plot_id: trashPlot._id}).fetch()
      # если других нодшейпов не найдено, то помечаем нод как trash_only
      unless Nodes.find(node_id: ns.node_id, _id: $ne: ns._id).count()
        Nodes.update(ns.node_id, $set: trash_only_nodeshapes: true)
      nIds.push ns.node_id
    #      TODO пока не понятно, как удалять плоты
      if ns.representing_plot_id
        plot = Plots.findOne ns.representing_plot_id
        if plot
          if plot.is_internal
            # если это nested плот и нету других нод шейпов поменчаем
            Plots.update(plot._id, $set: trash_only_nodeshapes_internal: true)
          else
            # если это просто плот и нету других нод шейпов поменчаем, по другому
            Plots.update(plot._id, $set: trash_only_nodeshapes: true)
        else
          skipNodeShapes.push ns._id


    for ls in LinkShapes.find({plot_id: trashPlot._id}).fetch()
      # если других нодшейпов не найдено, то помечаем нод как trash_only
      unless Links.find(link_id: ls.link_id, _id: $ne: ls._id).count()
        Links.update(ls.link_id, $set: trash_only_linkshapes: true)

    LinkShapes.direct.remove(plot_id: trashPlot._id)
    # удаляем все нодшейпы из корзины
    NodeShapes.direct.remove(plot_id: trashPlot._id, _id: $nin: skipNodeShapes)
    # удаяем ноды из нода-адаптера. наяпрямую, чтобы не было лишней логики
    Nodes.direct.update(trashPlot.node_id, {$pull: {'attrs.content': {$in: nIds}}})

  deleteLink: (linkId) ->
    LinkShapes.remove(link_id: linkId)
    Links.remove(linkId)

  readContentTest: (fileId) ->
    fs = Npm.require('fs')
    # Read from a CollectionFs FS.File
    # Assumes you have a "Pdfs" CollectionFs
    file = Files.findOne(_id: fileId)
    readable = file.createReadStream('tmp')
    console.log readable
    bytes = fs.readFileSync(readable)
    console.log bytes
    buffer = new Buffer(0)
    readable.on 'data', (buffer) ->
      buffer = buffer.concat([
        buffer
        readable
      ])
      return
    readable.on 'end', ->
      console.log buffer.length
      console.log buffer
      return

  #TODO это перестало работать!
  extractAreaFromImage: (self, area) ->
    [[x1,y1], [x2,y2]] = area

    path = "#{process.env.PWD}/public#{self.node_external_uri}"
    fileName = getFileName(self.node_external_uri)


    width = Math.abs(x2 - x1)
    height = Math.abs(y2 - y1)
    console.log "#{width}x#{height}+#{x1}+#{y1}"
#    '40x30+10+10'
    nodeShapeId = createNodeShapeFromNodeShape(self, null, null, null, width, height, true)

    #TODO node id
    fileNameNew = "#{nodeShapeId}_cropped"
    destPath = "#{process.env.PWD}/public#{self.node_external_uri.replace(fileName, fileName + fileNameNew)}"
    console.log path, destPath

    Imagemagick.convert([path, '-crop', "#{width}x#{height}+#{x1}+#{y1}", destPath])
    NodeShapes.update(nodeShapeId, $set: node_external_uri: "#{self.node_external_uri.replace(fileName, fileName + fileNameNew)}")
    nodeShapeId

  deleteChildren: (objNodeShape, toIds, linkType) ->
    for id in toIds
      console.log id

      ls = LinkShapes.findOne(
        node_shape_from_id: objNodeShape._id
        node_shape_to_id: id
        title: linkType
        type:
              $nin: [
                "tmp-link"
                "tmp-link-show"
              ]
        plot_id: objNodeShape.plot_id
      )
#      console.log ls
      LinkShapes.remove(ls._id)
      #TODO remove link?
  #TODO до конца так и не заработал
  getFileContent: (fileId) ->
    fs = Npm.require('fs')

    #Get the file itself as a BLOB (Buffer)
    file = Images.findOne(fileId)
    readStream = file.createReadStream()

    bytes = fs.readFileSync(readStream.path)
    buf = new Buffer(bytes, 'binary')
    return buf
  createNodeShapeAdapter: (adapterPlotId, x, y, plotId, width, height, options) ->
    createNodeShapeAdapter(adapterPlotId, [Meteor.userId()], x, y, plotId, width, height, options)

  createNodeShapeFromNode: (nodeId, plotId, x, y, width, height, viewType, editType, options, direct) ->
    #как быть с клонами??? - у них одни node_id, тут сейчас выбирается 1й
    # считаем, что плот есть в подписке
    plot = Plots.findOne(node_id: nodeId)
    if plot
      # если плот найден создаем адаптер
      createNodeShapeAdapter(plot._id, [Meteor.userId()], x, y,plotId, width, height, options)
    else
      createNodeShapeFromNode(nodeId, plotId, x, y, width, height, viewType, editType, options, direct)

  cloneNodeShape: (nodeShapeId, options) ->
    cloneNodeShape(nodeShapeId, options)

  recreateHypothesis: (node_id) ->
    nodeIds = findNodesWithLink(node_id, 'annotation')
    for nodeId in nodeIds
      for ns in NodeShapes.find(node_id: nodeId).fetch()
        removeNodeShape(ns._id, true)
    Nodes.update(node_id, $set: proccessed_by_hypothes: false)

  removeNodeShape: (nodeShapeId) ->
    removeNodeShape(nodeShapeId)

  recreateEmbedly: (node_id) ->
    nodeIds = findNodesWithLink(node_id, 'embedly_content')
    for nodeId in nodeIds
      for ns in NodeShapes.find(node_id: nodeId).fetch()
        removeNodeShape(ns._id, true)
    #TODO removeNode
    Nodes.update(node_id, $set: proccessed_by_embedly: false)

  remoteEmbedly: () ->
    linkShapesAggregate = LinkShapes.aggregate(
      {
        $group: {
#Group by fields to match
          _id: {
            node_from_id: '$node_from_id',
            node_to_id: '$node_to_id',
            node_shape_from_id: '$node_shape_from_id',
            plot_id: '$plot_id',
            link_id: '$link_id',
            title: 'embedly_content',
            link_type: 'auto',
          },

#Count number of matching docs for the group
          count: {$sum: 1},
# Save the _id for matching docs
          docs: {$push: "$_id"}
        }
      },

# Limit results to duplicates (more than 1 match)
      {
        $match: {
          count: {$gt: 1}
        }
      })
    for lsa in linkShapesAggregate
      remainingDoc = lsa.docs.pop()
      console.log 'remaining link shape', remainingDoc
      #остальные удаляем
      for linkShapeId in lsa.docs
        removeNodeShape(LinkShapes.findOne(linkShapeId).node_shape_to_id, true)
    console.log 'done'
      