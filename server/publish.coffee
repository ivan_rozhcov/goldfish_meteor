Meteor.publish null, ->
  if @userId
    inboxPlot = Plots.findOne(system_type:'inbox', users: @userId)
    if inboxPlot
      Counts.publish @, 'inbox-count', NodeShapes.find(plot_id: inboxPlot._id)
    trashPlot = Plots.findOne(system_type:'trash', users: @userId)
    if trashPlot
      Counts.publish @, 'trash-count', NodeShapes.find(plot_id: trashPlot._id)
  return undefined    # otherwise coffeescript returns a Counts.publish

Meteor.publish 'plot_one', (_id, filter) ->
  if @userId
    unless _id
      filter = _.extend(filter, users: @userId)
      plot = Plots.findOne(filter)
      _id = plot._id
    else
      plot = Plots.findOne(_id: _id, users: @userId)
    if plot
      @autorun (computation) ->
        additionalPlotIds = Plots.find({users: @userId, system_type: $in: ["protos", "tags", "sessions", "nodeshapes_types", "projects", "types", "pins"]},{fields: _id: 1}).map (itm)-> itm._id
        additionalPlotIds.push _id
        additionalPlotIds = additionalPlotIds.concat(NodeShapes.find({plot_id: _id, representing_plot_id: $exists: true}, {fields: representing_plot_id: 1}).map (itm) -> itm.representing_plot_id)
  #      console.log 'mMgH2MipkELiRyHtM', additionalPlotIds
        nodeShapesNodeIds = NodeShapes.find({plot_id: $in: additionalPlotIds}, {fields: node_id: 1}).map (itm) -> itm.node_id
  #      console.log 'node "vBEbXo3ExgFdC7w9c" - node_shape QheYpCCkx9TuXpMTw', nodeShapesNodeIds

        nodeIds = Nodes.find({_id: $in: nodeShapesNodeIds}, {fields: _id: 1}).map (itm) -> itm._id


        #ищем ноды для плотов
        nodeIds = nodeIds.concat(Plots.find({_id: $in: additionalPlotIds}, {fields: node_id: 1}).map (itm) -> itm.node_id)

        linkIds = Links.find({$or: [{node_from_id:{ $in: nodeIds}}, {node_to_id:{ $in: nodeIds}}]}, {fields: _id: 1}).map (itm) -> itm._id

        allPlotsIds = additionalPlotIds.concat([Plots.findOne(users: @userId, system_type: 'trash')._id])

        #TODO  тут не делается подписка на плоты на плотах и тем более на их нодшейпы и ноды
        # TODO пределать на template level subscription?
        nestedPlotsId = NodeShapes.find({plot_id: _id, representing_plot_id: $exists: true}, {fields: representing_plot_id: 1}).map (itm) -> itm.representing_plot_id

        nestedNestedPlotsId = NodeShapes.find({plot_id: {$in: nestedPlotsId}, representing_plot_id: $exists: true}, {fields: representing_plot_id: 1}).map (itm) -> itm.representing_plot_id

        additionalPlotIds = additionalPlotIds.concat(nestedNestedPlotsId)

        alNodeShapesIds = NodeShapes.find({plot_id: $in: additionalPlotIds}, {fields: _id: 1}).map (itm) -> itm._id
        alNodeShapesIds = alNodeShapesIds.concat(NodeShapes.find({plot_id: $in: nestedNestedPlotsId}, {fields: _id: 1}).map (itm) -> itm._id)

        for nestedPlotId in nestedPlotsId
          nestedPlotsId_ = NodeShapes.find({plot_id: nestedPlotId, representing_plot_id: $exists: true}, {fields: representing_plot_id: 1}).map (itm) -> itm.representing_plot_id

          nestedNestedPlotsId_ = NodeShapes.find({plot_id: {$in: nestedPlotsId_}, representing_plot_id: $exists: true}, {fields: representing_plot_id: 1}).map (itm) -> itm.representing_plot_id

          allPlotsIds = allPlotsIds?.concat(nestedNestedPlotsId_)

          alNodeShapesIds = alNodeShapesIds?.concat(NodeShapes.find({plot_id: $in: nestedNestedPlotsId_}, {fields: _id: 1}).map (itm) -> itm._id)

          for nestedPlotId_ in nestedPlotsId_
            nestedPlotsId__ = NodeShapes.find({plot_id: nestedPlotId_, representing_plot_id: $exists: true}, {fields: representing_plot_id: 1}).map (itm) -> itm.representing_plot_id

            nestedNestedPlotsId__ = NodeShapes.find({plot_id: {$in: nestedPlotsId__}, representing_plot_id: $exists: true}, {fields: representing_plot_id: 1}).map (itm) -> itm.representing_plot_id

            allPlotsIds = allPlotsIds?.concat(nestedNestedPlotsId__)

            alNodeShapesIds = alNodeShapesIds?.concat(NodeShapes.find({plot_id: $in: nestedNestedPlotsId__}, {fields: _id: 1}).map (itm) -> itm._id)

        [
          Plots.find(_id: $in: allPlotsIds),
          NodeShapes.find(_id: $in: alNodeShapesIds),
          LinkShapes.find($or: [{plot_id: plot._id}, link_id: $in: linkIds]),
          Nodes.find(_id: $in: nodeIds),
          Links.find(_id: $in: linkIds)
          NodeShapeAdaptersDefaultWidgets.find(), NodeTypes.find(), Images.find(),
          ServerSettings.find(), NodeShapeAdapters.find(), NodeTypesToWidgets.find(),
          NodeWidgetTypes.find(),
          TutorialPages.find(), Extensions.find(),
          Files.find()
        ]
  else
    []



Meteor.publish 'nodeshape_one', (_id) ->
  nodeShape = NodeShapes.findOne(_id)
  if nodeShape
    plotId = nodeShape.plot_id
    if nodeShape.representing_plot_id
      [
        Plots.find(_id: $in: [plotId,nodeShape.representing_plot_id]),
        NodeShapes.find($or: [{plot_id: nodeShape.representing_plot_id}, _id: _id]),
        LinkShapes.find(plot_id: nodeShape.representing_plot_id),
        NodeShapeAdaptersDefaultWidgets.find(), NodeTypes.find(), Images.find(),
        ServerSettings.find(), NodeShapeAdapters.find(), NodeTypesToWidgets.find(),
        NodeWidgetTypes.find(),
        TutorialPages.find(), Extensions.find(),
        Files.find()
      ]
    else
      [
        NodeShapes.find(_id), Plots.find(plotId),
        NodeShapeAdaptersDefaultWidgets.find(), NodeTypes.find(), Images.find(),
        ServerSettings.find(), NodeShapeAdapters.find(), NodeTypesToWidgets.find(),
        NodeWidgetTypes.find(),
        TutorialPages.find(), Extensions.find(),
        Files.find()
      ]
  
Meteor.publish 'widget_settings', () ->
  [NodeTypes.find(), NodeStorageTypes.find(), NodeWidgetTypes.find(), NodeShapeAdapters.find(), NodeShapeAdaptersDefaultWidgets.find(), NodeTypesToWidgets.find()]

Meteor.publish 'autocomplete-nodes', (selector, options) ->
  selector = _.extend(selector, users: @userId)
  newOpts = Object.assign({}, options)
  optionsForMap = _.extend(newOpts, fields: _id: 1)
  nodeIds = Nodes.find(selector, optionsForMap).map (itm) -> itm._id
  plotsIds = NodeShapes.find(node_id: $in: nodeIds, {fields: plot_id: 1}).map (itm) -> itm.plot_id
  Autocomplete.publishCursor(Nodes.find(selector, options), this)

  #TODO в теории эти данные могут быть всем клиентам доступны?
  Meteor.publish null, () ->
    [Nodes.find(_id: $in: nodeIds), NodeShapes.find(node_id: $in: nodeIds), Plots.find(_id: $in: plotsIds)]
  this.ready()

Meteor.publish 'autocomplete-plots', (selector, options) ->
  selector = _.extend(selector, users: @userId)
  newOpts = Object.assign({}, options)
  optionsForMap = _.extend(newOpts, fields: _id: 1)
  optionsForMap2 = _.extend(newOpts, fields: plot_preview_image: 1)
  plotIds = Plots.find(selector, optionsForMap).map (itm) -> itm._id
  plotImages = Plots.find(selector, optionsForMap2).map (itm) -> itm.plot_preview_image
  Autocomplete.publishCursor(Plots.find(selector, options), this)
  #TODO в теории эти данные могут быть всем клиентам доступны?
  Meteor.publish null, () ->
    [Plots.find(_id: $in: plotIds), Files.find(_id: $in: plotImages)]
  this.ready()

Meteor.publish 'db_stats', ->
  nodeIds =  Nodes.find(users: @userId, {fields: _id: 1}).map (itm) -> itm._id
  linkIds =  Links.find(users: @userId, {fields: _id: 1}).map (itm) -> itm._id
  Counts.publish @, 'nodes-count', Nodes.find(users: @userId)
  Counts.publish @, 'links-count', Links.find(users: @userId)
  Counts.publish @, 'plots-count', Plots.find(users: @userId)
  Counts.publish @, 'nodeshapes-count', NodeShapes.find(node_id: $in: nodeIds)
  Counts.publish @, 'linkshapes-count', LinkShapes.find(link_id: $in: linkIds)
  Counts.publish @, 'actions-count', Actions.find(user: @userId)
  [NodeStorageTypes.find()]

Meteor.publish 'similar_nodes', (_id) ->
  if _id
    node = Nodes.findOne(_id: _id)
    nodesIds = [node._id].concat(node.similar_nodes)
  else 
    nodesIds = Nodes.find({similar_nodes: {$exists: true}},{fields: _id: 1}).map (itm) -> itm._id
  nodeShapesIds = NodeShapes.find({node_id: $in: nodesIds},{fields: _id: 1}).map (itm) -> itm._id
  plotsIds = NodeShapes.find({node_id: $in: nodesIds},{fields: plot_id: 1}).map (itm) -> itm.plot_id


  [
    Nodes.find(_id: $in: nodesIds)
    NodeShapes.find(_id: $in: nodeShapesIds), Plots.find(_id: $in: plotsIds),
    NodeShapeAdaptersDefaultWidgets.find(), NodeTypes.find(), Images.find(),
    ServerSettings.find(), NodeShapeAdapters.find(), NodeTypesToWidgets.find(),
    NodeWidgetTypes.find(),
    TutorialPages.find(), Extensions.find(),
    Files.find()
  ]

