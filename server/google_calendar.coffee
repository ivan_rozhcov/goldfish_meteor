Meteor.methods
  getEvent: (options) ->
    GoogleApi.get('/your/api/path', options, callback)
  googleCalendarList: (options) ->
    GoogleApi.get('calendar/v3/users/me/calendarList')
  googleCreateEvent: (options) ->
    GoogleApi.post('calendar/v3/calendars/primary/events', data:
      'end': 'dateTime': '2015-05-24T13:22:53.108Z'
      'start': 'dateTime': '2015-05-24T10:22:53.108Z')
