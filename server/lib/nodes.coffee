@nodeShapePlotChangeMode = (plotId, nodeOldType, nodeNewType, linkOldType, linkNewType) ->
    NodeShapes.update
      plot_id: plotId
      type: nodeOldType
    ,
      $set:
        type: nodeNewType
    ,
      multi: true

    LinkShapes.update
      plot_id: plotId
      type: linkOldType
    ,
      $set:
        type: linkNewType
    ,
      multi: true

    return

@nodeShapeChangeMode = (options) ->
  # nodeShapeId, plotId, nodeOldType, nodeNewType, linkOldType, linkNewType

  nodeShapeId = options.nodeShapeId
  plotId = options.plotId
  nodeOldType = options.nodeOldType
  nodeNewType = options.nodeNewType
  linkOldType = options.linkOldType
  linkNewType = options.linkNewType

  #показываем линкшейпы, ранее удаленные
  LinkShapes.update
    plot_id: plotId
    type: linkOldType
    $or: [
      {
        node_shape_from_id: nodeShapeId
      }
      {
        node_shape_to_id: nodeShapeId
      }
    ]
  ,
    $set:
      type: linkNewType
  ,
    multi: true


  nodeshapesFrom = LinkShapes.find(
    type: linkNewType
    plot_id: plotId
    node_shape_to_id: nodeShapeId
  ).map( (l) -> l.node_shape_from_id)

  nodeshapesTo = LinkShapes.find(
    type: linkNewType
    plot_id: plotId
    node_shape_from_id: nodeShapeId
  ).map( (l) -> l.node_shape_to_id)

  #склеиваем эти 2 массива
  relatedNodeShapes = _.union(nodeshapesFrom, nodeshapesTo)

  # теперь показываем ноды, которые соеденены с этими линками
  NodeShapes.update
    _id:
      $in: relatedNodeShapes

    type: nodeOldType
  ,
    $set:
      type: nodeNewType
  ,
    multi: true
  return


@removeNodeShape = (nodeShapeId, direct) ->
  trashPlot = Plots.findOne(system_type:'trash', users: Meteor.userId())
  moveNodeShape(nodeShapeId, trashPlot._id)

  for linkShape in LinkShapes.find($or: [
    { node_shape_from_id: nodeShapeId }
    { node_shape_to_id: nodeShapeId }
  ]).fetch()
    LinkShapes.update linkShape._id, $set: plot_id: trashPlot._id
    # trash auto nodes
    if linkShape.node_shape_from_id != nodeShapeId
      autoNodeShape = NodeShapes.findOne(_id: linkShape.node_shape_from_id, type: $in: ['tmp-node-auto-show', 'tmp-node-auto'])
    else
      autoNodeShape = NodeShapes.findOne(_id: linkShape.node_shape_to_id, type: $in: ['tmp-node-auto-show', 'tmp-node-auto'])
    console.log 'autoNodeShape', autoNodeShape
    if autoNodeShape and not Links.findOne({$or: [{ node_from_id: autoNodeShape.node_id },{ node_to_id: autoNodeShape.node_id }], _id: $ne: linkShape.link_id} )
      removeNodeShape(autoNodeShape._id)

  if direct
#TODO #HACK #FIXME на клиенте это работать не будет
    NodeShapes.direct.update(nodeShapeId, $set: plot_id: trashPlot._id)
  else
    NodeShapes.update(nodeShapeId, $set: plot_id: trashPlot._id)