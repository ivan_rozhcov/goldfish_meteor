Meteor.startup(() ->
  Meteor.methods(
    'listNotebooks': (token) ->
      Evernote = Meteor.npmRequire('evernote').Evernote
      Future = Meteor.npmRequire('fibers/future')
      client = new Evernote.Client(
        token: token,
        sandbox: Meteor.settings.sandbox
      )
      noteStore = client.getNoteStore()

      f = new Future()
      noteStore.listNotebooks((err, notebooks) ->
        if err
          console.log("Error: " + JSON.stringify(err))
        else
          f.return(notebooks)
      )
      return f.wait()

    'createNote': (token, noteTitle, noteBody, parentNotebook) ->
      Evernote = Meteor.npmRequire('evernote').Evernote
      Future = Meteor.npmRequire('fibers/future')
      f = new Future()

      client = new Evernote.Client(
        token: token,
        sandbox: Meteor.settings.sandbox
      )
      noteStore = client.getNoteStore()

      nBody = '<?xml version="1.0" encoding="UTF-8"?>'
      nBody += '<!DOCTYPE en-note SYSTEM "http://xml.evernote.com/pub/enml2.dtd">'
      nBody += '<en-note>' + noteBody + '</en-note>'

      # Create note object
      ourNote = new (Evernote.Note)
      ourNote.title = noteTitle
      ourNote.content = nBody
      # parentNotebook is optional; if omitted, default notebook is used
      if parentNotebook and parentNotebook.guid
        ourNote.notebookGuid = parentNotebook.guid
      # Attempt to create note in Evernote account
      noteStore.createNote ourNote, (err, note) ->
        if err
          console.log("Error: " + JSON.stringify(err))
        else
          f.return(notebooks)
      return f.wait()
    #TODO не доделан
    'updateNote': (token) ->
      Evernote = Meteor.npmRequire('evernote').Evernote
      Future = Meteor.npmRequire('fibers/future')
      client = new Evernote.Client(
        token: token,
        sandbox: Meteor.settings.sandbox
      )
      noteStore = client.getNoteStore()

      f = new Future()
      #TODO что сюда передавать???
      noteStore.updateNote(note, (err, notebooks) ->
        if err
          console.log("Error: " + JSON.stringify(err))
        else
          f.return(notebooks)
      )
      return f.wait()

    'startOAuth': () ->
      Evernote = Meteor.npmRequire('evernote').Evernote
      Future = Meteor.npmRequire('fibers/future')
      client = new Evernote.Client(
        consumerKey: Meteor.settings.apiConsumerKey,
        consumerSecret: Meteor.settings.apiConsumerSecret,
        sandbox: Meteor.settings.sandbox
      )

      f = new Future()
      setTimeout(() ->
        client.getRequestToken(Meteor.settings.callbackUrl,
          (error, oauthToken, oauthTokenSecret, results) ->
            if error
              console.log("Error: " + JSON.stringify(error))
            else
              f.return(
                'oauthToken': oauthToken,
                'oauthTokenSecret': oauthTokenSecret,
                'authorizeUrl': client.getAuthorizeUrl(oauthToken)
              )
        )
      , 3 * 1000)

      return f.wait()

    'findNotes': (token, notebookGuid, filterOptions) ->
      Evernote = Meteor.npmRequire('evernote').Evernote
      Future = Meteor.npmRequire('fibers/future')
      client = new Evernote.Client(
        token: token,
        sandbox: Meteor.settings.sandbox
      )
      noteStore = client.getNoteStore()

      filter = new Evernote.NoteFilter
      filter.notebookGuid = notebookGuid

      spec = new Evernote.NotesMetadataResultSpec
      spec.includeTitle = true

      f = new Future()
      if filterOptions and filterOptions.noteGuid

        noteStore.getNote( filterOptions.noteGuid, true, true, true, true, (err, noteRes)->
          if err
            console.log("Error: " + JSON.stringify(err), err)
          else
            return f.return [null, [noteRes]]
        )
        return f.wait()



      noteStore.findNotesMetadata(filter, 0, 100, spec, (err, noteList) ->
        if err
          console.log("Error: " + JSON.stringify(err), err)
        else
          #TODO сделать передачу по chunks
          wholeNotes = []
          getNode =  (items, result) ->
            item = items.pop()
            if item
              noteStore.getNote( item.guid, true, true, true, true, (err, noteRes)->
#                console.log 'items', items
#                console.log 'result', result
#                console.log 'noteRes', noteRes
#                if noteRes and noteRes.resources
#                  for res in noteRes.resources
#                    hash = '4914ced8925f9adcc1c58ab87813c81f'
#                    console.log 'get by hash param ', token, item.guid, hash
#                    console.log 'get by hash',  noteStore.getResourceByHash(token, item.guid, hash, true, true, true , (result)->
#                      console.log 'result', err, noteRes
#                    )

                result.push(noteRes)
                getNode(items, result)
              )
            else
              return f.return([noteList, result])

          getNode(noteList.notes, wholeNotes)
      )
      return f.wait()

    'handleCallback': (userId, oauthToken, oauthTokenSecret, verifier) ->
      Evernote = Meteor.npmRequire('evernote').Evernote
      Future = Meteor.npmRequire('fibers/future')
      Fiber = Meteor.npmRequire('fibers')
      client = new Evernote.Client(
        consumerKey: Meteor.settings.apiConsumerKey,
        consumerSecret: Meteor.settings.apiConsumerSecret,
        sandbox: Meteor.settings.sandbox
      )

      f = new Future()
      setTimeout(() ->
        client.getAccessToken(oauthToken, oauthTokenSecret, verifier,
          (error, oauthAccessToken, oauthAccessTokenSecret, results) ->
#            console.log 'запрос', error, results, userId
            if error
              console.log("Error: " + JSON.stringify(error))
            else
              Fiber( ()->
                console.log 'userId', userId, oauthAccessToken, results.edam_userId, results.edam_expires
#                Meteor.users.update(
#                  userId,
#                  {$set:
#                    "services.evernote.accessToken": oauthAccessToken
#                    }
#                )

#                TODO services почему то не работает - пока пусть пишется в профай в будующем переделаем через
#                https://meteorhacks.com/extending-meteor-accounts.html
                Meteor.users.update(
                  userId,
                  {$set:
                    "profile.evernote":
                      {
                        accessToken: oauthAccessToken,
                        id: results.edam_userId,
                        expiresAt: results.edam_expires
                        edamNoteStoreUrl: results.edam_noteStoreUrl
                        edamShard: results.edam_shard
                      }
                    }
                )
              ).run()
#              Meteor.users.update(
#                _id: userId,
#                {$set:
#                  "services.evernote":
#                    {
#                      accessToken: oauthAccessToken,
#                      id: results.edam_userId,
#                      expiresAt: results.edam_expires
#                    }
#                  }
#              )
              f.return(
                'oauthAccessToken': oauthAccessToken,
                'oauthAccessTokenSecret': oauthAccessTokenSecret,
                'edamShard': results.edam_shard,
                'edamUserId': results.edam_userId,
                'edamExpires': results.edam_expires,
                'edamNoteStoreUrl': results.edam_noteStoreUrl,
                'edamWebApiUrlPrefix': results.edam_webApiUrlPrefix
              )
        )
      , 3 * 1000)

      return f.wait()
  )
)