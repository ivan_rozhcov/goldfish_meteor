CollectionAPI = require('meteor/xcv58:collection-api').CollectionAPI
Meteor.startup ->

  #curl -H "X-Auth-Token: YOUR_AUTH_KEY" http://localhost:3000/api/plots
  # code to run on server at startup
  # All values listed below are default
  collectionApi = new CollectionAPI(
    authToken: Meteor.settings.public.apiAuthToken # Require this string to be passed in on each request
    apiPath: "api" # API path prefix
    standAlone: false # Run as a stand-alone HTTP(S) server
    sslEnabled: false # Disable/Enable SSL (stand-alone only)
    listenPort: 3005 # Port to listen to (stand-alone only)
    listenHost: `undefined` # Host to bind to (stand-alone only)
  )


  collectionApi.addCollection Plots, "plots",
    methods: [
      'POST'
      'GET'
      'PUT'
    ]
    before:
      POST: (obj, requestMetadata, returnObject)->
        options = []
        options.is_auto_created = true
        options.source = 'http api'
        plotId = createPlot(obj.title, obj.type, [obj.for_user], options)
        plot = Plots.findOne(plotId)
        returnObject.success = true
        returnObject.statusCode = 201
        returnObject.body =
          method: 'POST'
          obj: plot
        true
      GET: undefined
#      PUT: undefined
      PUT: (obj, newValues, requestMetadata, returnObject) ->
        console.log arguments
        true

  collectionApi.addCollection NodeShapes, 'node_shapes',
    methods: [
      'POST'
      'GET'
      'PUT'
    ]
    before:
      POST: (obj, requestMetadata, returnObject)->
        obj.is_auto_created = true
        obj.source = 'http api'
        # из API так дергать проще

        node = createExtenalNode(obj.title, obj.content, [obj.for_user],
              obj.node_external_uri, obj.node_external_uri, obj.content_type)

        nodeShapeId = createNodeShapeFromNode(node, obj.plot_id, obj.x, obj.y, null, null, obj.view_type)
        nodeShape = NodeShapes.findOne(nodeShapeId)
        returnObject.success = true
        returnObject.statusCode = 201
        returnObject.body =
          method: 'POST'
          obj: nodeShape
        true
      GET: undefined
      PUT: undefined

  collectionApi.addCollection LinkShapes, 'link_shapes',
    methods: [
      'POST'
      'GET'
      'PUT'
    ]
    before:
      POST: (obj)->
        obj.is_auto_created = true
        obj.source = 'http api'
        # из API так дергать проще
        true
      GET: undefined
      PUT: undefined

  collectionApi.addCollection Nodes, 'nodes',
    methods: [
      'POST'
      'GET'
      'PUT'
    ]
    before:
      POST: (obj)->
        obj.is_auto_created = true
        obj.source = 'http api'
        obj.users = [obj.for_user]
        # из API так дергать проще
        obj.attrs = obj.attrs or {}
        if obj.content
          obj.attrs.content = obj.content
        if obj.title
          obj.attrs.title = obj.title
        obj.attrs.external_id = obj.external_id

        for key in Object.keys(obj)
          if key.startsWith('attrs.')
            attrsKey = key.replace('attrs.', '')
            obj.attrs[attrsKey] = obj[key]
            delete obj[key]

        true
      GET: undefined
      PUT: undefined

  # Starts the API server
  collectionApi.start()
  return
