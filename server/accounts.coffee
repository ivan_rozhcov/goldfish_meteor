Accounts.onCreateUser (options, user) ->
  #первым делом создаем типы нодов
  if not Plots.find(system_type: 'types', users: user._id).count()
    typesPlot = createPlot('Types', 'nsg_graph_plot', [user._id], system_type: 'types', system: true)
    Plots.update(typesPlot, {$set: {default_widget: 'nsl_type'}})
    createNodeWithNodeShapeNew(null, 'info', [user._id], 150, 150, typesPlot)

  if not Plots.find(system_type: 'nodeshapes_types', users: user._id).count()
    typesPlot = createPlot('Node Shapes Types', 'nsg_graph_plot', [user._id], system_type: 'nodeshapes_types', system: true)
    Plots.update(typesPlot, {$set: {default_widget: 'nsl_type'}})
  if not Plots.find(system_type: 'inbox', users: user._id).count()
    createPlot('Inbox', 'nsg_graph_plot', [user._id], system_type: 'inbox', system: true)
  if not Plots.find(system_type: 'trash', users: user._id).count()
    createPlot('Trash', 'nsg_graph_plot', [user._id], system_type: 'trash', system: true)
  if not Plots.find(system_type: 'protos', users: user._id).count()
    createPlot('Protos', 'nsg_graph_plot', [user._id], system_type: 'protos', system: true)
  if not Plots.find(system_type: 'home', users: user._id).count()
    createPlot('Home', 'nsg_grid_list_ns', [user._id], system_type: 'home', system: true)

  home = Plots.findOne(system_type: 'home', users: user._id)

  if home and not Plots.find(system_type: 'home_2', users: user._id).count()
    clonePlot(home._id, 'nsg_graph_main_list', system_type: 'home_2')

  if not Plots.find(system_type: 'last_viewed', users: user._id).count()
    createPlot('Last Viewed', 'nsg_graph_plot', [user._id], system_type: 'last_viewed', system: true)
  if not Plots.find(system_type: 'projects', users: user._id).count()
    createPlot('Projects', 'nsg_graph_plot', [user._id], system_type: 'projects', system: true)
  if not Plots.find(system_type: 'sessions', users: user._id).count()
    createPlot('Sessions', 'nsg_graph_plot', [user._id], system_type: 'sessions', system: true)
  if not Plots.find(system_type: 'tags', users: user._id).count()
    createPlot('Tags', 'nsg_graph_plot', [user._id], system_type: 'tags', system: true)
  if not Plots.find(system_type: 'pins', users: user._id).count()
    createPlot('Pins', 'nsg_graph_plot', [user._id], system_type: 'pins', system: true)
  if not (user.profile and user.profile.name) and user.services.google
    if not user.profile
      user.profile = {}
    user.profile.name = user.services.google.name
  user