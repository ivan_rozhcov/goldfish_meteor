if Meteor.isServer
  appDump.allow = ->
    # do your own auth here -- eg. check if user is an admin...
    console.log 'appDump.allow'
    console.log @user.services.google.id
    console.log Meteor.settings.admin_google_id
    if @user.services.google.id == Meteor.settings.admin_google_id
      return true
  NodeShapes._ensureIndex("node_id": 1)
  NodeShapes._ensureIndex("plot_id": 1)
  LinkShapes._ensureIndex("plot_id": 1)
  Links._ensureIndex("node_to_id": 1)
  Links._ensureIndex("node_from_id": 1)