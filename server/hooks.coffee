
nodeShapesIgnoreFields = ['tags']

forbiddenFields = ['x', 'y', 'content']

HOOKS_DEBUG = true

print = ->
  if HOOKS_DEBUG
    console.log.apply this, arguments

LinkShapes.before.insert (userId, doc) ->
  print 'LinkShapes.before.insert'
  doc.create_time = new Date()
  doc.who_added = userId
  if doc.for_user
    doc.users = [doc.for_user]
  if not doc.link_id
    nodeShapeForm = NodeShapes.findOne(doc.node_shape_from_id)
    nodeShapeTo = NodeShapes.findOne(doc.node_shape_to_id)
    doc.link_id = createLink(doc.title, nodeShapeForm.node_id, nodeShapeTo.node_id, doc.type)
    #todo вынести отдеьно, но подумать как, чтобы не делать лишние заропсы
    doc.start_x =  nodeShapeForm.x if not doc.start_x
    doc.start_y =  nodeShapeForm.y if not doc.start_y
    doc.end_x =  nodeShapeTo.x if not doc.end_x
    doc.end_y =  nodeShapeTo.y if not doc.end_y
    doc.node_from_id = nodeShapeForm.node_id
    doc.node_to_id = nodeShapeTo.node_id
  doc.width = 75 if not doc.width
  doc.height = 20 if not doc.height


NodeShapes.before.insert (userId, doc) ->
  unless doc.hooks_dont_sync_insert
    print 'NodeShapes.before.insert', doc
    doc.create_time = new Date()
    doc.who_added = userId
    if doc.for_user
      doc.users = [doc.for_user]
    if not doc.node_id
      doc.node_id = createNode(doc.node_attrs.title, doc.node_attrs.content, doc.users, doc.content_type)

NodeShapes.after.insert (userId, doc) ->
  #TODO это может вызвать проблемы при копировании содержимого нодшейпов
  unless doc.hooks_dont_sync_insert
    print 'NodeShapes.after.insert', doc._id
  #  # добавляем новые ноды в нод, дале по всем нодшейпам само синхронизируется
    if doc.plot_id
      plot = Plots.findOne(doc.plot_id)
      print 'add nodeshape to node ', Nodes.findOne(plot.node_id)?.attrs.content, doc.node_id
      Nodes.update plot.node_id, {$addToSet: {'attrs.content': doc.node_id}}
    node = Nodes.findOne(doc.node_id)
    if node and node.trash_only_nodeshapes
      Nodes.update node._id, $set: trash_only_nodeshapes: false


NodeShapes.after.remove (userId, doc) ->
  print 'NodeShapes.after.remove', doc._id
  #мы удаляем нодшейп - убираем и из нода
  plot = Plots.findOne(doc.plot_id)
  console.log plot.node_id, doc.node_id
  # удаляем из плота только если там нету других клонов
  unless NodeShapes.findOne(plot_id: doc.plot_id, node_id: doc.node_id)
    Nodes.update(plot.node_id, {$pull: {'attrs.content': doc.node_id}})

#TODO перенести в before - чтобы проверять, каие значения поменялись
#прототип
NodeShapes.after.update (userId, doc, fieldNames, modifier, options) ->
  print 'NodeShapes.after.update 1', doc._id

  print 'content', this.previous.node_attrs?.content, doc.node_attrs?.content

  if doc.is_proto
#      console.log doc.is_proto, fieldNames, modifier, options
    #Это ноды с конкретными аттрибутам, когда к ноду применяется прототип, часть его аттрибутов копируется в другой нод. При обновлении тоже.
    #прототипы могут задавать так же тип нода и тип данных, если данные не заданы они их переопределяют
    #если добавляются новые данные, то все агенты обновляют их
    #TODO обновлять только по определенным полям
    for field in fieldNames
      if field not in forbiddenFields
#          console.log field, {field: null},doc[field]
        filter = {proto: doc._id}
        filter['$or'] = []
        or1 = {}
        or1[field] = null
        filter['$or'].push(or1)

        or2 = {}
        or2[field] = {$exists: false}
        filter['$or'].push(or2)

#        console.log filter

        set = {}
        set[field] = doc[field]

        NodeShapes.update filter,
          $set: set
  else if 'proto' in fieldNames
    console.log 'proto update'
    # TODO копируем значения из протипа

#синхронизация с нодом
# специальные управляюшие команды hooks_do_sync, dont_sync
NodeShapes.after.update (userId, doc, fieldNames, modifier, options) ->
  print 'NodeShapes.after.update 2', doc._id
  Plots.update(doc.plot_id, $inc: version: 1)
  console.log 'NodeShapes.after.update fieldNames', fieldNames
  if 'hooks_do_sync' in fieldNames and not doc.has_conflict and not doc.dont_sync
    delete doc.hooks_do_sync
    #обвноялем другие нод шейпы
    if 'node_attrs' in fieldNames
      Nodes.update
        _id: doc.node_id
      ,
        $set:
          attrs: doc.node_attrs
        $inc:
          version: 1
      node = Nodes.findOne(_id: doc.node_id)
      print 'doc attrs ', doc.node_attrs, node.attrs
      NodeShapes.update
        node_id: doc.node_id
        _id:
          $ne: doc._id
        $or: [
          { has_conflict: false }
          { has_conflict: null }
        ]
      ,
        $set:
          node_attrs: node.attrs
          has_new_version: true
          has_conflict: false
          node_version: node.version
          version: doc.version
        ,
          multi: true
    #синхронизируем визуальные аттрибуты с остальными нодами
    if 'attrs' in fieldNames
      NodeShapes.update
        node_id: doc.node_id
        _id:
          $ne: doc._id
        $or: [
          { has_conflict: false }
          { has_conflict: null }
        ]
      ,
        $set:
          has_new_version: true
          attrs: doc.attrs
          has_conflict: false
          #TODO потенциально опасно - версия может разойтись
          node_version: node.version
          version: doc.version
        ,
          multi: true
  #раз он тут, значит мы удаляем нодшейп
  if 'plot_id' in fieldNames
    print 'delete nodeshape'
    plot = Plots.findOne(doc.plot_id)
    #Nodes.update( 'nBCvH89sErE6TrHaD', {$pull: {'attrs.content': '5S7RiYRXL2yMrnLe8'}})
    console.log plot.node_id, doc.node_id
    Nodes.update(plot.node_id, {$pull: {'attrs.content': doc.node_id}})


NodeShapes.before.update (userId, doc, fieldNames, modifier, options) ->
  if ('plot_id' in fieldNames) or ('node_id' in fieldNames)
    throw new Error("You can't update plot_id or node_id in nodeshape")
  print 'NodeShapes.before.update', doc._id, modifier, fieldNames
  node = Nodes.findOne(doc.node_id)
  # тут уже значение node_attrs.content неправильное NodeShapes.before.update content [ '27h3QKKamHuTqvRLg' ] node [ '27h3QKKamHuTqvRLg', 'LerTLZfm6ZEEDSJma', 'aZYy7QhDLsGFGoy4g' ]!
  print 'NodeShapes.before.update content', doc.node_attrs?.content, 'node', node.attrs?.content, 'original ns', NodeShapes.findOne(doc._id).node_attrs?.content
  # часто происходит ошибка, из-за того что в update передается объект, а не через $set
  if '$set' in Object.keys(modifier)
    if not 'change_time' in fieldNames
      modifier.$set.change_time = new Date()
#    else
#      modifier.change_time = new Date()



Nodes.after.insert (userId, doc) ->
  print 'Nodes.after.insert'
#если у этого нода нет нодшейпов добавить его в inbox
  if doc.without_node_shape
    # при создании по АПИ юзер указывается в теле объекта в поле for_user
    if doc.for_user
      userId = doc.for_user
    inboxPlot = Plots.findOne(system_type: 'inbox', users: userId)
    options = has_new_version:true
    # костыльная возможность передать из нода в нодшейп view_type
    if doc.node_shape_view_type_options
      options.view_type = doc.node_shape_view_type_options
    console.log 'without_node_shape', doc, inboxPlot
    createNodeShapeFromNodeNew(doc._id, inboxPlot._id, null, null, null, null, options)

Nodes.after.update (userId, doc, fieldNames, modifier, options) ->
  print 'Nodes.after.update', doc._id, doc.attrs?.content
  #нодшейпы обновляются в NodeShape.after.update
  console.log 'node after update', fieldNames
  if not doc.dont_sync
    console.log 'node after update 1', doc._id
    delete doc.dont_sync
    #обвноялем другие нод шейпы
    if doc.content_type == 'adapter'
      console.log 'its an adapter'
      if 'attrs' in fieldNames
        for plot in Plots.find(node_id: doc._id, $or: [
          { has_conflict: false }
          { has_conflict: null }
        ]).fetch()
          console.log 'plot', plot
          nodeShapesFilter = getOnlyNormalNodeShapesFilter(plot._id, true)

          # direct чтобы в Plots.after.update не возникал бесконечный цикл
          Plots.direct.update plot._id, $set: {title: doc.attrs.title}
          # sync nodeshapes on plots
          plotNodeShapesNodesIds = NodeShapes.find(nodeShapesFilter).fetch().map((s) -> s.node_id)
          console.log plot._id, plot.title
          console.log 'plotNodeShapesNodesIds', plotNodeShapesNodesIds, 'doc.attrs.content', doc.attrs.content
          console.log 'isEqual', _.isEqual(doc.attrs.content?.sort(), plotNodeShapesNodesIds?.sort())

          if not _.isEqual(doc.attrs.content.sort(), plotNodeShapesNodesIds?.sort())
            intersection = _.intersection(doc.attrs.content, plotNodeShapesNodesIds)
            nodesToDelete = _.difference(plotNodeShapesNodesIds, intersection)
            newNodes = _.difference(doc.attrs.content, intersection)
            # delete
            console.log 'nodesToDelete', nodesToDelete
            for nodeId in nodesToDelete
              nodeShape = NodeShapes.findOne(plot_id: plot._id, node_id: nodeId)
              #обновляем с direct, чтобы не вызывать волну апдейтов
              removeNodeShape(nodeShape._id, true)
            # add
            console.log 'newNodes', newNodes
            for nodeId in newNodes
              #обновляем с direct, чтобы не вызывать волну апдейтов
              createNodeShapeFromNodeNew(nodeId, plot._id, null, null, null, null, null, true)

          #A теперь синхронизируем content нодшейпов - кажется не нужно - тут уэе синхронизировано
          NodeShapes.direct.update representing_plot_id: plot._id,
            $set: 'node_attrs.content': doc.attrs.content
            ,
              multi: true




Plots.before.update (userId, doc, fieldNames, modifier, options) ->
  print 'Plots.before.update', doc._id
  console.log 'fieldNames', fieldNames
  if not ('change_time' in fieldNames)
    modifier.$set = modifier.$set || {}
    modifier.$set.change_time = new Date()

Plots.before.insert (userId, doc) ->
  if doc.change_time? or not doc.change_time
    doc.change_time = new Date()
  if doc.filter_type? or not doc.filter_type
    doc.filter_type = 'static'
  # при создании по АПИ юзер указывается в теле объекта в поле for_user
  if doc.for_user
    doc.users = [doc.for_user]

Plots.after.update (userId, doc, fieldNames, modifier, options) ->
  print 'Plots.after.update', doc._id
  unless doc.dont_sync
    delete doc.dont_sync
    if 'title' in fieldNames
      print 'plot - node name sync', doc.title, doc.node_id, fieldNames
      Nodes.update(doc.node_id,
        $set: 'attrs.title': doc.title)
      #TODO сделал тут напрямую, чтобы не возникало бесконечного цикла
      NodeShapes.direct.update node_id: doc.node_id,
        $set: 'node_attrs.title': doc.title
        ,
          multi: true


