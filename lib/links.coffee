@createLink = (title, nodeFromId, nodeToId, users, type) ->
  ###
   Создает ссылку
   @param {String} title to operate on
   @param {number} nodeFromId to operate on
   @param {number} nodeToId to operate on
   @param {[number]} users to operate on
   @param {String} type to operate on
   @return {number} Link Id
   ###
  #TODO зачем users?
  Links.insert(
    title: title
    node_from_id: nodeFromId
    node_to_id: nodeToId
    type: type
    users: users
  )


LINK_SHAPE_OFFSET_CORRECTION_X = 30
LINK_SHAPE_OFFSET_CORRECTION_Y = 30

#на плоте проще оперировать нодшейпами
@createLinkShape = (title, nodeShapeFromId, nodeShapeToId, users, type) ->
  nodeShapeForm = NodeShapes.findOne(nodeShapeFromId)
  nodeShapeTo = NodeShapes.findOne(nodeShapeToId)

  linkId = createLink(title, nodeShapeForm.node_id, nodeShapeTo.node_id, users, type)
  createLinkShapeFromLink(linkId, nodeShapeForm.plot_id, nodeShapeForm.x, nodeShapeForm.y,
    nodeShapeTo.x, nodeShapeTo.y, nodeShapeForm._id, nodeShapeTo._id
  )

@createLinkShapeFromLinkNs = (linkId, nodeShapeFromId, nodeShapeToId, users, type) ->
  nodeShapeForm = NodeShapes.findOne(nodeShapeFromId)
  nodeShapeTo = NodeShapes.findOne(nodeShapeToId)

  createLinkShapeFromLink(linkId, nodeShapeForm.plot_id, nodeShapeForm.x, nodeShapeForm.y,
    nodeShapeTo.x, nodeShapeTo.y, nodeShapeForm._id, nodeShapeTo._id, null, null, type
  )

@createLinkShapeFromLink = (linkId, plotId, startX, startY, endX, endY, nodeShapeFromId, nodeShapeToId, width, height, type) ->
  link = Links.findOne(linkId)

  linkShapeData =
    title: link.title
    start_x: startX
    start_y: startY
    end_x: endX
    end_y: endY
    link_id: linkId
    plot_id: plotId
    node_shape_from_id: nodeShapeFromId
    node_shape_to_id: nodeShapeToId
    node_from_id: link.node_from_id
    node_to_id: link.node_to_id
    width: width
    height: height
    link_type: link.type
    type: type

  linkShapeId = LinkShapes.insert(linkShapeData)

  saveAction({plot_id: plotId, link_id:linkId, link_shape_id: linkShapeId, action_type: ACTION_TYPE_CREATE})
  linkShapeId

@cloneLinkShape = (linkId, plotId, options) ->
  linkShape = LinkShapes.findOne(linkId)
  linkShape = _.extend(linkShape, options)
  linkShape.plot_id = plotId
  delete linkShape._id
  LinkShapes.insert(linkShape)

@saveLinkShapeChanges = ->
  newText = Session.get("editing_linkshape_value")
  editingLinkshapeId = Session.get("editing_linkshape_id")
  linkId = undefined
  hasConflict = true
  editingLinkshape = LinkShapes.findOne(editingLinkshapeId)
  linkId = editingLinkshape.link_id
  return  if editingLinkshape.title is newText
  editingLinkshape.title = newText
  unless editingLinkshape.has_conflict
    editingLinkshape.link_version = editingLinkshape.link_version + 1
    hasConflict = false
  LinkShapes.update
    _id: editingLinkshapeId
  , editingLinkshape
  unless hasConflict
    Meteor.call "saveOtherLinkShapeChanges",
      newText: newText
      linkId: linkId
      editedLinkShapeId: editingLinkshapeId

  Session.set "editing_linkshape_id", null
  Session.set "editing_linkshape_value", null

@unhideLinkShape = (linkShape)->
  LinkShapes.update
    _id: linkShape._id
  ,
    $set:
      type: "link"

  NodeShapes.update
    _id: linkShape.node_shape_to_id
  ,
    $set:
      type: "node"

  NodeShapes.update
    _id: linkShape.node_shape_from_id
  ,
    $set:
      type: "node"

@hasOutgoingLinkShapes = (nodeShape, excludeTitle)->
  if excludeTitle
    LinkShapes.find(
        node_shape_from_id: nodeShape._id
        title: $ne: excludeTitle
        type:
              $nin: [
                "tmp-link"
                "tmp-link-show"
              ]
        plot_id: nodeShape.plot_id
      ).count() > 0
  else
    LinkShapes.find(
        node_shape_from_id: nodeShape._id
        type:
              $nin: [
                "tmp-link"
                "tmp-link-show"
              ]
        plot_id: nodeShape.plot_id
      ).count() > 0

@getOutgoingLinkShapes = (nodeShape, excludeTitle)->
  if excludeTitle
    LinkShapes.find(
        node_shape_from_id: nodeShape._id
        title: $ne: excludeTitle
        type:
              $nin: [
                "tmp-link"
                "tmp-link-show"
              ]
        plot_id: nodeShape.plot_id
      ).fetch()
  else
    LinkShapes.find(
        node_shape_from_id: nodeShape._id
        type:
              $nin: [
                "tmp-link"
                "tmp-link-show"
              ]
        plot_id: nodeShape.plot_id
      ).fetch()

@getNodeShapesFromForLink = (nodeId)->
  nodeIds = Links.find(node_from_id: nodeId).map( (l) -> l.node_from_id)
  if nodeIds
    NodeShapes.find(node_id: $in: nodeIds).fetch()

@getNodeShapesToForLink = (nodeId)->
  nodeIds = Links.find(node_to_id: nodeId).map( (l) -> l.node_to_id)
  if nodeIds
    NodeShapes.find(node_id: $in: nodeIds).fetch()


@hasIncomingLinkShapes = (nodeShape, excludeTitle)->
  if excludeTitle
    LinkShapes.find(
        node_shape_to_id: nodeShape._id
        title: $ne: excludeTitle
        type:
              $nin: [
                "tmp-link"
                "tmp-link-show"
              ]
        plot_id: nodeShape.plot_id
      ).count() > 0
  else
    LinkShapes.find(
        node_shape_to_id: nodeShape._id
        type:
              $nin: [
                "tmp-link"
                "tmp-link-show"
              ]
        plot_id: nodeShape.plot_id
      ).count() > 0

@getIncomingLinkShapes = (nodeShape, excludeTitle)->
  if excludeTitle
    LinkShapes.find(
        node_shape_to_id: nodeShape._id
        title: $ne: excludeTitle
        type:
              $nin: [
                "tmp-link"
                "tmp-link-show"
              ]
        plot_id: nodeShape.plot_id
      ).fetch()
  else
    LinkShapes.find(
        node_shape_to_id: nodeShape._id
        type:
              $nin: [
                "tmp-link"
                "tmp-link-show"
              ]
        plot_id: nodeShape.plot_id
      ).fetch()

@addLinkShapeAuto = (link, plotId) ->
  nsFrom = NodeShapes.findOne(_id: link.node_from_id, plot_id: plotId)
  nsTo = NodeShapes.findOne(_id: link.node_to_id, plot_id: plotId)
  createLinkShape(link.title, nsFrom._id,  nsTo._id, link.users, 'auto')

#создаем нодшейп рядом с linkedNodeShape
@addNodeShapeForLink = (linkedNodeShape, nodeId, type)->
  ns = NodeShapes.findOne(linkedNodeShape)
  plot = Plots.findOne(ns.plot_id)
  #используем spaceManager для определения свободного места
  [x, y] = createNodeShapeNearNodeShape(ns)
  #Сделал так- ризонер теперь тоже добавляет ноды "в тихую" по этому они не попадают в node.attrs.content и ноды такого типа фильтруются в хуках.
  createNodeShapeFromNodeNew nodeId, plot._id, x, y, null, null, type: type, true

@findNodesWithLink = (nodeId, linkName) ->
  ###
   Возвращает список id нодов у котороым есть связь linkName
  ###
  nodesIds = []
  if Links.find(title: linkName, node_to_id: nodeId).count()
    nodesIds = Links.find(title: linkName, node_to_id: nodeId).map( (l) -> l.node_from_id)
  if Links.find(title: linkName, node_from_id: nodeId).count()
    nodesIds = Links.find(title: linkName, node_from_id: nodeId).map( (l) -> l.node_to_id)
  nodesIds

@findLinksForPlot = (plot) ->
#TODO медленно - зарефакторить!
  completedNodeShapeIds = NodeShapes.find({'node_attrs.resolved': $ne: true}, plot_id: plot._id, {fields: _id: 1}).map (itm) -> itm._id

  LinkShapes.find
    type:
      $nin: [
        "tmp-link"
        "tmp-link-show"
        "tmp-link-auto"
        "tmp-link-show-auto"
      ]
    $and: [
      { node_shape_from_id: $in: completedNodeShapeIds }
      { node_shape_to_id: $in: completedNodeShapeIds }
    ]
    plot_id: plot._id


#tree operations
@deleteChildren = (objNodeShape, toIds, linkType) ->
  Meteor.call('deleteChildren', objNodeShape, toIds, linkType)

@addChildren = (objNodeShape, toIds, linkType) ->
  for _id in toIds
#    console.log 'create link ', objNodeShape._id, _id
    createLinkShape(linkType, objNodeShape._id, _id, [Meteor.userId()], linkType)

@syncChildren = (hiered, linkType)->
  if hiered
    for obj,i in hiered
      objNodeShape = NodeShapes.findOne(obj.id)
#      console.log objNodeShape.node_attrs.content, objNodeShape.order, i
      NodeShapes.update objNodeShape._id, $set: order: hiered.length - i
      oldChildren = getChildren(objNodeShape, linkType).map( (s) -> s._id)

      children = _.map obj.children, (s) ->
        s.id

#      console.log 'initial obj ', obj.id, obj.children
#      console.log 'mapped obj ', oldChildren, children

      if children != oldChildren
        intersection = _.intersection(children, oldChildren)
        childrenToDelete = _.difference(oldChildren, intersection)
        newChildren = _.difference(children, intersection)
#        console.log 'result ', newChildren, childrenToDelete
        deleteChildren(objNodeShape, childrenToDelete, linkType)
        addChildren(objNodeShape, newChildren, linkType)

      if children
        syncChildren(obj.children, linkType)


@hasChildren = (nodeShape, linkType) ->
  relatedLinks = LinkShapes.find(
    node_from_id: nodeShape.node_id
    title: linkType
    plot_id: nodeShape.plot_id
    type:
      $nin: [
        "tmp-link"
        "tmp-link-show"
        "tmp-link-auto"
        "tmp-link-show-auto"
      ]

  )
  relatedLinks.count() > 0

@hasParent = (nodeShape, linkType) ->
  relatedLinks = LinkShapes.find(
    node_shape_to_id: nodeShape._id
    title: linkType
    type:
          $nin: [
            "tmp-link"
            "tmp-link-show"
            "tmp-link-auto"
            "tmp-link-show-auto"
          ]
    plot_id: nodeShape.plot_id
  )
  relatedLinks.count() > 0

@getChildren = (nodeShape, linkType) ->
  relatedLinks = LinkShapes.find(
    node_from_id: nodeShape.node_id
    title: linkType
    type:
          $nin: [
            "tmp-link"
            "tmp-link-show"
            "tmp-link-auto"
            "tmp-link-show-auto"
          ]
    plot_id: nodeShape.plot_id,
      sort: { order: -1 }
  )

  childNodeShapes = []
  for link in relatedLinks.fetch()
    childNodeShapes.push(link.node_shape_to_id)

  ns = NodeShapes.find(
      _id:
        $in: childNodeShapes
  )
#  console.log childNodeShapes
#  console.log ns.fetch()
  ns

#end of tree operations