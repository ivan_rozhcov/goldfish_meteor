@timerChain = (delaySecs, callbackArray) ->
  func = callbackArray.pop()
  if func
    setTimeout (->
      func()
      timerChain delaySecs, callbackArray
      return
    ), delaySecs
  return

@timerChainNormal = (delaySecs, callbackArray) ->
  timerChain delaySecs, callbackArray.reverse()

@emulateTypeText = (string, element) ->

  writer = (i) ->
    if string.length <= i++
      element.value = string
      return
    element.value = string.substring(0, i)
    if element.value[element.value.length - 1] != ' '
      element.focus()
    rand = Math.floor(Math.random() * 100) + 100
    setTimeout (->
      writer i
      return
    ), rand
    return

  writer 0
  return

@functionName = (fun) ->
  ret = fun.toString()
  console.log ret
  ret = ret.substr("function ".length)
  console.log ret
  ret = ret.substr(0, ret.indexOf("("))
  console.log ret
  ret

@deepFind = (obj, path) ->
  paths = path.split('.')
  current = obj
  i = undefined
  i = 0
  while i < paths.length
    if current[paths[i]] == undefined
      return undefined
    else
      current = current[paths[i]]
    ++i
  current

@getFileName = (url) ->
  [first, ..., last] = url.split('/')
  parts = last.split('.')
  parts.pop()
  parts.join('.')
