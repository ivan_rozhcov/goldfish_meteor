@syncNodeShapeAdapters = ()->
  NodeShapeAdapters.remove({})
  #сам выбирает тип нодшейпа в зависимости от контента
  #plots
  NodeShapeAdapters.insert(
    default_attributes:
      sort: undefined,
      limit: undefined,
    type: 'nsg_graph_plot'
  )
  NodeShapeAdapters.insert(
    default_attributes:
      sort: { order: -1 },
      sort_type: 'manual'
    type: 'nsg_ordered_list')
  NodeShapeAdapters.insert(width: 200, height: 200, x: 50, y: 50, type: 'nsg_table_plot')
  NodeShapeAdapters.insert(width: 200, height: 200, x: 50, y: 50, type: 'nsg_tree_plot')
  NodeShapeAdapters.insert(width: 200, height: 200, x: 50, y: 50, type: 'nsg_table_plot_content')
  NodeShapeAdapters.insert(width: 200, height: 200, x: 50, y: 50, type: 'nsg_dynamic_table_plot')
  NodeShapeAdapters.insert(
    default_attributes:
      sort: { order: -1 },
      sort_type: 'manual'
    type: 'nsg_grid_list')
  NodeShapeAdapters.insert(width: 200, height: 200, x: 50, y: 50, type: 'nsg_graph_plot_ext_edit')

@createInternalPlot = (plotId, title, type, users, options)->
  options = options or {}
  options = _.extend(options, {is_internal: true, internal_for: plotId})
  newPlotId = createPlot(title, type, users, options)
  Plots.update plotId, $addToSet: internal_plot_ids: newPlotId
  newPlotId


@createPlot = (title, type, users, options) ->
  # content для plot это массив
  #так как есть плот - не нужно побавлять в inbox
  nodeId = createNode(title, [], users, 'adapter', without_node_shape: false)
  console.log nodeId, options
  plot =
    node_id: nodeId
    title: title
    type: type
    #для совместимости с nodeShape
    node_type: type
    users: users
    view_type: type
  plot = _.extend(plot, options)

  Plots.insert plot

@copyPlot = (plotId, options) ->
    tab = Plots.findOne(plotId)
    tab.title = "#{tab.title}_копия"
    tab.node_id = copyNode(tab.node_id)
    tab = _.extend(tab, options)
    delete tab._id


    newPlotId = Plots.insert(tab)

    nodeShapes = NodeShapes.find(plot_id: plotId).fetch()
    linkShapes = LinkShapes.find(plot_id: plotId).fetch()

#    создаем mapping старых id к новым
    nodeShapesIdMap = {}
    for nodeShape in nodeShapes
      nodeShapesIdMap[nodeShape._id] = cloneNodeShapeToPlot(nodeShape._id, newPlotId)
    for linkShape in linkShapes
      cloneLinkShape(linkShape._id, newPlotId,
        node_shape_from_id: nodeShapesIdMap[linkShape.node_shape_from_id]
        node_shape_to_id: nodeShapesIdMap[linkShape.node_shape_to_id]
      )
    newPlotId

@clonePlot = (plotId, newPlotType, options)->
    tab = Plots.findOne(plotId)
    tab.type = newPlotType
    tab.title = "#{tab.title}_клон"
    tab.cloned_from = plotId
    tab = _.extend(tab, options)
    delete tab._id


    newPlotId = Plots.insert(tab)
#    Чтобы точно знать что откуда он склонирован
    Plots.update plotId, $addToSet: clones: newPlotId

    nodeShapes = NodeShapes.find(plot_id: plotId).fetch()
    console.log '@clonePlot nodeShapes', nodeShapes
    linkShapes = LinkShapes.find(plot_id: plotId).fetch()

#    создаем mapping старых id к новым
    nodeShapesIdMap = {}
    for nodeShape in nodeShapes
      # тут вызываем с параметро direct, чтобы не срабатывала логика хуков - иначе нодшейпы будут дублироваться
      # тк он попробует синхронизироваться нодшейпы с нодом плота, при домаблении первого нодшейпа
      nodeShapesIdMap[nodeShape._id] = cloneNodeShapeToPlot(nodeShape._id, newPlotId, null, true)
    for linkShape in linkShapes
      cloneLinkShape(linkShape._id, newPlotId,
        node_shape_from_id: nodeShapesIdMap[linkShape.node_shape_from_id]
        node_shape_to_id: nodeShapesIdMap[linkShape.node_shape_to_id]
      )
    newPlotId

@changeNodeShapeTypePlot = (nodeShapeId, viewType) ->
  #TODO тут надо метод класса вызывать, тк при изменения типа нодшейпа может и высота и некоторые другие аттрибуты поменяться
  newViewType = NodeShapeAdapters.findOne(type: viewType)
  #получаем список "дефолтных аттрибутов новго плота
  setModif = {}
  if newViewType
    defaultAttrs = newViewType.default_attributes
    if defaultAttrs
      setModif = _.extend(setModif, defaultAttrs)

  setModif = _.extend(setModif, {view_type: viewType, type: viewType})
  console.log setModif
  Plots.update(nodeShapeId, $set: setModif)

# sendUpdateToEvernote('ThGxj56rfR9bnB2JW', getEvernoteExtension().sync_zappier_url, "921ab971-2c79-41d3-8f2e-42facc2cc105", window.location.href)
# делать по крону? Иначе будет много "ложных" обновлений
@exportToEvernote = (plotId, url, sourceUrl, callback, host) ->
  plot = Plots.findOne(plotId)
  html = Blaze.toHTMLWithData(Template[plot.type], plot)
#  console.log html
  exportId = Random.id()
# проблема была втом, что передавал content как content
  HTTP.call 'POST', url,
    headers:
      #http://stackoverflow.com/a/30554385/1105534
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    params:
      title: plot.title
      content: html,
      source_url: sourceUrl
      export_id: exportId
      plot_id: plotId
      thumbnail: if host != 'http://localhost:3000' then host + Images.findOne(plot.plot_preview_image). url() else null
    (error, result) ->
      if not error
        console.log result.data.status
        set = $set: {}
        set["$set"]["exports.#{exportId}"] = {type: 'zapier-evernote', date: Date.now(), data: result.data, export_id: exportId}
        Plots.update(plotId, set)
        if callback
          callback()
      else
        throw 'Ошибка при экспорте ' + error

@sendUpdateToEvernote = (plotId, url, noteGuid, sourceUrl, callback) ->
  plot = Plots.findOne plotId
  HTTP.call 'POST', url,
    headers:
      #http://stackoverflow.com/a/30554385/1105534
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    params:
      note_guid: noteGuid
      plot_title: plot.title
      source_url: sourceUrl
      plot_id: plotId
    (error, result) ->
      if not error
        console.log result.data.status
        if callback
          callback()
      else
        throw 'Ошибка при экспорте ' + error