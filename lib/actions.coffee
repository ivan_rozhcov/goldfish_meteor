@saveAction  = (params) ->
  unless params.user
    try
      params.user = if @userId then @userId() else Meteor.userId()
    catch err
      params.user = 'robot'

  #    plot_id, node_id, node_shape_id, link_id, link_shape_id
  Actions.insert
    user: params.user
    plot_id: params.plot_id
    node_id: params.node_id
    node_shape_id: params.node_shape_id
    link_id: params.link_id
    link_shape_id: params.link_shape_id
    action_type: params.action_type
    old_value: params.old_value
    date_added: new Date()
    user_id: params.user_id


@ACTION_TYPE_CREATE = "create"
@ACTION_TYPE_UPDATE = "update"
@ACTION_TYPE_DELETE = "delete"
@ACTION_TYPE_HIDE = "hide"
@ACTION_TYPE_VIEW = "view"