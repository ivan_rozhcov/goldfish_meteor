


@createNode = (title, content, users, contentType, options) ->
  contentType = (if contentType then contentType else 'text')
  #TODO разобраться с логикой или сделать отдельный метод
  #TODO сделать типы нодов закешированные
  if users.length
    user = users[0]
    typesPlot = Plots.find(system_type: 'types', users: user._id)
    if typesPlot
      infoNodeShape = NodeShapes.findOne(plot_id: typesPlot._id, content: 'info')
      if infoNodeShape
        infoNode = infoNodeShape.node_id
  node =
    attrs:
      title: title
      content: content
    version: 1
    created: new Date()
    users: users
    #тип данных - проект, письмо - логика
    node_type: infoNode
    #формат кодирования - текст, html, video
    content_type: contentType


  if options
    node = _.extend(node, options)

  console.log '@createNode'

  Nodes.insert node


@createNodeNew = (options) ->
  console.log '@createNodeNew'
  node = options
  Nodes.insert(node)

@createExtenalNode = (title, content, users, externalUri, externalId, externalType) ->
  console.log 'ext'
  node = Nodes.findOne('attrs.node_external_uri': externalUri, external_id: externalId)
  if node
    console.log 'found node', node
    node._id
  else
    Nodes.insert(
      attrs:
        title: title
        content: content
        node_external_uri:  externalUri
      external_id: externalId
      external_type: externalType
      node_type: "external"
      version: 1
      created: new Date()
      users: users
      content_type: 'url'
    )

hex2a = (hexx) ->
  hex = hexx.toString()
  #force conversion
  str = ''
  i = 0
  while i < hex.length
    str += String.fromCharCode(parseInt(hex.substr(i, 2), 16))
    i += 2
  str

@preprocessEvernoteContent = (content, resources) ->
#  преобразоваваем контент, например заменяем en-todo на input type="checkbox"
  content = content.replace(/<en-todo/g, '<input type="checkbox"').replace(/<\/en-todo>/g, '</input>')
#  if resources
#    for res in resources
#      if res.mime == 'image/png'
##        '<en-media alt="Evernote Logo" type="image/png" hash="4914ced8925f9adcc1c58ab87813c81f"></en-media>' на inline image
#         console.log "<img height='#{res.height}' width='#{res.width}' src='#{Meteor.user().profile.evernote.edamNoteStoreUrl.replace('notestore', 'res/')}#{res.guid}.png' />"
#         createImageNode(res.guid 50, 50, @_id, null, '#{Meteor.user().profile.evernote.edamNoteStoreUrl.replace('notestore', 'res/')}#{res.guid}.png')
#      content
  content

@getOrCreateEvernoteNode = (node, nodeOptions, createOptions) ->
  existingNode = Nodes.findOne(external_id: node.guid)
  node.content = preprocessEvernoteContent(node.content, node.resources)
  console.log existingNode
  if existingNode
    console.log 'else', existingNode
    existingNode = _.extend(existingNode, nodeOptions)
    existingNode.original_node = node
    existingNode.users = nodeOptions.users
    existingNode.attrs.title = node.title
    existingNode.attrs.content = node.content
    existingNode.hooks_do_sync = true
    nodeId = existingNode._id
    delete existingNode._id
    Nodes.update(nodeId, $set: existingNode)
    nodeId
  else
    without_node_shape = createOptions.createInbox or false
    Nodes.insert
      attrs:
        title: node.title
        content: node.content
        node_external_uri:  "https://sandbox.evernote.com/view/notebook/#{node.guid}/"
      external_uri:  "https://sandbox.evernote.com/view/notebook/#{node.guid}/"
      external_id: node.guid
      external_type: 'evernote'
      node_type: "external"
      version: 1
      created: new Date()
      content_type: 'html'
      original_node: node
      users: nodeOptions.users
      without_node_shape: without_node_shape

#РАБОТА С НОД-ШЕЙПАМИ


#TODO брать из NodeShapeAdapters
@TYPE_ICONS = {
  'evernote': '/icons/node-type/evernote.ico'
  'google_calendar': '/icons/node-type/google_calendar.png'
  'trello': '/icons/node-type/trello.png'
  'image': '/icons/node-type/glyphicons_image.png'
  'video': 'glyphicon-film'
  'audio': '/icons/node-type/audio.png'
  'youtube_video': '/icons/node-type/youtube.ico'
  'pdf': '/icons/node-type/pdf.png'
  'text': '/icons/node-type/txt.png'
  'html': '/icons/node-type/html.png'
  'url': '/icons/node-type/external.png'
  'markdown': '/icons/node-type/txt.png'
#  'adapter': '/icons/node-type/adapter.png'
  'adapter': 'glyphicon-th-list'
}

#Глобальные значения в зависимости от типа данных
@DEFAULT_VIEW_TYPE = {
  html: "nsl_readonly"
  text: "nsl_readonly"
  adapter: 'nsl_plot_image_preview'
}

@DEFAULT_EDIT_TYPE = {
  html: "nsl_readonly"
  text: "nsl_readonly"
  adapter: 'nsl_plot'
}

#width, height
@DEFAULT_SIZES = {
  nsl_textarea_with_title: [150, 90]
  nsl_readonly: [150, 50]
  nsl_image: [250, 170]
  nsl_video: [325, 180]
  nsl_audio: [310, 37]
  nsl_iframe:  [340, 200]
  nsl_iframe_preview:  [340, 200]
  nsl_plot_image_preview: [325, 180]
  default_:  [150, 50]
}


@WIDGET_TYPE_ICONS = {
  'graph': '/icons/ui-text-area-icon.png'
  'nsl_plot': '/icons/ui-text-area-icon.png'
  'default': '/icons/edit.svg'
  'nsl_readonly': '/icons/readonly.png'
  'nsl_readonly_with_title': '/icons/readonly.png'
  'nsl_image': '/icons/readonly.png'
  'nsl_video': '/icons/readonly.png'
  'nsl_audio': '/icons/readonly.png'
  'nsl_iframe': '/icons/readonly.png'
}

#ОСновыне методы по работе с нодшейпами

@saveNodeShapeText = (nodeShape, event, name) ->
  saveNodeShapeAttribute(nodeShape, name, event.target.value)

@saveNodeShapeAttribute = (nodeShape, name, value, allNodeShapes=true) ->
  unless deepFind(nodeShape, name) is value
    s = {}
    s[name] = value

    if not allNodeShapes
      s.hooks_do_sync = false
    else
      s.hooks_do_sync = true

    NodeShapes.update
      _id: nodeShape._id
    , $set: s

#TODO устаревшее?


#TODO вынести в hooks!

@determineIcon = (node) ->
  if node.external_type
    TYPE_ICONS[node.external_type]
  else TYPE_ICONS[node.content_type]


@createNodeShapeFromNodeNew = (nodeId, plotId, x, y, width, height, options, direct) ->
  view_type = if options then options.view_type  else null
  console.log '@createNodeShapeFromNodeNew', options
  nodeShapeId = createNodeShapeFromNode(nodeId, plotId, x, y, width, height, view_type, null, options, direct)
  nodeShapeId

@createNodeShapeFromNode = (nodeId, plotId, x, y, width, height, viewType, editType, options, direct) ->
  #TODO откуда брать representing_plot_id? - пока что, нужно вызывать разные метод для создания адаптера и нодшейпа
  # например, как это делается в методе createNodeShapeFromNode
  #TODO use getNewNodeShapeCoords
  x = (if x then x else 50)
  y = (if y then y else 50)
  node = Nodes.findOne(nodeId)

  icon = determineIcon(node)
  unless icon
    icon = 'glyphicon-alert'

  #TODO тут нужна фабрика
  viewType = if viewType then viewType else DEFAULT_VIEW_TYPE[node.content_type]
  editType = if editType then editType else DEFAULT_EDIT_TYPE[node.content_type]

  width = if width then width else if DEFAULT_SIZES[viewType] then DEFAULT_SIZES[viewType][0] else DEFAULT_SIZES['default_'][0]
  height = if height then height else if DEFAULT_SIZES[viewType] then DEFAULT_SIZES[viewType][1] else DEFAULT_SIZES['default_'][1]

  nodeShapeData = {
    widget_type: 'view_only'
    node_attrs: node.attrs
    plot_id: plotId
    icon: icon
    data_icon: icon
    view_type: viewType
    edit_type: editType
    #инфа про нод, пока храниться тут, чтобы избежать лишних запросов
    node_id: nodeId
    node_version: node.version
    node_type: node.content_type
    type: node.node_type
    node_external_type: node.external_type
    node_external_uri: node.external_uri
    node_external_id: node.external_id
    #инфа по размещению, пока хардкодится - потом нужно пидумать что то поумнее
    x:x, y: y, width: width, height: height, zoom: 1, level: 100
  }

  console.log '@createNodeShapeFromNode', options
  nodeShapeData = _.extend(nodeShapeData, options)
  console.log '@createNodeShapeFromNode', options

  if direct
    #сделал так - хуки на сервере, но с клиента передается через документ еще специальны флаг,
    # который в хуках проверяется
    nodeShapeData = _.extend(nodeShapeData, hooks_dont_sync_insert: true)
    nodeShapeId = NodeShapes.direct.insert(nodeShapeData)
  else
    nodeShapeId = NodeShapes.insert(nodeShapeData)

  nodeShapeData._id = nodeShapeId
  saveAction({plot_id: plotId, node_id:nodeId, nodeShape: nodeShapeData._id,action_type: ACTION_TYPE_CREATE})
  nodeShapeId

@createNodeShapeFromNodeShape = (nodeShape, content, x, y, width, height, hasConflict, options) ->
  width = (if width then width else 150)
  height = (if height then height else 50)
  x = (if x then x else 50)
  y = (if y then y else 50)

  #TODO тут нужна фабрика
  if nodeShape.node_type == "nsl_textarea_with_title"
    height = 90

  nodeShapeData = {
    attrs: nodeShape.attrs
    node_attrs: nodeShape.node_attrs
    widget_type: nodeShape.widget_type
    plot_id: nodeShape.plot_id,
    icon: nodeShape.icon
    data_icon: nodeShape.data_icon
    view_type: nodeShape.view_type
    #инфа про нод, пока храниться тут, чтобы избежать лишних запросов
    node_id: nodeShape.node_id
    node_version: nodeShape.node_version
    node_type: nodeShape.node_type
    node_external_type: nodeShape.node_external_type
    #инфа по размещению, пока хардкодится - потом нужно пидумать что то поумнее
    x:x, y: y, width: width, height: height, zoom: 1, level: 100
  }
  if content
    nodeShapeData.node_attrs.content = content
  if hasConflict
    nodeShapeData.has_conflict = true
  nodeShapeData = _.extend(nodeShapeData, options)
  newNodeShape = NodeShapes.insert(nodeShapeData)
  nodeShapeData._id = newNodeShape
  saveAction({plot_id: nodeShape.plot_id, node_id: nodeShape.node_id, nodeShape: nodeShapeData._id, action_type: ACTION_TYPE_CREATE})
  newNodeShape

@cloneNodeShape = (nodeShapeId, options) ->
  ###
   клонирует NodeShape (точнее копирует, но остается связь через нод)
   @param {number} nodeShapeId to operate on
   @param {Object} options объект с аттрибутами, эти свойства перезатрут свойства оригинального нодшейпа
   @return {number} NodeShapes Id
   ###
  nodeShape = NodeShapes.findOne(nodeShapeId)
  nodeShape = _.extend(nodeShape, options)
  delete nodeShape._id
  NodeShapes.insert(nodeShape)

@copyNode = (nodeId) ->
  node = Nodes.findOne(nodeId)
  delete node._id
  Nodes.insert node

@copyNodeShape = (nodeShapeId, options) ->
  ###
   клонирует NodeShape (точнее копирует, но остается связь через нод)
   @param {number} nodeShapeId to operate on
   @param {Object} options объект с аттрибутами, эти свойства перезатрут свойства оригинального нодшейпа
   @return {number} NodeShapes Id
   ###
  nodeShape = NodeShapes.findOne(nodeShapeId)
  nodeId = copyNode(nodeShape.node_id)

  nodeShape.node_id = nodeId

  nodeShape = _.extend(nodeShape, options)
  delete nodeShape._id
  NodeShapes.insert(nodeShape)


@moveNodeShape = (nodeShapeId, newPlotId, options) ->
  ###
   Перемещает NodeShape на другой плот. Очень осторожно - помнить про связи
   @param {number} nodeShapeId to operate on
   @param {number} newPlotId куда перемещать
   @param {Object} options объект с аттрибутами, эти свойства перезатрут свойства оригинального нодшейпа
   @return {number} NodeShapes Id
   ###
  options = _.extend(options or {}, plot_id: newPlotId)
  cloneNodeShape(nodeShapeId, options)
  NodeShapes.remove(nodeShapeId)

@trashNodeShape  = (nodeShapeId, direct) ->
  Meteor.call 'removeNodeShape', nodeShapeId, direct

@changeNodeShapeWidgetType = (nodeShapeId, field, viewType) ->
  #TODO тут надо метод класса вызывать, тк при изменения типа нодшейпа может и высота и некоторые другие аттрибуты поменяться
  obj = {}
  obj[field] = viewType
  NodeShapes.update(nodeShapeId, $set: obj)

@changeNodeShapeType = (nodeShapeId, newType) ->
  NodeShapes.update(nodeShapeId, $set: {node_shape_type: newType})

@changeNodeType = (nodeShape, newType) ->
  saveNodeShapeAttribute(nodeShape, 'node_attrs.type', newType)

@changeNodeProto = (nodeShapeId, newProto) ->
  NodeShapes.update(nodeShapeId, $set: {proto: newProto})

@changeNodeData = (nodeShapeId, newData) ->
  #TODO обновлять тип нода!!!
  throw 'нужно ковертировать содержимое нода!'

@changeNodeShapeWidget = (nodeShapeId, newWidgetId) ->
  newWidget = NodeWidgetTypes.findOne(newWidgetId)
  #TODO тут надо метод класса вызывать, тк при изменения типа нодшейпа может и высота и некоторые другие аттрибуты поменяться
  NodeShapes.update(nodeShapeId, $set: {widget_type: newWidget.type, edit_type: 'nsl_textarea_with_title'})

@createNodeWithNodeShape = (title, content, users, x, y, plotId, width, height, contentType, widgetType) ->
  contentType = (if contentType then contentType else null)
  node = createNode(title, content, users, contentType)
  createNodeShapeFromNode(node, plotId, x, y, width, height, widgetType)

@createNodeWithNodeShapeNew = (title, content, users, x, y, plotId, width, height, nodeOptions, nodeShapeOptions) ->
#  TODO нужно передавать content_type
  nodeOptions = nodeOptions || {}
  unless nodeOptions.attrs
    nodeOptions.attrs =  {}
  nodeOptions.attrs.title =  title
  nodeOptions.attrs.content =  content
  nodeOptions.users =  users
  node = createNodeNew(nodeOptions)
  createNodeShapeFromNodeNew(node, plotId, x, y, width, height, nodeShapeOptions)

#TODO отказаться от Plot
@createNodeWithNodeShapeAdapter = (title, users, x, y, plotId, width, height , contentType, widgetType) ->
  contentType = 'adapter'
  adapterPlotId = createInternalPlot plotId, 'nested Plot', widgetType, users
  adapterPlot = Plots.findOne(adapterPlotId)
  nodeShape = createNodeShapeFromNode(adapterPlot.node_id, plotId, x, y, width, height, widgetType)
  NodeShapes.update(nodeShape, $set: {'view_type': 'nsl_plot', 'representing_plot_id': adapterPlotId})
  nodeShape

#TODO users не используется
@createNodeShapeAdapter = (adapterPlotId, users, x, y, plotId, width, height, options) ->
  console.log 'options params', options
  adapterPlot = Plots.findOne(adapterPlotId)
  options = _.extend({'representing_plot_id': adapterPlotId}, options)
  console.log 'options', options
  nodeShapeId = createNodeShapeFromNodeNew(adapterPlot.node_id, plotId, x, y, width, height,
    options
  )
  nodeShapeId

@cloneNodeShapeToPlot = (nodeShapeId, NewPlotId, options, direct) ->
  nodeShape = NodeShapes.findOne(nodeShapeId)
  nodeShape = _.extend(nodeShape, options)
  delete nodeShape._id
  nodeShape.plot_id = NewPlotId
  if direct
    console.log 'NodeShapes direct insert'
    #сделал так - хуки на сервере, но с клиента передается через документ еще специальны флаг,
    # который в хуках проверяется
    nodeShape = _.extend(nodeShape, hooks_dont_sync_insert: true)
    NodeShapes.direct.insert(nodeShape)
  else
    console.log 'NodeShapes insert'
    NodeShapes.insert(nodeShape)

_explode_by_n = (content, splitSymbol='\n') ->
  content.split(splitSymbol)

@explodeNodeShape = (nodeShapeId, linkType='explodeTo', splitSymbol='\n') ->
  nodeShape = NodeShapes.findOne(nodeShapeId)
  if nodeShape.node_attrs.content
    for text in _explode_by_n(nodeShape.node_attrs.content, splitSymbol)
      #TODO возможно через @createNodeShapeFromNodeShape hasConflicts true
      newNodeShapeId = copyNodeShape nodeShape._id, plot_id:nodeShape.plot_id, x:50, y:50, node_attrs: {content: text}
      createLinkShape(linkType, nodeShape._id, newNodeShapeId, nodeShape.users, 'system')

@createButtonNode = (title, x, y, plot) ->
  createNodeWithNodeShape title, null, [Meteor.userId()],  50, 230,
          plot, x, y, 'system', 'button'

@createImageNode = (title, x, y, plotId, width, height, img_url) ->
  unless img_url
    img_url = title
  createNodeWithNodeShapeNew null, "image", [Meteor.userId()], x, y, plotId, width, height,
    content_type: 'image', attrs: node_external_uri: img_url,
      view_type:"nsl_image", node_external_uri: img_url, node_attrs: node_external_uri: img_url

@createAudioNode = (title, x, y, plot, width, height, url) ->
  unless url
    url = title
  createNodeWithNodeShapeNew null, "image", [Meteor.userId()], x, y, plot, width, height,
    content_type: 'audio',
      view_type:"nsl_audio", node_external_uri: url

@createVideoNode = (title, x, y, plot, width, height, url) ->
  unless url
    url = title
  createNodeWithNodeShapeNew null, "video", [Meteor.userId()], x, y, plot, width, height,
    content_type: 'video',
      view_type:"nsl_video", node_external_uri: url

@createPdfNode = (title, x, y, plot, width, height, url) ->
  unless url
    url = title
  createNodeWithNodeShapeNew null, "pdf", [Meteor.userId()], x, y, plot, width, height,
    content_type: 'pdf',
      view_type:"nsl_iframe", node_external_uri: url



@getOnlyNormalNodeShapesFilter = (plotId, full) ->
  filter =
    type:
      $nin: [
        "tmp-node"
        "tmp-node-show"
        "tmp-node-auto"
        "tmp-node-auto-show"
      ]
    plot_id: plotId

  unless full
    filter['node_attrs.resolved'] = $ne: true
  filter

#sort & limit нужно хранить в Plot
@findNodesForPlot = (plot, extendedFilter=null, sort=undefined , limit=undefined, orFilter=undefined)->
    filter = getOnlyNormalNodeShapesFilter(plot._id)

    if plot.filter_type == 'dynamic'
      filter = _.extend(filter, JSON.parse(plot.filter))
    #полностью задачет фильтр
    else if plot.filter_type == 'dynamic_full'
      filter = JSON.parse(plot.filter)
    if extendedFilter
      filter = _.extend(filter, extendedFilter)
    if orFilter
      filter = $or: [filter, orFilter]
    unless sort
      sort = plot.sort or undefined
    unless limit
      limit = plot.limit or undefined

    NodeShapes.find filter,
      sort: sort,
      limit: limit


#TODO просто копировать в нод, а синхронизировать через хуки?
@getVirtualAttribute = (nodeId, linkName) ->
  ###
   возвращает нод, для нода и связи, - она позволяет получать аттрибуты связанного нода, через линк (как будто нестед)
   @param {String} nodeId - initial node id
   @param {String} linkName than connects node with nodeId with destination node
   @return {Object} related Node attribute
   ###
  relatedLinks = Links.find(
    node_from_id: nodeId
    title: linkName
  ).fetch()
#  console.log relatedLinks
  if relatedLinks.length > 0
    Nodes.findOne(relatedLinks[0].node_to_id)

@getVirtualAttributeForNodeShape = (nodeShapeId, linkName) ->
  ###
   ###
  relatedLinks = LinkShapes.find(
    node_shape_from_id: nodeShapeId
    title: linkName
  ).fetch()
#  console.log relatedLinks
  if relatedLinks.length > 0
    NodeShapes.findOne(relatedLinks[0].node_shape_to_id)

@showNodeShape = (nodeShape) ->
  if nodeShape.type == 'tmp-node'
    NodeShapes.update nodeShape._id, $set: type: 'tmp-node-show'

@getNodeShapeById =  (nodeShapeId) ->
    NodeShapes.findOne nodeShapeId

@resolveNode  = (nodeShapeId, val) ->
  NodeShapes.update nodeShapeId, $set:
    hooks_do_sync: true
    'node_attrs.resolved': val

#parse nodes dusi

dusiTypesToGoldFish =
  'Date': 'date'
  'Text': 'text'
  'Person': 'person'

@parseNodesDusi =  ->
    nodes = Nodes.find(
      proccessed_by_dusi: $ne: true
      content_type: 'text'
      is_auto_created: $ne: true
      trash_only_nodeshapes: $ne: true
    ).fetch()
    for node in nodes
#      console.log 'nodeId', node._id
      nodeId = node._id

      func2 = ((node) ->
#        console.log 'nodeId func2', node._id
        Meteor.http.get "http://markup.dusi.mobi/api/text?lang=ru&text=#{node.attrs.content}", (err, res) ->
  #        console.log res.statusCode, res
#          console.log 'nodeId call', node._id
          res = JSON.parse(res.content)
          for token in res.tokens
            #вот как решать проблему с замыканиями - замыкания
            unless node.users
              node.users = [Meteor.userId()]
            func = ((ns) ->
              nodeTo = createNode('dusi result', token, ns.users, dusiTypesToGoldFish[token.type],
                proccessed_by_dusi: true
                is_auto_created: true
                source: 'dusi'
              )
              createLink('dusi result', ns._id, nodeTo, ns.users, 'auto')
              console.log 'NodeId loop', ns._id, ns._id, nodeId
              Nodes.update(ns._id, $set: proccessed_by_dusi: true)
              return
            )
            func(node)
          Nodes.update(node._id, $set: proccessed_by_dusi:true)
      )
      func2(node)

@getWebPageAnnotations =  ->
    console.log '@getWebPageAnnotations'
    for user in Meteor.users.find('profile.hypothes': '$exists': true).fetch()
      nodes = Nodes.find(
        proccessed_by_hypothes: $ne: true
        is_auto_created: $ne: true
        trash_only_nodeshapes: $ne: true
        external_type: 'url').fetch()
      for node in nodes
        fn = ((node) ->
          Meteor.http.get "https://hypothes.is/api/search?limit=200&user=#{user.profile.hypothes.login}&uri=#{node.attrs.node_external_uri}", (err, res) ->
            res = JSON.parse(res.content)
            console.log 'rows proccess', res.rows.length
            for token in res.rows
              console.log 'создание аннотации для нода ', node._id
              console.log 'token', token
              existingNode = Nodes.findOne(external_id: token.id)
              addParams = {}
              addParams.tags = token.tags
              addParams.external_id = token.id
  #            addParams.without_node_shape = true
              addParams.users = node.users
              addParams.for_user = user._id
              addParams.hypothes = true
              addParams.hooks_do_sync = true
              addParams.proccessed_by_hypothes = true
              addParams.is_auto_created = true
              addParams.source = 'hypothes'
              attrs = {}
              attrs.node_external_uri = token.links?.incontext
              attrs.exerpt_context = token.text
              unless existingNode
                nodeTo = createNode('annotation', token.text, node.users, 'text', addParams)
                createLink('annotation', node._id, nodeTo, node.users, 'auto')
              else
                existingNode = _.extend(existingNode, addParams)
                console.log 'existingNode.attrs', existingNode.attrs
                existingNode.attrs = _.extend(existingNode.attrs, attrs)
                console.log 'existingNode.attrs', existingNode.attrs
                existingNodeId = existingNode._id
                delete existingNode._id
                Nodes.update existingNodeId, $set: existingNode
                #создаем ссылку на существуюй нод
                linkId = createLink('annotation', node._id, existingNodeId, node.users, 'auto')
                console.log 'created link for existing node linkId', linkId
            Nodes.update(node._id, $set: proccessed_by_hypothes: true)
        )(node)

@getWebPageContent =  ->
    console.log '@getWebPageContent'
    for user in Meteor.users.find('profile.embedly': '$exists': true).fetch()
      nodes = Nodes.find(
        proccessed_by_embedly: $ne: true
        trash_only_nodeshapes: $ne: true
        external_type: 'url').fetch()
      for node in nodes
        #TODO extranct additiona info?
#        Meteor.http.get "http://api.embed.ly/1/extract?maxwidth=500&key=#{user.profile.embedly.key}&url=#{node.external_uri}", (err, res) ->
        fn = ((node) ->
          Meteor.http.get "http://api.embed.ly/1/oembed?key=#{user.profile.embedly.key}&url=#{node.attrs.node_external_uri}", (err, res) ->
  #          console.log res.statusCode, res
            res = JSON.parse(res.content)

            addParams = {}
            addParams.raw_data = res
            addParams.for_user = user._id
            addParams.proccessed_by_embedly = true
            addParams.is_auto_created = true
            addParams.source = 'embedly'

            nodeIds = findNodesWithLink(node._id, 'embedly_content')
            content = "<h5>#{res.title}</h5>"
            #описания может и не быть
            if res.description
              content  += "<div>#{res.description}</div>"
            #картинки может и не быть
            if res.thumbnail_url
              content  += "<img src='#{res.thumbnail_url}'>"

#            console.log content
            if nodeIds.length
              for nodeId in nodeIds
                console.log 'update existing for', node._id
                embedlyNode = Nodes.findOne(nodeId)
                addParams['attrs'] = {}
                addParams['attrs']['content'] = content
                addParams.hooks_do_sync = true
                embedlyNode = _.extend(embedlyNode, addParams)
                delete embedlyNode._id
                Nodes.update nodeId, $set: embedlyNode
            else
              console.log 'create new node for', node._id
              nodeTo = createNode('content', content, node.users, 'html', addParams)
              #создаем автоматическую связиь без нодшейпов
              createLink('embedly_content', node._id, nodeTo, node.users, 'auto')
              # пока сразу же обновляем
            Nodes.update(node._id, $set: proccessed_by_embedly: true)
        )(node)


@createAuto = () ->
    console.log '@createAuto'
# Это что-то типа reasoner в онтологии, пока для простоты я сделал
#  нужно найти все линки, для которых нет линк шейпов (ни одного)
    linksWithLinkShapesIds = LinkShapes.find(trash_only_linkshapes: $ne: true).map( (l) -> l.link_id)
#    console.log linksWithLinkShapesIds
    linksWithoutLinkShapes = Links.find(trash_only_linkshapes: {$ne: true}, _id: $nin: linksWithLinkShapesIds).fetch()
#    console.log linksWithoutLinkShapes
    linksToProcess = {}
    for link in linksWithoutLinkShapes
      linksToProcess[link._id] = {}
      #Создаем список нодщейпов к которым нужно добавить временные ноды и линки
      # по любому у нас или from или to
      #TODO берутся всегда 1е нодшейпы
      nodeShapesFromForLink = getNodeShapesFromForLink(link.node_from_id)
      #filter trash plot nodeshapes or  убираем линки у которых связь с trash_only_nodeshapes. TODO подумать, что с ними делать, может вообще удалять?
      nsToRemove = []
      for ns in nodeShapesFromForLink
        if Plots.findOne(_id: ns.plot_id, system_type:'trash') or Nodes.findOne(_id: ns.node_id, trash_only_nodeshapes: true)
          nsToRemove.push ns
      if nsToRemove
        for nsId in nsToRemove
          index = nodeShapesFromForLink.indexOf(nsId)
          nodeShapesFromForLink.splice(index, 1)

      for ns in nodeShapesFromForLink
        unless linksToProcess[link._id].hasOwnProperty(ns.plot_id)
          linksToProcess[link._id][ns.plot_id] = {}
        linksToProcess[link._id][ns.plot_id]['from_id'] = ns._id
        linksToProcess[link._id][ns.plot_id]['node_id'] = link.node_to_id

      nodeShapesToForLink = getNodeShapesToForLink(link.node_to_id)
      #filter trash plot nodeshapes
      nsToRemove = []
      for ns in nodeShapesToForLink
        if Plots.findOne(_id: ns.plot_id, system_type:'trash') or Nodes.findOne(_id: ns.node_id, trash_only_nodeshapes: true)
          nsToRemove.push ns
      if nsToRemove
        for nsId in nsToRemove
          index = nodeShapesToForLink.indexOf(nsId)
          nodeShapesToForLink.splice(index, 1)

      for ns in nodeShapesToForLink
        unless linksToProcess[link._id].hasOwnProperty(ns.plot_id)
          linksToProcess[link._id][ns.plot_id] = {}
        linksToProcess[link._id][ns.plot_id]['to_id'] = ns._id
        linksToProcess[link._id][ns.plot_id]['node_id'] = link.node_from_id
      # { tWoEN35GANAiPwPaZ: { bL4kgoAF4ZdCvsfLT: { from_id: 'teHbvegYRhdpd6X9N', node_id: 'L4AzvA5TzQpFp5A8d' } } }
      # { link_id: { plot_id: { nodeshape_from_id node_id}}}
#      console.log linksToProcess
      console.log '@createAuto', linksToProcess
      #обработчик
      for linkId in Object.keys(linksToProcess)
        linkProcessor =  linksToProcess[linkId]
        for plotId in Object.keys(linkProcessor)
          linkPlotProcessor =  linkProcessor[plotId]
          if linkPlotProcessor.hasOwnProperty('from_id') and linkPlotProcessor.hasOwnProperty('to_id')
            #просто рисуем связь
            createLinkShapeFromLinkNs(link._id, linkPlotProcessor['from_id'],  linkPlotProcessor['to_id'], link.users, 'tmp-link-auto')
          else if linkPlotProcessor.hasOwnProperty('from_id')
#            console.log 'nodeshape', linkPlotProcessor['from_id']
            nsToId = addNodeShapeForLink(linkPlotProcessor['from_id'], linkPlotProcessor['node_id'], 'tmp-node-auto')
#            console.log 'created nodeshape to - id ', nsToId
            createLinkShapeFromLinkNs(link._id, linkPlotProcessor['from_id'],  nsToId, link.users, 'tmp-link-auto')
          else if linkPlotProcessor.hasOwnProperty('to_id')
            nsFromId = addNodeShapeForLink(linkPlotProcessor['to_id'], linkPlotProcessor['node_id'], 'tmp-node-auto')
#            console.log 'created nodeshape from - id ', nsFromId
            createLinkShapeFromLinkNs(link._id, nsFromId,  linkPlotProcessor['to_id'], link.users, 'tmp-link-auto')
          #если нодшейпов вообще нету ничего не делаем
  #        else
  #          pass

#TODO делает пересинхронизацию - лишние удалеят
#@syncAutoLinks = () ->
#  pass

@countNodeShapesByRelation = (nodeShapeId, linkType, nodeType) ->
  nodeShape = NodeShapes.findOne nodeShapeId
  plotId = nodeShape.plot_id

  nodeshapesFrom = LinkShapes.find(
    type: linkType
    plot_id: plotId
    node_shape_to_id: nodeShapeId
  ).map( (l) -> l.node_shape_from_id)

  nodeshapesTo = LinkShapes.find(
    type: linkType
    plot_id: plotId
    node_shape_from_id: nodeShapeId
  ).map( (l) -> l.node_shape_to_id)

  #склеиваем эти 2 массива
  relatedNodeShapes = _.union(nodeshapesFrom, nodeshapesTo)

  # теперь показываем ноды, которые соеденены с этими линками
  NodeShapes.find(
    _id:
      $in: relatedNodeShapes
    plot_id: plotId
    type: nodeType
  ).count()

@countRelatedNodeShapes = (nodeShapeId) ->
  #  считает количество спрятанных связанных нодов
  countNodeShapesByRelation nodeShapeId, "tmp-link", "tmp-node"

@countAutoNodeShapes = (nodeShapeId) ->
  # считает количество автоматически созданных нодов
  # сейчас для этого просто создаются нодшейпы, по идее нужно делать, как в отчетах по SERM
  # создавать виртуальную коллекцию и подписываться на нее клиенту
  countNodeShapesByRelation nodeShapeId, "tmp-link-auto", "tmp-node-auto"