Router.configure
  layoutTemplate: "layout"
  loadingTemplate: "loading"
  notFoundtemplate: "notFound"
  onBeforeAction: (a)->
#    console.log 'onBeforeAction'
    userId = if Meteor.isClient then Meteor.userId() else @userId
    if userId or a.url == '/appDump'
      @next()
    else
      if Meteor.isClient then @render('accessDenied') else @next()


class @PlotController extends RouteController
  data: ->
    Plots.findOne(@params._id)
  onBeforeAction: ->
    plot = Plots.findOne(@params._id)
    if plot and Meteor.userId() in plot.users then @next() else @render 'forbidden'
    if plot
      document.title = "#{plot.title} - GoldFish"

#раньше это была главная страница - просто вывод 3х последних плотов
Router.route '/home_old',
  template: 'content'
  name: 'content'
  onBeforeAction: ->
    Session.set 'plotPath', null
    Session.set 'nodeShapePath', null
    @next()


Router.map ->
  @route "home",
    path: "/home"
    onBeforeAction: ->
      Session.set 'plotPath', null
      Session.set 'nodeShapePath', null
      @next()
    data: ->
      plot = Plots.findOne(system_type: 'home',users: Meteor.userId())
      if plot
        Session.set 'selectedPlotId', plot._id
      plot

      
  #TODO хз почему бесконечная загурзка для неавторизиованных
  @route "homeNew",
    path: "/"
    data: ->
      plot = Plots.findOne(system_type: 'home_2',users: Meteor.userId())
      if plot
        Session.set 'selectedPlotId', plot._id
      plot
    onBeforeAction: ->
      console.log 'onBeforeAction'
      #это для плагина дампа
      userId = if Meteor.isClient then Meteor.userId() else @userId
      unless userId
        @render "accessDenied"
        @stop()
      else
        @next()

    waitOn: ->
      [
        Meteor.subscribe('plot_one',  null, system_type: 'home_2')
      ]

#  показывает системные плоты
  @route "system",
    path: "/system"
    onBeforeAction: ->
      Session.set 'plotPath', null
      Session.set 'nodeShapePath', null
      @next()

  @route "search",
    path: "/search/:_query"

  @route "trash",
    template: "plotTab",
    name: "trash",
    path: "/trash/"
    data: ->
      plot = Plots.findOne(system_type: 'trash',users: Meteor.userId())
      if plot
        Session.set 'selectedPlotId', plot._id
      plot
    waitOn: ->
      [
        Meteor.subscribe('plot_one', null, system_type: 'trash')
      ]
  @route "inbox",
    template: "plotTab",
    name: "inbox",
    path: "/inbox/"
    data: ->
      plot = Plots.findOne(system_type: 'inbox',users: Meteor.userId())
      if plot
        #TODO так почему то не работет
        Session.set 'selectedPlotId', plot._id
        plot
    waitOn: ->
      [
        Meteor.subscribe('plot_one', null, system_type: 'inbox')
      ]
  #    TODO тут тоже нужно выбирать в зависимости от типа плота
  @route "plotTab",
    path: "/plot/:_id"
    controller: PlotController
    onBeforeAction: ->
      Session.set 'plotPath', @params._id
      Session.set 'nodeShapePath', null
      Session.set 'selectedPlotId', @params._id
      @next()
    waitOn: ->
      [
        Meteor.subscribe('plot_one', @params._id)
      ]

  @route "nodeShapeTab",
    path: "/node-shape/:_id/"
    data: ->
      NodeShapes.findOne(@params._id)
    waitOn: ->
      [
        Meteor.subscribe('nodeshape_one', @params._id)
      ]

  @route "mindTrail",
    path: "/plot/mindtrail/:_id"
    data: ->
      Actions.findOne plot_id: @params._id

  @route "connectedServices",
    path: "/account/connected-services/"

  @route "authorizedServices",
    path: "/account/authorized-services/"

  @route "profileApi",
    path: "/account/profile-api/"

  @route "widgetSettings",
    path: "/account/widget-settings/"
    waitOn: ->
      [
        Meteor.subscribe('widget_settings')
      ]


  @route "storageSettings",
    path: "/account/storage-settings/"
    waitOn: ->
      [
        Meteor.subscribe('db_stats')
      ]

  @route "RestApi",
    path: "/account/rest-api/"

   @route "notificationSettings",
    path: "/account/notification-settings/"

  @route "embedlyApi",
    path: "/account/profile-api/embedly"

  @route "hypothesApi",
    path: "/account/profile-api/hypothes"

  @route "dropboxApi",
    path: "/account/profile-api/dropbox"

  @route "dropboxApiCallback",
    path: "/account/profile-api/dropbox/callback"

  @route "googleCalendar",
    path: "/account/profile-api/google_calendar"

  @route "evernoteApi",
    path: "/account/profile-api/evernote"

  @route "evernoteApiCallback",
    path: "/account/profile-api/evernote/callback"

  @route "evernoteApiLogout",
    path: "/account/profile-api/evernote/logout"

  @route "evernoteApiProd",
    path: "/account/profile-api/evernote-prod"

  @route "functionsHelp",
    path: "/help/functions-help"

  @route "mainConcept",
    path: "/help/main-concept"

  @route "submitIdea",
    path: "/submit-idea/"

  @route "exportForm",
    path: "/export-form/"
  onBeforeAction: ->
    if Meteor.userId() and Meteor.user().profile then @next() else @render 'forbidden'

  @route "suggestFeature",
    path: "/suggest-feature/"

  @route "useCases",
    path: "/usecases/"

  @route "featuresDemoPage",
    path: "/help/features-demo"
    waitOn: ->
      [
        Meteor.subscribe('widget_settings')
      ]

  @route "cronSettings",
    path: "account/cron"

  @route "backUp",
    path: "/account/backup"

  @route "similarNodes",
    path: "/similar-nodes/"
    waitOn: ->
      [
        Meteor.subscribe('similar_nodes')
      ]

  @route "similarNode",
    path: "/similar-nodes/:_id"
    data: ->
      res = []
      ignoreIds = []
      for node in Nodes.find({_id: @params._id}).fetch()
        unless node._id in ignoreIds
          similarNodes = Nodes.find(_id: $in: node.similar_nodes).fetch()
          _.each(similarNodes, (itm) -> itm.similarity = node.similarities?[itm._id])
          node.similarNodes = similarNodes
          res.push node
          ignoreIds = ignoreIds.concat(node.similar_nodes)
      res
    waitOn: ->
      [
        Meteor.subscribe('similar_nodes', @params._id)
      ]

  return

#Router.onBeforeAction "loading"
