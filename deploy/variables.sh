#!/bin/bash
set -e

### Configuration ###

APP=goldfish
USER=goldfish
SUPER_USER=ivan133
DOMAIN=goldfish-app.ivan133.ru
SERVER=${USER}@${DOMAIN}
SERVER_SUDO=${SUPER_USER}@${DOMAIN}
APP_DIR=/var/www/${APP}
DB_NAME=myappdb
KEYFILE=
REMOTE_SCRIPT_PATH=/home/${USER}/deploy-${APP}.sh
RESTART_ARGS=
NGINX_CONF_PATH=/etc/nginx/sites-enabled/${APP}.conf
APP_PYTHON_DIR=/var/www/goldfish_python
