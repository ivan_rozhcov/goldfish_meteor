#!/bin/bash
set -e

. $(dirname "$0")/variables.sh

### Library ###

function run()
{
  echo "Running: $@"
  "$@"
}

# you need to add some commands to run without prompt
# sudo visudo -f /etc/sudoers
# ivan133 ALL=(ALL) NOPASSWD: /bin/mkdir
# TODO you need to restart nginx manually for 1st deploy sudo service nginx restart

### Automation steps ###

if [[ "$KEYFILE" != "" ]]; then
  KEYARG="-i $KEYFILE"
else
  KEYARG=
fi

if [[ `meteor --version` =~ "Meteor 1.4."* ]]; then
  run meteor build --server-only ../.output
  mv ../.output/*.tar.gz /tmp/package.tar.gz
else
  run meteor bundle /tmp/package.tar.gz
fi

echo "---- Copying config files ----"
ssh -t $SERVER_SUDO "sudo mkdir -p $APP_DIR/bundle"
ssh -t $SERVER_SUDO "sudo chown $USER:$USER $APP_DIR"
run scp $KEYARG /tmp/package.tar.gz $SERVER:$APP_DIR
run scp $KEYARG .python/get_stimiliar_nodes.py $SERVER:$APP_PYTHON_DIR/

cat $(dirname "$0")/variables.sh > /tmp/work.sh
cat $(dirname "$0")/work.sh >> /tmp/work.sh

run scp $KEYARG /tmp/work.sh $SERVER:$REMOTE_SCRIPT_PATH

. $(dirname "$0")/nginx-template.conf > /tmp/nginx_config.conf
run scp $KEYARG /tmp/nginx_config.conf $SERVER_SUDO:$NGINX_CONF_PATH

echo
echo "---- Running deployment script on remote server ----"
run ssh $KEYARG $SERVER bash $REMOTE_SCRIPT_PATH